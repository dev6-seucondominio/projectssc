Rails.application.routes.draw do
  root 'cobrancas#index'

  resources :cobrancas#, only: [:index, :show, :update]
  resources :recebimentos, only: [:create, :destroy] 
  post '/recebimentos/receber', to: 'recebimentos#jurosMulta'
end