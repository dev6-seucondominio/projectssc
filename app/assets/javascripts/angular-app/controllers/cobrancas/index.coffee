angular.module 'app'
  .controller 'Cobranca::IndexCtrl', [
    '$scope', '$filter', 'CobrancaResource', 'RecebimentoResource', 'ComposicaoResource', 'ReceberResource'
    (sc, $filter, CobrancaResource, RecebimentoResource, ComposicaoResource, ReceberResource)->

      sc.cobranca = CobrancaResource.get id: 1

      sc.receber =
        data: ''
        valor: 0
        juros: 0
        multa: 0
        cobranca_id: 1

      sc.addRec = ()->
        if sc.receber.valor != 0
          RecebimentoResource.save sc.receber,
            (data)->
              sc.cobranca.recebimentos.push data
              sc.cobranca.totais.recebimentos += data.valor
              sc.receber =
                data: ''
                valor: 0
                juros: 0
                multa: 0
                cobranca_id: 1
            (response)->
              alert('error')

          CobrancaResource.update
            id: sc.cobranca.id,
            sc.receber,
            (data)->
              sc.cobranca.atualizado = data.atualizado
            (response)->
              alert('error')
      
      sc.$watch 'receber.data', ()->
        atualizaJurosMulta()
        
      atualizaJurosMulta = ()->
        ReceberResource.jurosMulta sc.receber,
          (data)->
            sc.receber.juros = data.juros
            sc.receber.multa = data.multa
            sc.receber.atualizado = data.atualizado
          (response)->
            alert('error')

      sc.deleteReceb = (item, index)->
        RecebimentoResource.delete
          id: item.id,
          (data)->
            sc.cobranca.recebimentos.splice index, 1
            sc.cobranca.totais.recebimentos -= data.valor
            atualizaJurosMulta()
          (response)->
            alert('error')
        
        CobrancaResource.update
          id: sc.cobranca.id,
          sc.receber,
          (data)->
            sc.cobranca.atualizado = data.atualizado
          (response)->
            alert('error') 
  ]