angular.module 'app', [
  'sc.app.helpers'
]

.controller 'AppCtrl', [
  '$scope', 'scToggle'
  (sc, toggle)->
    
    sc.viewSlides =
      view: new toggle
      params: {}

    sc.atual = 'slides'

    sc.galerias = [
      {
        id:0
        nome: 'Álbum de Feriados'
        foto: 'http://i63.tinypic.com/xfe234_th.jpg'
        slides: [
          {
            id: 0
            nome: 'Paisagem 1'
            foto: 'http://wallpaper.ultradownloads.com.br/277277_Papel-de-Parede-Bela-Paisagem-de-Campo_1680x1050.jpg'
            time: 10
          }
          {
            id: 1
            nome: 'Paisagem 2'
            foto: 'http://sergiorochareporter.com.br/wp-content/uploads/imagens-imagem-paisagem-e03050.jpg'
            time: 10
          }
          {
            id: 2
            nome: 'Paisagem 3'
            foto: 'http://wallpaper.ultradownloads.com.br/280708_Papel-de-Parede-Paisagem-de-Outono_1920x1080.jpg'
            time: 10
          }
        ]
        total: 20
      }
      {
        id:1
        nome: 'Álbum de Paisagens'
        foto: 'http://i68.tinypic.com/21l26ms_th.jpg'
        slides: [
          {
            id: 0
            nome: 'Foto 1'
            foto: 'http://i68.tinypic.com/21l26ms_th.jpg'
            time: 10
          }
          {
            id: 1
            nome: 'Foto 2'
            foto: 'http://2.bp.blogspot.com/-2VgYVSIqRkw/VCF-RvDJkrI/AAAAAAAA4bg/ln6M7MrLy_Y/s1600/paisagens.jpg'
            time: 10
          }
          {
            id: 2
            nome: 'Foto 3'
            foto: 'http://wallpaper.ultradownloads.com.br/278810_Papel-de-Parede-Paisagem-da-Suica_1920x1200.jpg'
            time: 10
          }
        ]
        total: 30
      }
      {
        id:2
        nome: 'Álbum de Anúncios'
        foto: 'http://i65.tinypic.com/33u4j04_th.jpg'
        slides: [
          {
            id: 0
            nome: 'Outubro rosa'
            foto: 'http://oi64.tinypic.com/33eq5bq.jpg'
            time: 10
          }
          {
            id: 1
            nome: 'Black Friday'
            foto: 'http://i64.tinypic.com/s5z53m_th.png'
            time: 10 
          }
        ]
        total: 20
      }
    ]

    sc.initForm = (obj)->
      sc.showSlide = obj

    sc.showSlides = (obj)->
      if obj.slides
        sc.showSlide = obj.slides 
        sc.showSlide.albumId = sc.galerias.indexOf obj 
      else
        console.log sc.showSlide.albumId
        sc.viewSlides.view.open()
        sc.viewSlides.params.slideAtual = sc.galerias[sc.showSlide.albumId].slides[sc.galerias[sc.showSlide.albumId].slides.indexOf obj]
        sc.viewSlides.params.slides = sc.galerias[sc.showSlide.albumId].slides

      sc.atual = 'slidesShow'

    sc.alterPhoto = (item)->
      sc.viewSlides.params.slideAtual = item

    sc.backSlides = ()->
      sc.showSlide = sc.galerias
      sc.atual = 'slides'
]