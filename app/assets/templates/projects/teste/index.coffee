angular.module "myApp", [ 'ngResource', 'ngRoute' ]

.config [
  '$routeProvider', '$locationProvider'
  ($routeProvider, $locationProvider) ->

    $routeProvider
      .when '/home',
        templateUrl: "home.html"
      .when '/teste1',
        templateUrl: "teste1.html"
      .otherwise
        redirectTo: '/'

    $locationProvider.html5Mode(true)
]

.controller "controll", [
  "$scope", "Post",
  (sc, Post) ->
    console.log Post.get()
]

.factory 'Post', [
  '$resource',
  ($resource)->
    return $resource 'https://teste-12.firebaseio.com/'
]
