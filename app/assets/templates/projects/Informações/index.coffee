angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->
  $scope.usuario = [
    {
      icon:'http://cdn4.iconfinder.com/data/icons/general13/png/64/administrator.png'
      name:'Diego Felipe'
      endereco:'Rua Perna Longa, Curta Perne - CP'
      cpf:'111111111-00'
      rg:'2222222'
      email:'DiegoFelipe@gmail.com'
      tell1:'4444-4444'
      tell2:'7070-7070'
      public:true
    }
    {
      name:'Minhas Cobranças'
      saldo:'0,00'
      mesPassado:'06'
      valorPassado:'385,00'
      mesRef:'05'
      valorRef:'250,00'
      registros: [
        {
          data:'07/01/2015'
          status:'PAGAMENTO EFETUADO'
          taxa:'01/15'
        }
        {
          data:'07/02/2015'
          status:'PAGAMENTO EFETUADO'
          taxa:'01/15'
        }
        {
          data:'07/03/2015'
          status:'PAGAMENTO EFETUADO'
          taxa:'01/15'
        }
      ]
    }
    {
      name:'Minha Residencia'
      informacoes: [
        {
          icon:'http://www.maisfamilia.com/images/icon2.jpg'
          name:'Familiares'
          subInfo: [
            {
              name:'fulano de tal'
              email:'fulanodetal@gmail.com'
              tell:'1234-1234'
              rg:'1234567'
              cpf:'123456789-09'
            }
            {
              name:'fulano de tal'
              email:'fulanodetal@gmail.com'
              tell:'1234-1234'
              rg:'1234567'
              cpf:'123456789-09'
            }
            {
              name:'fulano de tal'
              email:'fulanodetal@gmail.com'
              tell:'1234-1234'
              rg:'1234567'
              cpf:'123456789-09'
            }
          ]
        }
        {
          icon:'http://www.iconesbr.net/iconesbr/2008/12/7326/7326_64x64.png'
          name:'Veículos'
          subInfo: [
            {
              marca:'Marca tal'
              modelo:'Modelo tal'
              cor:'vermelho'
              placa:'abc-1234'
            }
          ]
        }
        {
          icon:'http://tenax.googlecode.com/svn/branches/jsf2/web2.0/WebContent/resources/images/icones/funcionario.png'
          name:'Funcionários'
          subInfo: [
            {
              name:'Fulano de tal'
              funcao:'Função tal'
              tell:'1234-1234'
              rg:'1234567'
              cpf:'123456789-09'
            }
          ]
        }
      ]
    }
    {
      name:'Minhas Ocorrências'
    }
    {
      name:'Agendamentos'
    }
    {
      name:'Circulares e Documentos'
    }
    {
      name:'Encomendas'
    }
  ]
]
