menu =
  w:             $(window)
  topScroll:     60
  position:      60
  oddScroll:     0
  initScroll:    true
  topMenu:       $('#top-menu')
  topBar:        $('#top-bar')
  topBarContent: $('#top-bar-content')
  init: ->
    @setTopBarHeight()
  setTopBarHeight: ->
    @topBar.css 'height', @topBarContent[0].offsetHeight
  scroll: ->
    scroll = @w.scrollTop()
    @oddScroll = scroll if @initScroll
    @initScroll = false

    @position += (@oddScroll - scroll)
    @position = Math.min(@position, @topScroll)
    @position = Math.max(@position, 0)

    if scroll > @topScroll
      @topBarContent.addClass 'fixed'
    else if scroll < 20
      @topBarContent.removeClass 'fixed'

    @topMenu.css 'top', @position - @topScroll
    @topBarContent.css 'top', @position
    @oddScroll = scroll

menu.init()
menu.w.scroll -> menu.scroll()
menu.w.resize -> menu.setTopBarHeight()

angular.module "appUikit", [
  'sc.app.helpers'
]

.controller "UikitCtrl", [
  "$scope", "$scModal",
  (sc, scModal)->

    sc.font =
      size: 1

    sc.listingIcones = [
      "sc-icon-rede",
      "sc-icon-globo",
      "sc-icon-predio",
      "sc-icon-aperto-mao",
      "sc-icon-skype",
      "sc-icon-instagram",
      "sc-icon-youtube",
      "sc-icon-linkedin",
      "sc-icon-google-plus",
      "sc-icon-facebook",
      "sc-icon-twitter",
      "sc-icon-feed",
      "sc-icon-whatsapp",
      "sc-icon-menu",
      "sc-icon-loja",
      "sc-icon-sofa",
      "sc-icon-suporte",
      "sc-icon-sair",
      "sc-icon-casa-1",
      "sc-icon-filtro-1",
      "sc-icon-filtro-2",
      "sc-icon-ordem-a-z",
      "sc-icon-ordem-z-a",
      "sc-icon-ordem-1-9",
      "sc-icon-ordem-9-1",
      "sc-icon-reticiencias-h",
      "sc-icon-reticiencias-v",
      "sc-icon-download-1",
      "sc-icon-upload-1",
      "sc-icon-download-2",
      "sc-icon-upload-2",
      "sc-icon-banir",
      "sc-icon-info-1",
      "sc-icon-info-2",
      "sc-icon-duvida-1",
      "sc-icon-duvida-2",
      "sc-icon-exclamacao-1",
      "sc-icon-exclamacao-2",
      "sc-icon-exclamacao-3",
      "sc-icon-exclamacao-4",
      "sc-icon-fechar-1",
      "sc-icon-fechar-2",
      "sc-icon-quadrado-1",
      "sc-icon-quadrado-2",
      "sc-icon-circulo-1",
      "sc-icon-circulo-1-marcado",
      "sc-icon-circulo-2",
      "sc-icon-mais-1",
      "sc-icon-mais-2",
      "sc-icon-mais-3",
      "sc-icon-mais-4",
      "sc-icon-menos-1",
      "sc-icon-menos-2",
      "sc-icon-menos-3",
      "sc-icon-menos-4",
      "sc-icon-visto-1",
      "sc-icon-visto-2",
      "sc-icon-visto-3",
      "sc-icon-visto-4",
      "sc-icon-visto-5",
      "sc-icon-video-2",
      "sc-icon-video-1",
      "sc-icon-telefone-2",
      "sc-icon-telefone-1",
      "sc-icon-sino",
      "sc-icon-seta-11-esquerda",
      "sc-icon-seta-11-direita",
      "sc-icon-seta-11-cima",
      "sc-icon-seta-11-baixo",
      "sc-icon-seta-10-esquerda",
      "sc-icon-seta-10-direita",
      "sc-icon-seta-10-cima",
      "sc-icon-seta-10-baixo",
      "sc-icon-seta-9-esquerda",
      "sc-icon-seta-9-direita",
      "sc-icon-seta-9-cima",
      "sc-icon-seta-9-baixo",
      "sc-icon-seta-8-esquerda",
      "sc-icon-seta-8-direita",
      "sc-icon-seta-8-cima",
      "sc-icon-seta-8-baixo",
      "sc-icon-seta-7-esquerda",
      "sc-icon-seta-7-direita",
      "sc-icon-seta-7-cima",
      "sc-icon-seta-7-baixo",
      "sc-icon-seta-6-esquerda",
      "sc-icon-seta-6-direita",
      "sc-icon-seta-6-direita-esquerda",
      "sc-icon-seta-6-cima",
      "sc-icon-seta-6-baixo",
      "sc-icon-seta-5-esquerda",
      "sc-icon-seta-5-direita",
      "sc-icon-seta-5-cima",
      "sc-icon-seta-5-baixo",
      "sc-icon-seta-4-esquerda",
      "sc-icon-seta-4-direita",
      "sc-icon-seta-4-cima",
      "sc-icon-seta-4-baixo",
      "sc-icon-seta-3-esquerda",
      "sc-icon-seta-3-direita",
      "sc-icon-seta-3-cima",
      "sc-icon-seta-3-baixo",
      "sc-icon-seta-2-esquerda",
      "sc-icon-seta-2-direita",
      "sc-icon-seta-2-cima",
      "sc-icon-seta-2-baixo",
      "sc-icon-seta-1-esquerda",
      "sc-icon-seta-1-direita",
      "sc-icon-seta-1-cima",
      "sc-icon-seta-1-baixo",
      "sc-icon-relogio-2",
      "sc-icon-relogio-1",
      "sc-icon-reload",
      "sc-icon-historico",
      "sc-icon-polegar-cima",
      "sc-icon-polegar-baixo",
      "sc-icon-pasta-2-fechada",
      "sc-icon-pasta-2-aberta",
      "sc-icon-pasta-1-fechada",
      "sc-icon-pasta-1-aberta",
      "sc-icon-olho-bloqueado",
      "sc-icon-olho",
      "sc-icon-mulher-2",
      "sc-icon-mulher-1",
      "sc-icon-maleta",
      "sc-icon-lupa-2",
      "sc-icon-lupa-1",
      "sc-icon-local",
      "sc-icon-lixeira-2",
      "sc-icon-lixeira-1",
      "sc-icon-lapis",
      "sc-icon-impressora",
      "sc-icon-homem-2",
      "sc-icon-homem-1",
      "sc-icon-grupo-2",
      "sc-icon-grupo-3",
      "sc-icon-grupo-1",
      "sc-icon-grafico-3",
      "sc-icon-grafico-2",
      "sc-icon-grafico-1",
      "sc-icon-gota",
      "sc-icon-folha-planta",
      "sc-icon-fogo",
      "sc-icon-extintor",
      "sc-icon-executivo",
      "sc-icon-estrela",
      "sc-icon-engrenagem",
      "sc-icon-dinheiro-3",
      "sc-icon-dinheiro-2",
      "sc-icon-dinheiro-1",
      "sc-icon-coracao",
      "sc-icon-compras-2",
      "sc-icon-compras-1",
      "sc-icon-compartilhar",
      "sc-icon-comentarios",
      "sc-icon-comentario",
      "sc-icon-codigo-barras-2",
      "sc-icon-codigo-barras-1",
      "sc-icon-clipe",
      "sc-icon-chave-de-fenda",
      "sc-icon-chave",
      "sc-icon-carta-4",
      "sc-icon-carta-2-aberta",
      "sc-icon-carta-2",
      "sc-icon-carta-1-aberta",
      "sc-icon-carta-1",
      "sc-icon-carro",
      "sc-icon-carregando-2",
      "sc-icon-carregando-1",
      "sc-icon-camera-4",
      "sc-icon-camera-3",
      "sc-icon-camera-2",
      "sc-icon-camera-1",
      "sc-icon-calendario-2",
      "sc-icon-calendario-1",
      "sc-icon-caixa-fechada",
      "sc-icon-caixa-aberta",
      "sc-icon-cafe",
      "sc-icon-cadeirante",
      "sc-icon-cadeado-fechado",
      "sc-icon-cadeado-aberto",
      "sc-icon-arquivo-4",
      "sc-icon-arquivo-3",
      "sc-icon-arquivo-2",
      "sc-icon-arquivo-1",
      "sc-icon-animal-2",
      "sc-icon-animal-1",
      "sc-icon-alto-falante-2",
      "sc-icon-alto-falante-1",
      "sc-icon-asterisco",
      "sc-icon-bandeira",
      "sc-icon-microfone",
      "sc-icon-nota-musical",
      "sc-icon-paisagem",
      "sc-icon-infracao",
      "sc-icon-votacao",
      "sc-icon-fabrica",
      "sc-icon-banco"
    ]
    
    sc.modalExamplo = scModal()
]
