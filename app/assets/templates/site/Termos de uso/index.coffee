angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  $scope.contrateVar = {}
  $scope.aviso = new $scModal()
  $scope.avisoContato = new $scModal()
  $scope.termosDeUso = new $scModal()
  $scope.contrato = new $scModal()
  $scope.avisoContrato = new $scModal()

  $scope.teste2 = ->
    if $scope.contrateVar.caixaMarcar2
      $scope.avisoContato.close()
      $scope.avisoContrato.open()
    else
      $scope.aviso.open()

  $scope.teste = ->
    if $scope.contrateVar.caixaMarcar
      $scope.contrato.open()
    else
      $scope.aviso.open()

]
