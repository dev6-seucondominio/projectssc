angular.module "myapp", [ "sc.app.helpers" ]
.controller "ModalCtrl", [
  "$scope"
  "$filter"
  "$scTarget"
  "$scModal"
  ($scope, $filter, $scTarget, $scModal) ->
    $scope.init = (i) ->
      $scope.i = i
      $scope.i.anyModal = new $scModal()  
]
.controller "controll", ["$scope", ($scope) ->
  $scope.itens = [
    {pos: 1},
    {pos: 2},
  ]  
  $scope.init = (controllers) ->
    $scope.controllers = controllers

  $scope.Post = [
    {
      photo: 'http://i.imgur.com/j7BH0XE.png'
      name: 'Jualiana Cristina'
      bloc: 'Bloco 1 - 101'
      published: 'Antenas Estão soltas e poderão cair caso 
      ocorra uma ventania.
      O condomínio tem de verificar isso.'
      hr: '14:20'
      date: '27/04/2015'
      anexo: 'http://i.imgur.com/ywbiUyN.png'
      commentsPost: [
        {
          comment: 'Comentário 1'
          photo: 'http://i.imgur.com/j7BH0XE.png'
          name: 'Jualiana Cristina'
          bloc: 'Bloco 1 - 101'
          date: '27/04/2015'
          hr: '14:25'
          unLikes: 0
          likes: 0
        }
        {
          comment: 'Comentário 2'
          photo: 'http://i.imgur.com/j7BH0XE.png'
          name: 'Jõao Paulo'
          bloc: 'Bloco 1 - 102'
          date: '27/04/2015'
          hr: '14:23'
          unLikes: 0
          likes: 0
        }
      ]
    }
    {
      photo: 'http://i.imgur.com/j7BH0XE.png'
      name: 'Jõao Paulo'
      bloc: 'Bloco 1 - 102'
      published: 'Antenas Estão soltas e poderão cair caso ocorra uma ventania. O condomínio tem de verificar isso.'
      hr: '11:00'
      date: '26/05/2015'
      anexo: 'http://i.imgur.com/7jn8stG.png'
      commentsPost: [
        {
          comment: 'Comentário 1'
          photo: 'http://i.imgur.com/j7BH0XE.png'
          name: 'Jualiana Cristina'
          bloc: 'Bloco 1 - 101'
          date: '26/05/2015'
          hr: '11:23'
          likes: 0
          unLikes: 0
        }
        {
          comment: 'Comentário 2'
          photo: 'http://i.imgur.com/j7BH0XE.png'
          name: 'Jõao Paulo'
          bloc: 'Bloco 1 - 102'
          date: '26/05/2015'
          hr: '11:20'
          likes: 0
          unLikes: 0
        }
      ]
    }
  ]

  $scope.SomaLike = (index) ->
      $scope.Post[index].ContLike+=1

    $scope.SomaUnLike = (index) ->
      $scope.Post[index].ContUnLike+=1
      
  $scope.AnexarArquivo = ->
    angular.element('#anexarFile').trigger('click');

  $scope.editpostHide = (i) ->
    $scope.Post[i].editpost = false

  $scope.BlocAlt = (i) ->
    $scope.Post[i].bloquear = !$scope.Post[i].bloquear
  
  addzero = (i) ->
    if i < 10
      "0" + i
    else i
  
  $scope.AddPost = ->
    return if $scope.postText == ''
    now = new Date
    $scope.Post.unshift
      photo: 'http://i.imgur.com/j7BH0XE.png'
      name: 'Diego Felipe'
      bloc: 'Bloco 2 - 107'
      published: $scope.postText
      hr: addzero(now.getHours()) + ':' + addzero(now.getMinutes())
      date: addzero(now.getDate()) + '/' + addzero(now.getMonth()+1) + '/' + now.getFullYear()
      anexo: 'http://i.imgur.com/7jn8stG.png'
      commentsPost: []
    $scope.postText = ''
    $scope.PostInputShow = false

]

.controller 'CommentsCtrl', ['$scope', ($scope) ->  
  $scope.init = (post) ->
    $scope.post = post
    $scope.post.novoComentario = ''

  addzero = (i) ->
    if i < 10
      "0" + i
    else i
    
  $scope.AddComent = ->
    return if $scope.post.novoComentario == ''
    alert
    now = new Date
    $scope.post.commentsPost.unshift
      comment:  $scope.post.novoComentario
      photo: 'http://i.imgur.com/j7BH0XE.png'
      name: 'Diego Felipe'
      bloc: 'Bloco 1 - 101'
      likes: 0
      unLikes: 0
      date: addzero(now.getDate()) + '/' + addzero(now.getMonth()+1) + '/' + now.getFullYear()
      hr: addzero(now.getHours()) + ':' + addzero(now.getMinutes())
    $scope.post.novoComentario = ''
    $scope.post.InputShow2 = false
]

.controller 'CommentCtrl', ['$scope', ($scope) ->  
  $scope.init = (comment) ->
    $scope.comment = comment

  $scope.sumLikeComment = ->
    $scope.comment.likes += 1
    $scope.comment.likesClick = true

  $scope.sumUnLikeComment = ->
    $scope.comment.likes -= 1
    $scope.comment.likesClick = false

  $scope.edtComment = ->
    $scope.comment.commentedit = true
    $scope.comment.commentShow = false

  $scope.SaveEditComment = ->
    $scope.comment.commentedit = false

  $scope.CancelEditComment = ->
    $scope.comment.commentedit = false

  $scope.deleteComment = ->
    $scope.comment.commentedit = false
    op = confirm("Deseja realmente apagar seu comentário?")
    if op == true 
      $scope.post.commentsPost.splice($scope.commets, 1)
    else alert 

]

.controller 'PostsCtrl', ["$scope", "$scModal", ($scope, $scModal) ->
  $scope.initPost2 = (post) ->
    $scope.post = post
    $scope.post.confirm = new $scModal()

  $scope.editando = ->
    $scope.post.editingPost = true
    $scope.post.editpost = false
    $scope.post.editPost = $scope.post.published

  $scope.saveEdit = ->
    $scope.post.editingPost = false
    $scope.post.published = $scope.post.editPost
    $scope.post.editPost =  ''
  
  $scope.cancelEdit = ->
    $scope.post.editpost = false
    $scope.post.editingPost = false
    $scope.post.editPost = ''

  $scope.deletePost = ->
    $scope.post.editpost = false
    op = confirm("Deseja realmente apagar seu comentário?")
    if op == true 
      $scope.Post.splice($scope.post, 1)

  $scope.EditPost = ->
    $scope.post.editpost = !$scope.post.editpost
]