angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  ##### Modeis
  $scope.devolucaoModal = new $scModal()
  $scope.encontroModal = new $scModal()
  ##### Modeis

  ##### variaveis
  $scope.objetosPerdidos = [
    {
      name: 'Carro audi A8 (xls-1819)'
      status: 'Perdido'
      BoolStatus: false
      acao: false
      perdido: {
        responsavel: 'Matheus Alexandre'
        data: '2015-06-12'
        contato: '(62) 1234-5678'
        descricao: 'Encontrei este celular ontem a tarde, em uma das espreguiçadeiras da piscina.'
      }
    }
    {
      name: 'celular Galaxy s6 preto'
      status: 'Achado'
      BoolStatus: false
      acao: false
      achado: {
        data: '2015-06-12'
        responsavel: 'Diego Felipe'
        contato: '(62) 1234-5678'
        descricao: 'Encontrei este cabelo ontem a tarde, em uma das espreguiçadeiras da piscina.'
      }
    }
    {
      name: 'Celular Sony Xperia Z1'
      status: 'Devolvido'
      BoolStatus: true
      acao: true
      perdido : {
        data: '2015-06-12'
        responsavel: 'Diego Felipe'
        contato: '(62) 1234-5678'
        descricao: 'Encontrei este celular ontem a tarde, em uma das espreguiçadeiras da piscina.'
      }
      achado: {
        data: '2015-06-12'
        responsavel: 'Diego Felipe'
        contato: '(62) 1234-5678'
        descricao: 'Encontrei este celular ontem a tarde, em uma das espreguiçadeiras da piscina.'
      }
      devolvido: {
        responsavel: 'Marta Santana'
        data: '2015-06-12'
        rgCpf: '55555555'
        destino: '101 - Bloco A'
      }
    }
    {
      name: 'Cabelo - louro com uma trança feita pela metade'
      status: 'Devolvido'
      BoolStatus: true
      acao: true
      achado: {
        data: '2015-06-12'
        responsavel: 'Diego Felipe'
        contato: '(62) 1234-5678'
        descricao: 'Encontrei este cabelo ontem a tarde, em uma das espreguiçadeiras da piscina.'
      }
      devolvido: {
        responsavel: 'Marta Santana'
        data: '2015-06-12'
        rgCpf: '55555555'
        destino: '101 - Bloco A'
      }
    }
  ]
  $scope.show = {
    newObj: false
  }
  $scope.tempId = {
    devolucao: -1
  }
  ##### variaveis
]

.controller "NewObj", ["$scope", ($scope) ->
  #### variaveis
  $scope.addPedidoTemp = {}
  #### variaveis

  #### Funções INICIO
  $scope.registrarEvento = ->
    if $scope.addPedidoTemp.name
      if confirm 'Deseja realizar o registro do objeto?'
        $scope.objetosPerdidos.push
          name: $scope.addPedidoTemp.name
          BoolStatus: $scope.addPedidoTemp.BoolStatus
          acao: false
          achado:
            data: $scope.addPedidoTemp.data
            responsavel: $scope.addPedidoTemp.responsavel
            descricao: $scope.addPedidoTemp.descricao
        add = $scope.objetosPerdidos[$scope.objetosPerdidos.length-1]
        if $scope.addPedidoTemp.BoolStatus
          add.status = 'Achado'
        else
          add.status = 'Perdido'
        $scope.addPedidoTemp = {}
        $scope.show.newObj = false

  $scope.excluirRegistro = ->
    $scope.addPedidoTemp = {}
    $scope.show.newObj = false
  #### Funções FIM
]

.controller "listaObj", ["$scope", "$timeout", ($scope, $timeout) ->
  #### Variavéis
  $scope.bgColorAcc = 'background-color: #f3f3f4;'
  #### Variavéis

  #### Funções INICIO
  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openModal = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined

  $scope.editarRegistro = (lista) ->
    alert 'abrir modal para editar'

  $scope.devolverobj = (id) ->
    $scope.tempId.devolucao = id
    $scope.devolucaoModal.open()

  $scope.excluirRegistro = (lista) ->
    if confirm "Deseja realmente excluir o registro ?"
      $scope.objetosPerdidos.splice($scope.objetosPerdidos.indexOf(lista), 1)
  #### Funções FIM
]

.controller "devObj", ["$scope", ($scope) ->
  #### variaveis
  $scope.relatorioDevolucaoTemp = {}
  #### variaveis

  #### Funções INICIO
  $scope.salvarDevolucao = ->
    if $scope.relatorioDevolucaoTemp.responsavel
      if confirm 'Deseja realizar a devolução do objeto?'
        add = $scope.objetosPerdidos[$scope.tempId.devolucao]
        add.acao = true
        add.devolvido =
          data: $scope.relatorioDevolucaoTemp.data
          responsavel: $scope.relatorioDevolucaoTemp.responsavel
          rgCpf: $scope.relatorioDevolucaoTemp.rgCpf
          destino: $scope.relatorioDevolucaoTemp.destino
        $scope.relatorioDevolucaoTemp = {}
      $scope.devolucaoModal.close()

  $scope.cancellDevolucao = ->
    $scope.relatorioDevolucaoTemp = {}
    $scope.devolucaoModal.close()
  #### Funções FIM
]
