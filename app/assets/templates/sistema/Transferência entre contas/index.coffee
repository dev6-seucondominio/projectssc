angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->
  $scope.transferencias = new $scModal()
  $scope.deletar = new $scModal()

  $scope.novo = {}
  $scope.contas = [
    {
      photo:'https://pbs.twimg.com/profile_images/488662411189030912/UVW-kaM7.png'
      name:'Banco do Brasil'
      cc:'387542-87'
      ag:'85247'
      saldo:'10.000,00'
      gerente:'Fulado de tal'
      telefone:'(62) 2341-2342'
      email:'açsdlkfj@gmail.com'
      data:'12/12/1234'
      endereco:'Rua da jabuticabeira Qd. 7 Lt. 2'
      bairro:'Bairro da Jabuticaba'
      cep:'1132412341'
      estado:'Go'
      cidade:'Goiânia'
    }
    {
      photo:'http://assets.lwsite.com.br/uploads/widget_image/image/485/065/485065/20-caixa_icon-256x256.png'
      name:'Caixa Econômica Federal'
      cc:'387542-87'
      ag:'85247'
      saldo:'10.000,00'
      gerente:'Fulado de tal'
      telefone:'(62) 2341-2342'
      email:'açsdlkfj@gmail.com'
      data:'12/12/1234'
      endereco:'Rua da jabuticabeira Qd. 7 Lt. 2'
      bairro:'Bairro da Jabuticaba'
      cep:'1132412341'
      estado:'Go'
      cidade:'Goiânia'
    }
  ]

  $scope.initNovoEdit = (lista) ->
    lista.bgColorAcc = 'background-color: #337ab7;'
    lista.bColor = 'border-right: 1px solid #438ccb;'
    lista.txtColor = 'color: white;'

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openAcc = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined

  $scope.openNovoAcc = (novo) ->
    novo.opened = !novo.opened
    if novo.opened
      novo.bgColorAcc = 'background-color: #337ab7;'
      novo.bColor = 'border-right: 1px solid #438ccb;'
      novo.txtColor = 'color: white;'
    else
      novo.bgColorAcc = 'background-color: #f3f3f4;'
      novo.bColor = 'border-right: 1px solid #e0e2e3;'
      novo.txtColor = undefined
]
