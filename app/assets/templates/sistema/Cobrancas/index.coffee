angular.module 'app', [
  'sc.app.helpers'
]

.controller 'controll', [
  '$scope', 'CobrancasService', '$scModal', '$window', '$filter', "$timeout", '$element', '$http'
  (sc, CobrancasService, $scModal, win, $filter, $timeout, element, $http)->

    sc.cobrancas = []
    $http.get('cobrancas.json').success (data)-> 
      sc.cobrancas = data

    sc.hoje                    = new Date()
    sc.gerarRecibo             = new $scModal()
    sc.modalNadaConsta         = new $scModal()
    sc.modalNadaConsta         = new $scModal()

    sc.actionClick             = {}
    sc.alterarTela             = false
    sc.listagemDevedoresFiltro = CobrancasService.listagemDevedoresFiltro
    
    sc.filtroCobrancaArray = []
    sc.filtro =
      pago:''
      aVencer:''
      vencido:''
      juridico:''

    sc.custos = [
      {
        titulo: "Custas"
        valor: 100
        planContas: 1
      }
      {
        titulo: "Emolumentos"
        valor: 100
        planContas: 2
      }
      {
        titulo: "Cartório"
        valor: 100
        planContas: 3
      }
      {
        titulo: "Honorário"
        valor: 100
        planContas: 3
      }
      {
        titulo: "Advocatícios"
        valor: 100
        planContas: 3
      }
    ]

    sc.dadosGerais = 
      menu: [
        {nome: 'Gerar Boleto'}
        {nome: 'Receber'}
        {nome: 'Editar Cobrança'}
        {nome: 'Histórico'}
        {nome: 'Excluir'}
      ]

    sc.config =
      menu: [
        {nome:'Configurações de Cobranças'}
        {nome:'Configurações Boleto'}
        {nome:'Plano de contas padrão'}
      ]
      status: [
        {nome: 'Aberto'}
        {nome: 'Reaberto'}
        {nome: 'Gerado'}
        {nome: 'Excluído'}
      ]
      modalCobranca: new $scModal
      modalBoleto: new $scModal
      modalPadrao: new $scModal
      composicao:    ['Ambos', 'Débito', 'Crédito']
      planoDeContas: ['1 Receita', '2 Despesa']

    sc.recebimento = 
      _edit: false
      params: {}
      menu: [
        {nome: 'Visualizar'}
        {nome: 'Editar'}
        {nome: 'Excluir'}
      ]
      modal: new $scModal

    sc.gerarBoleto =
      modal: new $scModal

    sc.acoes = [
      {nome: 'Quitar'}
      {nome: 'Gerar Acordo'}
      {nome: 'Gerar Boletos'}
      {nome: 'Dividir Cobrança'}
      {nome: 'Cancelar'}
    ]

    sc.filter = [
      {item: "Recebidos",         active: false}
      {item: "Vencidos",          active: false}
      {item: "A Vencer",          active: false}
      {item: "Parcelados",        active: false}
      {item: "Cobrança Jurídica", active: false}
      {item: "Pagas a Maior",     active: false}
      {item: "Pagas a Menor",     active: false}
      {item: "Canceladas",        active: false}
    ]

    sc.testeclick = ''

    sc.gBoleto = []

    sc.qtdSelecionados = (obj) ->
      qt = CobrancasService.qtdSelecionados(obj)
      return 1 if qt == 1
      return qt if qt > 1
      return 0

    sc.acaoClick = (obj)->
      if obj.nome == 'Gerar Acordo'
        copyTo()
        sc.altTelaClick()
        return
      if obj.nome == 'Gerar Boletos'
        sc.gerarBoleto.modal.open()

    sc.actionMenu = (obj, objExt = [])->
      switch obj.nome
        when 'Gerar Boleto'
          sc.gBoleto = angular.copy(objExt)
          sc.gerarBoleto.modal.open()
        when 'Receber'
          sc.recebimento.modal.open()
          sc.recebimento._receber = true
          sc.recebimento.cobranca_id = objExt.id
          sc.recebimento.params =
              valorDevido: objExt.valor
              valor: objExt.valor
              data: sc.hoje
              multa: 2
              juros: 0.03

        when 'Editar Cobrança'
          objExt._edit = true
          objExt.acc = true
        when 'Configurações Boleto'
          sc.config.modalBoleto.open()
        when 'Configurações de Cobranças'
          sc.config.modalCobranca.open()
        when 'Plano de contas padrão'
          sc.config.modalPadrao.open()

    copyTo = ->
      objRef = CobrancasService.cobrancas
      objDes = CobrancasService.acordos
      for o in objRef
        if o.checked && o.devido
          objDes.push
            id: o.id
            checked: o.checked
            atraso: o.atraso
            imobiliaria: o.imobiliaria
            status: o.status
            valor: o.valor
            devido: o.devido
            juros: o.jurosMulta
            multa: o.jurosMulta
            cadastro: {
              nome: o.cadastro.nome
              rateio: o.cadastro.rateio
              vencimento: o.cadastro.vencimento
              conta: o.cadastro.conta
              operacao: o.cadastro.operacao
            }
            composicaoCobranca: {
              titulo: o.composicaoCobranca.titulo
              valor: o.composicaoCobranca.valor
            }

    sc.altTelaClick = ()->
      sc.alterarTela = !sc.alterarTela
    
    sc.mudarDeTela = ()->
      sc.altTela()
]

.controller 'CobrancasCtrl', [
  '$scope', 'CobrancasService', '$filter', "$timeout"
  (sc, CobrancasService, $filter, $timeout)->

    sc.qtTotal = ()->
      count = 0
      count++ for o in sc.cobrancas
      count

    sc.myFilter = ->
      array = []
      for a in sc.cobrancas
        if a.status == 'Pago'
          array[array.length] = a if sc.filtro.pago == 'Pago'
        if a.status == 'Vencido'
          array[array.length] = a if sc.filtro.vencido == 'Vencido'
        if a.status == 'À vencer'
          array[array.length] = a if sc.filtro.aVencer == 'À vencer'
        if a.status == 'Jurídica'
          array[array.length] = a if sc.filtro.juridico == 'Jurídica'

      if sc.filtro.pago == '' && sc.filtro.vencido == '' && sc.filtro.aVencer == '' && sc.filtro.juridico == ''
        array = sc.cobrancas
      array

    sc.somas = (nome)->
      obj = sc.cobrancas
      switch nome
        when 'Pago' then CobrancasService.somaQTPago(obj)
        when 'À vencer' then CobrancasService.somaQTAVencer(obj)
        when 'Vencido' then CobrancasService.somaQTVencido(obj)
        when 'Jurídica' then CobrancasService.somaQTJuridico(obj)

    sc.somaTotal = ()->
      CobrancasService.somaTotal(sc.cobrancas)

    sc.toggleAcccobrancas = (pag)->
      p.acc = false for p in sc.cobrancas unless pag.acc
      pag.acc = !pag.acc

    sc.viewMarcarTodos = (obj)->
      if obj
        sc.todasMarcadas = false if CobrancasService.qtdSelecionados(obj) != obj.length
        sc.todasMarcadas = true if CobrancasService.qtdSelecionados(obj) == obj.length

    sc.ordernarPor = (name, varBool)->
      sc.orderBool1 = sc.orderBool2 = sc.orderBool3 = sc.orderBool4 = false
      if sc.orderby == name
        switch name
          when 'valor' then sc.orderby = '-valor'; sc.orderBool1 = true
          when 'cadastro.vencimento' then sc.orderby = '-cadastro.vencimento'; sc.orderBool2 = true
          when 'cadastro.nome' then sc.orderby = '-cadastro.nome'; sc.orderBool3 = true
          when 'status' then sc.orderby = '-status'; sc.orderBool4 = true
          else ''
      else
        switch name
          when 'valor' then sc.orderBool1 = true
          when 'cadastro.vencimento' then sc.orderBool2 = true
          when 'cadastro.nome' then sc.orderBool3 = true
          when 'status' then sc.orderBool4 = true
          else ''
        sc.orderby = name

    # sc.cobrancas = CobrancasService.cobrancas

    sc.qtdSelecionadosValue = (obj)->
      CobrancasService.qtdSelecionadosValue(obj)

    sc.selecionarTodos = (obj) ->
      CobrancasService.selecionarTodos(obj)

    sc.desmarcarTodos = (obj) ->
      CobrancasService.desmarcarTodos(obj)
]

.controller 'CobrancaCtrl', [
  '$scope', 'CobrancasService'
  (sc, CobrancasService)->

    sc.actionMenuReceb = (item, obj)->
      switch item.nome
        when 'Visualizar'
          sc.verRecebimento(item)
        when 'Editar'
          sc.recebimento.modal.open()
          sc.recebimento._receber = false
          sc.recebimento._edit = true
        when 'Excluir'
          sc.removeRecebimento(obj)

    sc.initForm = (obj)->
      sc.form = obj

    sc.corText = (obj)->
      switch obj.status
        when 'Jurídica' then 'sc-text-red-darker'
        when 'Vencido' then 'sc-text-red'
        when 'Pago' then 'sc-text-green'
        when 'À vencer' then 'sc-text-yellow'
        else ''

    sc.CalcValorDevido = (recebimento)->
      valor = 0
      valor = (recebimento.multa + recebimento.juros + recebimento.acrescimo + recebimento.custoAdv)
      (valor - recebimento.descontos)

    sc.pesquisar = ->
      skip_ids = sc.form.cobrancas.map (e)-> e.pessoa.id

    sc.removeComposicao = (obj)->
      if confirm "Tem certeza que deveja remover? É sério, não tem como cancelar.. :p"
        sc.form.valor -= obj.valor
        sc.form.composicaoCobranca.splice sc.form.composicaoCobranca.indexOf(obj), 1

    sc.changeSave = ()->
      sc.form.composicaoTotal = somaComposicao()
      sc.form.ajuste = sc.form.composicaoTotal
      sc.form._edit = false

    somaComposicao = ()->
      count = 0
      count += i.valor for i in sc.form.composicaoCobranca
      count

    sc.removeRecebimento = (obj)->
      if confirm "Tem certeza que deveja remover? É sério, não tem como cancelar.. :p"
        sc.form.recebimentoTotal -= obj.valor
        sc.form.recebimentos.splice sc.form.recebimentos.indexOf(obj), 1

    sc.verRecebimento = (obj)->
      sc.recebimento.modal.open()
      sc.recebimento._receber = false
      sc.recebimento._edit = false

    sc.editarRecebimento = (obj)->
      sc.recebimento.modal.open()
      sc.recebimento._receber = false
]

.controller 'AcordosCtrl', [
  '$scope', 'CobrancasService', '$filter', "$timeout"
  ($scope, CobrancasService, $filter, $timeout)->

    $scope.acordos = CobrancasService.acordos
    $scope.parcelamento = {qtdParcelas: 0}
    $scope.parcelas = []

    $scope.somaDevedor = ()->
      CobrancasService.somaDevedor($scope.acordos)

    $scope.valorPagar = ()->
      CobrancasService.somaValor($scope.acordos)

    $scope.simularParcelas = ->
      $scope.parcelas = []
      if $scope.parcelamento.qtdParcelas > 0
        for a in [1..$scope.parcelamento.qtdParcelas]
          $scope.parcelas.push
            parcela: a
            vencimento: $scope.parcelamento.vencimento
            valor: $scope.parcelamento.vencimento

    $scope.toggleAcccobrancas = (pag)->
      p.acc = false for p in $scope.cobrancas unless pag.acc
      pag.acc = !pag.acc
]

.controller 'AcordoCtrl', [
  '$scope'
  ($scope)->

    $scope.initForm = (obj)->
      $scope.form2 = obj
      $scope.form2._openCadastro = true

    $scope.corText = (obj)->
      switch obj.status
        when 'Jurídica' then 'sc-text-red-darker'
        when 'Vencido' then 'sc-text-red'
        when 'Pago' then 'sc-text-green'
        when 'À vencer' then 'sc-text-yellow'
        else ''

    $scope.pesquisar = ->
      skip_ids = $scope.form.cobrancas.map (e)-> e.pessoa.id
]

.controller "BoletosModalCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 10
        cobrancaJuridica: 0
        juros: 10
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.fecharModal = ->
      sc.config.modalBoleto.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
      {id: 5, nome: "Demostrativo de rateio"}
    ]

    sc.modelos = [
      {
        nome: "Taxa de condomínio"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.m."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [2]
        mensagemAdiconal: ""
      }
      {
        nome: "Taxa de rateio"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
      }
    ]
]

.controller "ReceberCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.dias_mes = [
      [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
      [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    ]
    
    data1 = {}
    data2 = {}

    sc.$watch 'recebimento.params.valor', ()->
      id = sc.recebimento.cobranca_id
      sc.recebimento.params.valorDevido = sc.calcularJuros(sc.cobrancas[id], sc.recebimento.params) if id
    sc.$watch 'recebimento.params.juros', ()->
      id = sc.recebimento.cobranca_id
      sc.recebimento.params.valorDevido = sc.calcularJuros(sc.cobrancas[id], sc.recebimento.params) if id
    sc.$watch 'recebimento.params.multa', ()->
      id = sc.recebimento.cobranca_id
      sc.recebimento.params.valorDevido = sc.calcularJuros(sc.cobrancas[id], sc.recebimento.params) if id

    sc.fecharModal = ->
      sc.recebimento.modal.close()

    sc.receber = ()->
      id = sc.recebimento.cobranca_id
      sc.cobrancas[id].recebimentos.push
        valor: sc.recebimento.params.valor
        data: sc.recebimento.params.data
        multa: sc.recebimento.params.multa
        juros: sc.recebimento.params.juros
      sc.cobrancas[id].recebimentoTotal += sc.recebimento.params.valor
      sc.cobrancas[id].atualizado = sc.calcularJuros(sc.cobrancas[id], sc.recebimento.params)
      sc.recebimento.modal.close()

    sc.calcularJuros = (obj, obj2)->
      data1.dia = $filter('date')(obj2.data, 'dd')
      data1.mes = $filter('date')(obj2.data, 'MM')
      data1.ano = $filter('date')(obj2.data, 'yyyy')
      
      data2.dia = $filter('date')(obj.cadastro.vencimento, 'dd')
      data2.mes = $filter('date')(obj.cadastro.vencimento, 'MM')
      data2.ano = $filter('date')(obj.cadastro.vencimento, 'yyyy')

      juros = obj.valor * (obj2.juros/100) * calcDias(data2, data1)
      multa = obj.valor * (obj2.multa/100)

      obj.valor + multa + juros - obj2.valor

    calcDias = (date1, date2)->
      idias = parseInt date1.dia
      fdias = parseInt date2.dia
      
      date1.mes--
      dbissexto = bissexto(date1.ano)
      for i in [date1.mes..0]
        idias += sc.dias_mes[dbissexto][i]

      date2.mes--
      dbissexto = bissexto(date2.ano)
      for i in [date2.mes..0]
        fdias += sc.dias_mes[dbissexto][i]
      
      def_anos = 0
      while date1.ano < date2.ano
        def_anos += 365 + bissexto date1.ano++

      (def_anos - idias + fdias)

    bissexto = (ano)->
      if (ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0))
        return 1
      return 0
]


.controller "ConfigCobrancasCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.listaInput = [{num: 0}]

    sc.addInput = ->
      sc.listaInput.push
        num: sc.listaInput.length

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 10
        cobranca1Juridica: 0
        juros: 0
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.fecharModal = ->
      sc.config.modalCobranca.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
    ]

    sc.modelos = [
      {
        nome: "Taxa de condomínio"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.m."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [2]
        mensagemAdiconal: ""
      }
      {
        nome: "Sala Comercial"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
      }
    ]
]

.controller "escolhaAcordoControll", [
  "$scope"
  (sc)->

    sc.openAcordoC = ->
      sc.modalDesfAcordo.close()
]

.controller "GerarBoletoCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.listaInput = [{num: 0}]

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 10
        cobranca1Juridica: 0
        juros: 0
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.addInput = ->
      sc.listaInput.push
        num: sc.listaInput.length

    sc.fecharModal = ->
      sc.config.modalCobranca.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
    ]

    sc.modelos = [
      {
        nome: "Taxa de condomínio"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.m."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [2]
        mensagemAdiconal: ""
      }
      {
        nome: "Sala Comercial"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
      }
    ]
]

.factory 'CobrancasService', [
  '$http'
  ($http)->
    cobran =
      somaTotal: (obj)->
        count = 0
        count+= o.valor for o in obj
        count

      somaQTJuridico: (obj)->
        count = 0
        count++ for o in obj when o.status == 'Jurídica'
        count

      somaQTPago: (obj)->
        count = 0
        count++ for o in obj when o.status == 'Pago'
        count

      somaQTAVencer: (obj)->
        count = 0
        count++ for o in obj when o.status == 'À vencer'
        count

      somaQTVencido: (obj)->
        count = 0
        count++ for o in obj when o.status == 'Vencido'
        count

      qtdSelecionados: (obj)->
        if obj?.length > 0
          count = 0
          count++ for o in obj when o.checked == true
          return count
        else
          return 0

      qtdSelecionadosValue: (obj)->
        if obj?.length > 0
          count = 0
          count+=o.valor for o in obj when o.checked == true
          return count
        else
          return 0

      somaDevedor: (obj)->
        count = 0
        count += o.devido for o in obj
        count

      somaValor: (obj)->
        count = 0
        count += o.valor for o in obj
        count

      selecionarTodos: (obj)->
        a.checked = true for a in obj

      desmarcarTodos: (obj)->
        a.checked = false for a in obj

      listagemDevedoresFiltro: [
        {
          nome: 'Torre 1'
          itens: [
            {nome: 'Torre 1 - 101'}
            {nome: 'Torre 1 - 102'}
            {nome: 'Torre 1 - 103'}
            {nome: 'Torre 1 - 104'}
          ]
        }
        {
          nome: 'Torre 2'
          itens: [
            {nome: 'Torre 2 - 101'}
            {nome: 'Torre 2 - 102'}
            {nome: 'Torre 2 - 103'}
            {nome: 'Torre 2 - 104'}
          ]
        }
        {
          nome: 'Quadra 1'
          itens: [
            {nome: 'Quadra 1 - 101'}
            {nome: 'Quadra 1 - 102'}
            {nome: 'Quadra 1 - 103'}
            {nome: 'Quadra 1 - 104'}
          ]
        }
        {
          nome: 'Lançamentos individuais'
          itens: [
            {nome: 'Seguradora'}
            {nome: 'Vidraçaria'}
          ]
        }
        {
          nome: 'Garagens'
          itens: [
            {nome: 'Garagem 10-A'}
            {nome: 'Garagem 10-B'}
            {nome: 'Garagem 10-C'}
            {nome: 'Garagem 10-D'}
          ]
        }
      ]
      listaCobrancas: [
        {id: 1,nome: 'Torre 01 - 101'}
        {id: 2,nome: 'Torre 01 - 102'}
        {id: 2,nome: 'Torre 01 - 103'}
      ]
      cobrancas: []
      acordos: []
    cobran
]