angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->
  $scope.novo = new $scModal()
  $scope.editar = new $scModal()
  $scope.avisoOK = new $scModal()
  $scope.avisoOFF = new $scModal()
  $scope.entregar = new $scModal()
  $scope.notificar = new $scModal()
  $scope.confirmVirtua = new $scModal()
  $scope.livroEncomenda = new $scModal()

  $scope.actionClick = {}

  $scope.colunasLista = [
    {name:'Data/Hora'}
    {name:'Unidade'}
    {name:'Porteiro'}
    {name:'Descrição'}
    {name:'Observação'}
  ]

  $scope.imprimirPdfLista = [
    {
      name:'Bloco 1 - Apt. 4 - fulado de tal'
      descricao:'chegou a segundos trás / Ultimas tentativas segundos atrás'
      testes:false
    }
    {
      name:'Torre5 - 101 - fulado de tal'
      descricao:'chegou a um minuto trás / Ultimas tentativas segundos atrás'
      testes:false
    }
    {
      name:'Bloco 1 - Apt. 1 - fulado de tal'
      descricao:'Chegou a 8 minutos'
      testes:false
    }
    {
      name:'Bloco 1 - Apt. 3 - fulado de tal'
      descricao:'Chegou a 4 dias atrás/Ultima tentativa 4 dias atrás'
      testes:false
    }
    {
      name:'Bloco 1 - Apt. 3 - fulado de tal'
      descricao:'Chegou a 9 dias atrás/Ultima tentativa 9 dias atrás'
      testes:true
    }
    {
      name:'João'
      descricao:'Chegou a 16 dias atrás'
      testes:true
    }
    {
      name:'Angular Crazy'
      descricao:'Chegou a 19 dias atrás'
      testes:true
    }
  ]

  #### funções
  $scope.clickTeste = (op1) ->
    if op1
      $scope.confirmVirtua.open()
    else
      $scope.entregar.close()

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openModal = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined
]
