angular.module 'app', [ 
  'sc.app.helpers'
]

.controller 'AppCtrl', [
  '$scope', '$scModal', '$filter', 'scToggle', '$timeout', 'DadosEventos'
  (sc, $scModal, $filter, toggle, $timeout, DadosEventos)->

    sc.pageMod = {list: false, agenda: true}
    
    sc.filtro = {}

    sc.eMailModal     = new $scModal
    sc.ver_mais       = new $scModal
    sc.conf           = new $scModal
    sc.acaoEventModal = new $scModal

    sc.visualizando  = {}

    sc.meses         = ['domingo', 'segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado']
    sc.tipos         = [
      {name:'Aquisição',  bgColor:'sc-bg-cian-light', borderColor:'sc-border-cian-solid-md'}
      {name:'Manutenção', bgColor:'sc-bg-yellow-light', borderColor:'sc-border-yellow-solid-md'}
      {name:'Evento',     bgColor:'sc-bg-blue-light', borderColor:'sc-border-blue-solid-md'}
      {name:'Reforma',    bgColor:'sc-bg-gray-light', borderColor:'sc-border-gray-solid-md'}
      {name:'Lembrete',   bgColor:'sc-bg-green-light', borderColor:'sc-border-green-solid-md'}
      {name:'Outro',      bgColor:'sc-bg-red-light', borderColor:'sc-border-red-solid-md'}
    ]
    
    sc.repetir       = [{tipo:'Diário'}, {tipo:'Semanal'}, {tipo:'Quizenal'}, {tipo:'Mensal'}, {tipo:'Semestral'}, {tipo:'Anual'}]
    sc.config        = 
      opNotif: ['Minutos', 'Horas', 'Dias', 'Semanas', 'Mês']

    setTimeout ()->
      sc.$apply ()->
        $("body,html").animate({scrollTop: 0}, "slow")
    , 0

    date                      = new Date()
    sc.hoje                   = $filter('date')(new Date(date.getFullYear(), date.getMonth()+1, 0), 'yyyy-MM-dd')
    sc.monthPickerDate        = sc.hoje = $filter('date')(new Date(), 'yyyy-MM-dd')
    sc.addNewRegisterDay      = $filter('date')(new Date(), 'dd')
    sc.days                   = []
    sc.numDays                = 0
    sc.winScroll              = false
    sc.nextDays               = []
    sc.prevDays               = []
    sc.acaoEvento             = []
    sc.dadosEnter             = []
    sc.addNewRegisterDay      = {}
    sc.addNewRegister         = {}  
    sc.addNewRegister.assunto = ''
    sc.limitView              = {week:3, event:5}
    
    sc.boxConf                = 
      modal: new toggle
        onOpen: ->
          $timeout -> angular.element('.mini-modal input')[0].focus()
      left: 0
      top: 0

    sc.filterOpen = ()->
      dados = []
      id = 0
      for evento in DadosEventos.eventos[0].evento
        dados.push
          id: id++
          day: evento.dia
          mes: 'Out'
          ano: 15
          events: evento.events
      sc.filtro.dados = dados
      sc.filtro.opened = true
      sc.openFilter = false
      sc.pageMod.list = sc.pageMod.agenda = false

    sc.acaoEventClose = ()->
      sc.acaoEventModal.close()
      sc.ver_mais.open() if sc.acaoEvento.title == 'Editar Agendamento'

    sc.editRegister = (obj)->
      sc.acaoEventModal.open()
      sc.acaoEvento = angular.extend {}, obj
      sc.acaoEvento.title = 'Editar Agendamento'

    sc.removeRegister = (obj)->  
      DadosEventos.removeEvent obj, ($filter('date') sc.monthPickerDate, 'MM'), sc.visualizando.day

    sc.newRegisterFull = ()->
      sc.acaoEventModal.open()
      sc.acaoEvento = []
      sc.acaoEvento.title = 'Novo Agendamento'

    sc.modClick = (op)->
      sc.filtro.opened = false if sc.filtro.opened
      if op == 1
        sc.pageMod.agenda = true
        sc.pageMod.list = false
      else
        sc.pageMod.agenda = false
        sc.pageMod.list = true
      sc.getLimit()

    sc.getLimit = ()->
      setTimeout ()->
        sc.$apply ()->
          win = angular.element('html')
          boxListHW = angular.element('.box-list')
          boxTextHW = angular.element('.box-text')
          boxCircleHW = angular.element('.box-circle')
          if win.outerHeight() < 436
            sc.winScroll = true
          else
            sc.winScroll = false
          if win.outerWidth() < 545
            sc.limitView.week = 1
          else
            sc.limitView.week = 3
          if (win.outerHeight() > 530) || sc.pageMod.list
            sc.limitView.event = parseInt((boxListHW.outerHeight() / boxTextHW.outerHeight()-1))
            if !sc.limitView.event && sc.limitView.event != 0
              sc.limitView.event = parseInt((boxListHW.outerWidth() / boxCircleHW.outerWidth())) if sc.pageMod.list
              sc.limitView.event = parseInt((boxListHW.outerWidth() / boxCircleHW.outerWidth()-2)) if sc.pageMod.agenda
          else
            sc.limitView.event = 0
      , 10

    sc.verMais = (obj)->
      sc.visualizando = obj
      sc.ver_mais.open()

    $(window).resize -> sc.getLimit()
    
    sc.getLimit()
]


.controller 'EventosCtrl', [
  '$scope', '$filter', 'DadosEventos'
  (sc, $filter, DadosEventos)->

    sc.$watch 'monthPickerDate', (newValue) ->
      sc.days = []
      sc.nextDays = []
      sc.prevDays = []
      sc.dadosEnter = []
      sc.numDays = 0
      sc.atualizeData(newValue)

    sc.atualizeData = (date)->
      idInit = sc.findId(date)
      year = $filter('date')(date, 'yyyy')
      month = $filter('date')(date, 'MM')
      prevDays = $filter('date')(new Date(year, month-1, 0), 'dd') if idInit
      sc.numDays = $filter('date')(new Date(year, month, 0), 'dd')
      falta = (parseInt(sc.numDays) + parseInt(idInit))
      sc.dadosEnter = sc.DadosEventosIndex(month)
      sc.dadosEnterPre = sc.DadosEventosIndex(parseInt(month)-1)
      sc.dadosEnterNex = sc.DadosEventosIndex(parseInt(month)+1)
      id = 1

      if id != idInit
        for day in [(prevDays-idInit+1)..prevDays]
          sc.prevDays.push
            id:id
            day:day
            events:[]
          id+=1

      if sc.dadosEnterPre
        for dados in sc.dadosEnterPre
          if dados.dia >= (prevDays-idInit+1)
            add = sc.prevDays[((dados.dia)-(prevDays-idInit+1))]
            add.events = dados.events

      mesCount = idInit+1
      for day in [1..sc.numDays]
        sc.days.push
          id:id
          day:day
          hoje:false
          mes:sc.meses[mesCount-1]
        id+=1
        mesCount+=1
        mesCount = 1 if mesCount > 7

      if sc.dadosEnter
        for dados in sc.dadosEnter
          add = sc.days[dados.dia-1]
          add.events = dados.events

      diaMes1 = $filter('date')(date, 'yyyy-MM')
      diaMes2 = $filter('date')(sc.hoje, 'yyyy-MM')
      if diaMes1 == diaMes2
        diaHoje = $filter('date')(sc.hoje, 'dd')
        sc.days[parseInt(diaHoje)-1].hoje = true

      if falta != 42
        for day in [1..(42-falta)]
          sc.nextDays.push
            id:id
            day:day
          id+=1

      if sc.dadosEnterNex
        for dados in sc.dadosEnterNex
          if dados.dia <= (42-falta)
            add = sc.nextDays[dados.dia-1]
            add.events = dados.events

    sc.DadosEventosIndex = (mesAtual)->
      for dados in DadosEventos.eventos
        if parseInt(dados.mes) == parseInt(mesAtual)
          return dados.evento
      return []

    sc.findId = (date)->
      select = $filter('date')(date, 'EEEE')
      for i in [0..6]
        if sc.meses[i] == select
          return i
      return 0
    
    sc.addRegister = ()->
      if sc.addNewRegister.assunto != ''
        month = $filter('date')(sc.monthPickerDate, 'MM')
        add = sc.addNewRegister
        add.tipo = 5
        add.status = false
        DadosEventos.pushEvents(add, parseInt(month), sc.addNewRegisterDay.day)
        sc.addNewRegister = []
      
        sc.days = []
        sc.nextDays = []
        sc.prevDays = []
        sc.dadosEnter = []
        sc.numDays = 0
        sc.atualizeData(sc.monthPickerDate)
        sc.boxConf.modal.close()
      sc.addNewRegister.assunto = ''

    sc.cancellRegister = ()->
      sc.boxConf.modal.close()
      sc.addNewRegister.assunto = ''
]

.controller 'EventoCtrl', [
  '$scope', 'DadosEventos', '$filter'
  (sc, DadosEventos, $filter)->

    sc.fastRegister = (obj, eventMouse)->
      if obj.events?.length > 0
        sc.verMais(obj)
        sc.ver_mais.open()
      else
        sc.boxConf.modal.close()
        setTimeout ()->
          sc.$apply ()->
            sc.boxConf.modal.open()
            sc.boxConf.left = '' + (eventMouse.pageX-($('#box').outerWidth(true) / 2))
            sc.boxConf.top = '' + eventMouse.pageY
            sc.boxConf.display = 'block'
            sc.addNewRegister = angular.extend {}, obj
            sc.addNewRegister.day = obj.day
            sc.addNewRegisterDay.day = obj.day
]

.factory 'DadosEventos', [
  ()->
    dados =
      pushEvents: (eventEnter, month, day)->
        for i in dados.eventos
          if i.mes == month
            for e in i.evento
              if e.dia == day
                e.events.push eventEnter
                return
            i.evento.push
              dia: day
              events: []
            newEvent = i.evento[i.evento.length-1]
            newEvent.events.push eventEnter
            return
        dados.eventos.push
          mes:month
          evento: [
            dia: day
            events: eventEnter
          ]
      removeEvent: (eventIn, month, day)->
        for e in dados.eventos
          if parseInt(e.mes) == parseInt(month)
            for i in e.evento
              if i.dia == day
                i.events.splice i.events.indexOf(eventIn), 1

      eventos: [
        {
          mes:9
          evento: [
            {
              dia:3
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
              ]
            }
            {
              dia:10
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:false
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:true
                }
              ]
            }
            {
              dia:22
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:false
                }
              ]
            }
            {
              dia:30
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:true
                }
              ]
            }
          ]
        }
        {
          mes:10
          evento: [
            {
              dia:1
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
              ]
            }
            {
              dia:5
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
              ]
            }
            {
              dia:10
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:false
                }
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:false
                }
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
              ]
            }
            {
              dia:20
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:false
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:true
                }
              ]
            }
          ]
        }
        {
          mes:11
          evento: [
            {
              dia:1
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
              ]
            }
            {
              dia:5
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:true
                }
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
              ]
            }
            {
              dia:12
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
                {
                  assunto: 'Visita'
                  tipo:5
                  status:true
                }
                {
                  assunto: 'Aniversário'
                  tipo:2
                  status:true
                }
              ]
            }
            {
              dia:18
              events: [
                {
                  assunto: 'Manutenção'
                  tipo:1
                  status:true
                }
              ]
            }
          ]
        }
      ]
    dados
]

$('#alt').click ()->
  $("html, body").animate({ scrollTop: 0 }, 0)