angular.module 'app', [
  'sc.app.helpers'
]

.controller 'LancamentosCtrl', [
  '$scope', 'LancamentosService', '$window', '$scModal', '$element'
  (sc, LancamentosService, win, $scModal, element)->

    sc.hoje = new Date()
    sc.novoRegistro = []

    registroDefault =
      _new: true
      acc: true
      titulo: 'Novo Lançamento'
      bomPara: new Date()
      bomPara2: new Date()

    sc.config =
      criterioRateio: ['Fração Ideal', 'Fração Igual', 'Fração Percentual', 'Fração pela Área', 'Multiplicar por:']
      status: [
        {nome: 'Aberto', active: true}
        {nome: 'Reaberto', active: true}
        {nome: 'Gerado'}
        {nome: 'Pausado'}
        {nome: 'Excluído'}
      ]
      menu: [
        {
          icon: 'sc-icon2 sc-icon2-cogs'
          name: 'Gerar'
        }
        {
          icon: 'sc-icon sc-icon-lapis'
          name: 'Editar'
        }
        {
          icon: 'sc-icon sc-icon-lixeira-2'
          name: 'Excluir'
        }
      ]
      destinatarios: ['Morador', 'Proprietário']
      repeticao: ['Quinzenal', 'Mensal', 'Semanal', 'Diario', 'Semestral']
      modRepeticao: [{id:0, name:'Qtde definida'}, {id:1, name:'Até a data'}, {id:2, name:'Indefinido'}, {id:3, name:'Parcelado', fMvp:true}]
    
    sc.novoLancamento = ()->
      sc.novoRegistro = angular.extend {}, registroDefault unless sc.novoRegistro?._new
      sc.initNew(sc.novoRegistro)

    sc.planoDeContas1 = [
      {
        plano: '1 Ativo'
      }
      {
        plano: '1.1 Ativo Circulante'
      }
      {
        plano: '1.1.1 Disponibilidades'
      }
      {
        plano: '1.1.1.1 Caixa'
      }
      {
        plano: '1.1.1.2 Bancos'
      }
    ]
  
    sc.planoDeContas2 = [
      {
        plano: '1 Receitas'
      }
      {
        plano: '1.1 Cotas condominiais'
      }
      {
        plano: '1.1.1 Taxas de Condomínio'
      }
      {
        plano: '1.1.2 Taxas de utilização de área comum'
      }
    ]

    sc.initNew = (obj)->
      sc.form = obj
      sc.form._openCadastro = true
      sc.form._openComposicaoC = true
      sc.form._inputs = []
      sc.form.pagadores = []
      sc.form.parcelados = []
    
    sc.addFormParc = (obj)->
      console.log 'addFormParc'
      console.log obj
      obj.parcelados = []
      for i in [0..(obj.qtd-1)]
        obj.parcelados.push
          valor: 0

    sc.toggleAccPagadores = (pag)->
      p.acc = false for p in sc.pagadores unless pag.acc
      pag.acc = !pag.acc
    
    sc.pagadores = LancamentosService.pagadores
]

.controller 'LancamentoCtrl', [
  '$scope', 'LancamentosService', '$scModal'
  ($scope, LancamentosService, $scModal)->

    $scope.initForm = (obj)->
      $scope.form = obj
      $scope.form._openCadastro = true
      $scope.form._openComposicaoC = true
      $scope.form._inputs = []
      $scope.form.pagadores = []
      $scope.form.parcelados = []

    $scope.actionMenu = (obj, item)->
      console.log 'actionMenu'

    $scope.removeLancamento = (obj) ->
      $scope.form._inputs.splice($scope.form._inputs.indexOf(obj), 1)

    $scope.removeLancamentoSub = (numC, obj) ->
      numC.splice(numC.indexOf(obj), 1)

    $scope.addLancamento = () ->
      $scope.form._inputs.push
        num: $scope.form._inputs.length
        nome: 'Novo Rateio'
        valor: 0
        planoContas: 1
        subComposicao: [
          {
            num: 0
            nome: 'Título'
            valor: 0
          }
        ]

    $scope.addLancamentoSub = (numC) ->
      numC.subComposicao.push
        num: 0
        nome: 'Título'
        valor: 0

    $scope.setPagador = (pagador)->
      $scope.form.pagadores ||= []
      $scope.form.pagadores.push {pessoa: pagador}
]

.factory 'LancamentosService', [
  ()->
    lanc =
      pagadores: [
        {
          id: 1
          nome: 'Parcela das câmeras'
          bomPara: new Date()
          pagador: 'Torre 01 - 101'
          status: 'Aberto'
          programacao:true
          composicao: []
        }
        {
          id: 2
          nome: 'Programação Churrasqueira'
          bomPara: new Date()
          pagador: 'Torre 01 - 102'
          status: 'Gerado'
          composicao: []
        }
        {
          id: 3
          nome: 'Taxa de área comum'
          bomPara: new Date()
          pagador: 'Torre 02 - 201'
          status: 'Excluido'
          composicao: []
        }
      ]
    lanc
]