angular.module 'GaleriaApp', [
  'sc.app.helpers'
]

.controller 'GaleriaCtrl', [
  '$scope', '$scModal', 'DadosEventos'
  (sc, $scModal, DadosEventos)->

    sc.novo_album = new $scModal()

    sc.eventos = DadosEventos.eventos
    sc.novoAlbum = {}
    sc.albumTmp = [
      {
        title:'De boa no whats'
        image: 'http://i.imgur.com/d169Wp0.jpg'
      }
      {
        title:'Whats!!, ah!'
        image: 'http://i.imgur.com/KLTSowp.jpg'
      }
      {
        title:'Rhododendron Park!'
        image: 'http://i.imgur.com/JfKwovX.jpg'
      }
    ]

    sc.config = 
      menu: [
        {
          name:'Editar' 
          icon:'sc-icon sc-icon-lapis'
          iconColor:'sc-text-yellow'
        }
        {
          name:'Excluir'
          icon:'sc-icon sc-icon-lixeira-1'
          iconColor:'sc-text-red'
        }
      ]
      permissoes: [
        {
          id:0
          perm:'Conselheiros'
        }
        {
          id:1
          perm:'Familiares'
        }
        {
          id:2
          perm:'Proprietários'
        }
        {
          id:3
          perm:'Moradores'
        }
      ]

    sc.actionMenu = (obj, item)->
      if item.name == 'Editar'
        obj._edit = true 

    sc.novoAlbum = ()->
      sc.novoAlbum.galery = [
        {
          title:'De boa no whats'
          image: 'http://i.imgur.com/d169Wp0.jpg'
        }
        {
          title:'Whats!!, ah!'
          image: 'http://i.imgur.com/KLTSowp.jpg'
        }
        {
          title:'Rhododendron Park!'
          image: 'http://i.imgur.com/JfKwovX.jpg'
        }
      ]
      sc.novoAlbum.new = true
      sc.novo_album.close()

    sc.toggleAcc = (pag)->
      p.acc = false for p in sc.eventos unless pag.acc
      pag.acc = !pag.acc
]

.controller 'EventosCtrl', [
  '$scope'
  (sc)->
]

.controller 'EventoCtrl', [
  '$scope'
  (sc)->
    sc.init = (obj)->
      sc.form = obj
      sc.selectForm = sc.form.galery[0]
      sc.form.galery[0].select = true
]

.factory 'DadosEventos', [
  ()->
    even = 
      eventos: [
        {
          name:'Evento 1'
          date:'2015-10-05'
          galery: [
            {
              title:'De boa no whats'
              image: 'http://i.imgur.com/d169Wp0.jpg'
              select:false
            }
            {
              title:'Whats!!, ah!'
              image: 'http://i.imgur.com/KLTSowp.jpg'
              select:false
            }
            {
              title:'Rhododendron Park!'
              image: 'http://i.imgur.com/JfKwovX.jpg'
              select:false
            }
            {
              title:'Buuh'
              image:'http://i.imgur.com/UTrTgyM.jpg'
              select:false
            }
            {
              title:'Tirando a Pedra'
              image:'http://i.imgur.com/hugRif8b.jpg'
              select:false
            }
            {
              title:'Poxa...'
              image:'http://i.imgur.com/s87ouoab.jpg'
              select:false
            }
          ]
        }
        {
          name:'Evento 2'
          date:'2015-09-05'
          galery: [
            {
              title:'De boa no whats'
              image: 'http://i.imgur.com/d169Wp0.jpg'
              select:false
            }
            {
              title:'Whats!!, ah!'
              image: 'http://i.imgur.com/KLTSowp.jpg'
              select:false
            }
            {
              title:'Rhododendron Park!'
              image: 'http://i.imgur.com/JfKwovX.jpg'
              select:false
            }
            {
              title:'Buuh'
              image:'http://i.imgur.com/UTrTgyM.jpg'
              select:false
            }
            {
              title:'Tirando a Pedra'
              image:'http://i.imgur.com/hugRif8b.jpg'
              select:false
            }
            {
              title:'Poxa...'
              image:'http://i.imgur.com/s87ouoab.jpg'
              select:false
            }
          ]
        }
        {
          name:'Evento 3'
          date:'2015-08-05'
          galery: [
            {
              title:'De boa no whats'
              image: 'http://i.imgur.com/d169Wp0.jpg'
              select:false
            }
            {
              title:'Whats!!, ah!'
              image: 'http://i.imgur.com/KLTSowp.jpg'
              select:false
            }
            {
              title:'Rhododendron Park!'
              image: 'http://i.imgur.com/JfKwovX.jpg'
              select:false
            }
            {
              title:'Buuh'
              image:'http://i.imgur.com/UTrTgyM.jpg'
              select:false
            }
            {
              title:'Tirando a Pedra'
              image:'http://i.imgur.com/hugRif8b.jpg'
              select:false
            }
            {
              title:'Poxa...'
              image:'http://i.imgur.com/s87ouoab.jpg'
              select:false
            }
          ]
        }
        {
          name:'Evento 4'
          date:'2015-07-05'
          galery: [
            {
              title:'De boa no whats'
              image: 'http://i.imgur.com/d169Wp0.jpg'
              select:false
            }
            {
              title:'Whats!!, ah!'
              image: 'http://i.imgur.com/KLTSowp.jpg'
              select:false
            }
            {
              title:'Rhododendron Park!'
              image: 'http://i.imgur.com/JfKwovX.jpg'
              select:false
            }
            {
              title:'Buuh'
              image:'http://i.imgur.com/UTrTgyM.jpg'
              select:false
            }
            {
              title:'Tirando a Pedra'
              image:'http://i.imgur.com/hugRif8b.jpg'
              select:false
            }
            {
              title:'Poxa...'
              image:'http://i.imgur.com/s87ouoab.jpg'
              select:false
            }
          ]
        }
      ]
    even
]