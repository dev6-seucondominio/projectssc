angular.module 'app', [
  'sc.app.helpers'
]

.controller 'Recebiemntos::indexCtrl', [
  '$scope', "$filter"
  (sc, $filter)->

    sc.cobranca =
      valor: 95
      valorDev: 95
      juros: 0.03
      multa: 2
      totalComp: 95
      totalRec: 0
      atualizado: 0
      ajuste: 0
      cadastro:
        ref: 'Nov/2015'
        nome: 'Fulano de tal'
        pagador: 'Fulano de tal'
        criadoEm: '2015-09-12'
        vencimento: '2015-10-12'
        destinatatio: 'Fulano de Tal'
        confCobranca: 'Cobrança Mensal'
      composicaoCobranca: [ {
          valor: 95
          titulo: 'Multa por barulho'
      } ]
      recebimentos: []

    sc.composicao =
      valor: 0
      titulo: ''

    data1 =
      dia: $filter('date')(new Date(), 'dd')
      mes: $filter('date')(new Date(), 'MM')
      ano: $filter('date')(new Date(), 'yyyy')
    data2 =
      dia: $filter('date')(sc.cobranca.cadastro.vencimento, 'dd')
      mes: $filter('date')(sc.cobranca.cadastro.vencimento, 'MM')
      ano: $filter('date')(sc.cobranca.cadastro.vencimento, 'yyyy')

    sc.dias_mes = [
      [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
      [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    ]

    atualizaJuros = ()->
      data1 =
        dia: $filter('date')(sc.receber.data, 'dd')
        mes: $filter('date')(sc.receber.data, 'MM')
        ano: $filter('date')(sc.receber.data, 'yyyy')
      if sc.cobranca.recebimentos.length > 0
        data2 =
          dia: $filter('date')(sc.cobranca.recebimentos[sc.cobranca.recebimentos.length-1].data, 'dd')
          mes: $filter('date')(sc.cobranca.recebimentos[sc.cobranca.recebimentos.length-1].data, 'MM')
          ano: $filter('date')(sc.cobranca.recebimentos[sc.cobranca.recebimentos.length-1].data, 'yyyy')
      sc.receber.juros = sc.cobranca.valorDev * 0.0003 * calcDias(data2, data1)
    
    sc.$watch 'receber.data', ()->
      atualizaJuros()
      sc.receber.multa = sc.cobranca.valorDev * 0.02 if sc.cobranca.recebimentos.length == 0
    sc.$watch 'cobranca.valor', ()->
      atualizaJuros()
      sc.receber.multa = sc.cobranca.valorDev * 0.02 if sc.cobranca.recebimentos.length == 0

    sc.addRec = ()->
      if sc.receber.valor != 0
        data1 =
          dia: $filter('date')(sc.receber.data, 'dd')
          mes: $filter('date')(sc.receber.data, 'MM')
          ano: $filter('date')(sc.receber.data, 'yyyy')
        if sc.cobranca.recebimentos.length > 0
          data2 =
            dia: $filter('date')(sc.cobranca.recebimentos[sc.cobranca.recebimentos.length-1].data, 'dd')
            mes: $filter('date')(sc.cobranca.recebimentos[sc.cobranca.recebimentos.length-1].data, 'MM')
            ano: $filter('date')(sc.cobranca.recebimentos[sc.cobranca.recebimentos.length-1].data, 'yyyy')
        sc.cobranca.recebimentos.push
          valor: sc.receber.valor
          data: sc.receber.data
          multa: sc.receber.multa
          juros: sc.receber.juros
        sc.cobranca.atualizado = sc.calcularJuros()
        sc.cobranca.valorDev = sc.cobranca.atualizado
        sc.cobranca.totalRec += sc.receber.valor
        sc.cobranca.totalComp += (sc.receber.juros + sc.receber.multa)
        sc.receber =
          data: ''
          valor: 0
          juros: 0
          multa: 0

    sc.addComp = ()->
      if sc.composicao.valor != 0 && sc.composicao.titulo != ''
        sc.cobranca.composicaoCobranca.push
          valor: sc.composicao.valor
          titulo: sc.composicao.titulo
        sc.cobranca.valor += sc.composicao.valor
        sc.cobranca.valorDev += sc.composicao.valor
        sc.cobranca.atualizado += sc.composicao.valor if sc.cobranca.atualizado > 0
        sc.cobranca.totalComp += sc.composicao.valor
        sc.composicao =
          titulo: ''
          valor: 0

    sc.calcularJuros = ()->
      sc.cobranca.composicaoCobranca.push
        titulo: 'Multa (' + $filter('date')(sc.receber.data, 'dd/MM/yyyy') + ')'
        valor: sc.receber.multa
      sc.cobranca.composicaoCobranca.push
        titulo: 'Juros (' + $filter('date')(sc.receber.data, 'dd/MM/yyyy') + ')'
        valor: sc.receber.juros
      
      ((sc.cobranca.valorDev + sc.receber.multa + sc.receber.juros) - sc.receber.valor)

    calcDias = (date1, date2)->
      idias = parseInt date1.dia
      fdias = parseInt date2.dia
      
      mes1 = date1.mes - 1
      dbissexto = bissexto(date1.ano)
      for i in [mes1..0]
        idias += sc.dias_mes[dbissexto][i]

      mes2 = date2.mes - 1
      dbissexto = bissexto(date2.ano)
      for i in [mes2..0]
        fdias += sc.dias_mes[dbissexto][i]

      def_anos = 0
      while date1.ano < date2.ano
        def_anos += 365 + bissexto date1.ano++

      (def_anos - idias + fdias)

    bissexto = (ano)->
      if (ano % 4 == 0) && ((ano % 100 != 0) || (ano % 400 == 0))
        return 1
      return 0

    sc.receber =
      data: new Date()
      valor: 0
      juros: sc.cobranca.valorDev * 0.0003 * calcDias(data2, data1)
      multa: sc.cobranca.valorDev * 0.02
]