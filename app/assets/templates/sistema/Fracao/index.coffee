angular.module 'app', [ 
  'sc.app.helpers'
]

.controller 'AppCtrl', [
  '$scope', '$scModal', '$filter'
  (sc, $scModal,$filter)->
    
    sc.selected = []
    sc.actionAlt = {}
    sc.cadastro = {}
    sc.actions     = {}
    sc.editMedidorObj = {}
    sc.addMedidor  = {}

    
    sc.medidores = [
      {
        id: 0
        name:'Água'
        unidade:'m³'
        valor: 1.5
        unidadesDesativadas:'101'
      }
      {
        id: 1
        name:'Gás'
        unidade:'cm³'
        valor: 0.25
        unidadesDesativadas:'102'
      }
      {
        id: 2
        name:'Energia'
        unidade:'kWh'
        valor: 3.2
        unidadesDesativadas:'103'
      }
    ]

    sc.fracoes = [
      {
        title:'Fração Igual'
        id:0
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
        ]
      }
      {
        title:'Fração Ideal'
        id:0
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-10-01'
            valor:1100.90
            proporcao:0.5
          }
        ]
      }
    ]

    sc.mesReferencia = ()->
      $filter('date')(sc.monthPickerDate, 'yyyy-MM')

    sc.toggleAcc = (obj)->
      i.opened = false for i in sc.fracoes if !obj.opened
      obj.opened = !obj.opened
    
    sc.selectLeitura = (leitura)->
      if !leitura.select
        sc.selected = angular.extend {}, leitura
        i.select = false for i in sc.fracoes
        leitura.select = true
      else
        sc.selected = []
        leitura.select = false
        
    sc.totalConsumo = ()->
      soma = 0
      if sc.selected.unidades
        for select in sc.selected.unidades
          soma+=(select.atual - select.anterior)
      soma
      
    sc.totalAPagar = ()->
      soma = 0
      if sc.selected.unidades
        for select in sc.selected.unidades
          soma+=((select.atual - select.anterior) * sc.medidores[sc.selected.id].valor)
      soma

    sc.iniciarCadastro = ()->
      add = sc.cadastro
      date = $filter('date')(new Date(), 'yyyy-MM-dd')
      
      add.date = date
      add.null = true

      addBase = add.idBase
      add.unidades = []
      add = add.unidades
      for unidade in sc.unidades
        addAnterior = sc.fracoes[addBase].unidades[sc.unidades.indexOf(unidade)].anterior
        add.push
          title:unidade.title
          date: date
          anterior: addAnterior
      sc.actionAlt.addNovaLeitura = true
      sc.actionAlt.novaLeituraOpen = !sc.actionAlt.novaLeituraOpen

]

.controller 'fracoesCtrl', [
  '$scope', '$filter'
  (sc, $filter)->

    sc.selectionOption = (medidor)->
      a.select = false for a in sc.medidores
      medidor.select = true
      sc.cadastro.title = medidor.name
      sc.cadastro.id = sc.medidores.indexOf(medidor)

    sc.selectionLeitura = (leitura)->
      a.select = false for a in sc.fracoes
      leitura.select = true
      sc.cadastro.idBase = sc.fracoes.indexOf(leitura)
      sc.cadastro.base = leitura.title
      
    sc.salveCadastro = ()->
      add = sc.cadastro
      sc.fracoes.push
        title: add.title
        id: add.id
        date: add.date
        idBase: add.idBase
        null: add.null
      addBase = add.idBase
      sc.fracoes[sc.fracoes.length-1].unidades = []
      addNew = sc.fracoes[sc.fracoes.length-1].unidades
      for unidade in add.unidades
        addNew.push
          title:unidade.title
          data: unidade.date
          atual: parseInt(unidade.atual) || 0
          anterior: unidade.anterior
      sc.actionAlt.addNovaLeitura = false
    
    sc.cancelarCadastro = ()->
      sc.actionAlt.addNovaLeitura = false
      
]