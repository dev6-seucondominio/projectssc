angular.module 'myApp', [
  'sc.app.helpers'
]

.controller 'AppCtrl', [
  '$scope', '$filter'
  ($scope, $filter)->
    
    $scope.contaSelected = { id: 0 }
    
    $scope.init = ()->
      for i in [111...116]
        $scope.contas.push
          id: $scope.contas.length
          cc: i
    
    $scope.contas = []
  
    $scope.mes = '1988-02-01'

    monthNames = ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN", "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"]

    $scope.saldoConta =
      data:
        x: 'mes'
        columns: [
          ['mes', '2015-01-01', '2015-02-01', '2015-03-01', '2015-04-01', '2015-05-01', '2015-06-01']
          ['dados',  1400.52, 1081.01, 1852.80, 2298.51, 2032.63, 3074.91, 3891.94]
        ]
        names:
            dados: 'Saldo'
        colors:
          dados: '#6aade6'
          columns: 'red'
        type: 'area-spline'

      config:
        axis:
          x:
            type: 'timeseries',
            tick:
              format: (date)-> monthNames[date.getMonth()]
        point:
          r: 4
        grid:
          x:
            show: true
        legend:
            show: false
          
    $scope.ocorrencias =
      data:
        columns: [
          ['resolvidas',  15]
          ['naoResolvidas',  4]
        ]
        names:
            resolvidas: 'Resolvidas'
            naoResolvidas: 'Não Resolvidas'
        colors:
          resolvidas: '#6aade6'
          naoResolvidas: '#f38282'
        type: 'donut'
      config:
        donut:
          width: 50

    $scope.usuariosAcessaram =
      data:
        columns: [
          ['acessos', 83] # 83%
          ['resto', 17]   # 17%
        ]
        colors:
          acessos: '#FFF'
          resto: 'rgba(255,255,255,.2)'
        type: 'donut'

    $scope.encomendasEntregues =
      data:
        columns: [
          ['entregues', 50]
          ['resto', 50]
        ]
        colors:
          entregues: '#FFF'
          resto: 'rgba(255,255,255,.2)'
        type: 'donut'

    $scope.configDonutCards =
      donut:
        width: 2
        label:
          show: false
      legend:
        show: false
      tooltip:
        show: false
      size:
        width: 80
        height: 80

    $scope.contasCondominio =
      data:
        columns: [
          ['pagas',  15]
          ['aPagar',  6]
          ['vencidas',  3]
        ]
        names:
          pagas: 'Pagas'
          aPagar: 'A Pagar'
          vencidas: 'Vencidas'
        colors:
          pagas: '#7cdf7c'
          aPagar: '#ffb94c'
          vencidas: '#f38282'
        type: 'donut'
        tooltipTemplate: "<%= value %>",
        onAnimationComplete: ()->
          this.showTooltip(this.segments, true);
        , tooltipEvents: [],
      config:
        showTooltips: true
        donut:
          width: 50
     
    $scope.contasCondominio2 =
      data:
        columns: [
          ['pagas',  15, 10, 6, 5]
          ['aPagar',  6, 10, 3, 9]
          ['vencidas',  3, 8, 5, 3]
        ]
        names:
            pagas: 'Pagas'
            aPagar: 'A Pagar'
            vencidas: 'Vencidas'
        colors:
          pagas: '#7cdf7c'
          aPagar: '#ffb94c'
          vencidas: '#f38282'
        type: 'bar'
        groups: [
          ['pagas', 'aPagar', 'vencidas']
        ]
      config:
        axis:
          rotated: true
        bar:
          widht: 50
        
    $scope.contasCondominio3=
      data:
        columns: [
          ['pagas',  15, 10, 6, 5]
          ['aPagar',  6, 10, 3, 9]
          ['vencidas',  3, 8, 5, 3]
        ]
        names:
            pagas: 'Pagas'
            aPagar: 'A Pagar'
            vencidas: 'Vencidas'
        colors:
          pagas: '#7cdf7c'
          aPagar: '#ffb94c'
          vencidas: '#f38282'
        type: 'bar'
        groups: [
          ['pagas', 'aPagar', 'vencidas']
        ]
      config:
        bar:
          widht: 50
        
    $scope.resumoPortaria =
      data:
        columns: [
          ['encomendas',  3, 8, 5, 3]
          ['ocorrencias',  6, 10, 3, 9]
          ['visitantes',  15, 10, 6, 5]
        ]
        names:
          encomendas: 'Encomendas'
          ocorrencias: 'Ocorrências'
          visitantes: 'Visitantes'
        colors:
          visitantes: '#ffb94c'
          encomendas: '#6aade6'
          ocorrencias: '#7cdf7c'
        type: 'bar'
        groups: [
          ['ocorrencias', 'encomendas', 'visitantes']
        ]
      config:
        axis:
          rotated: true
          x:
            type: 'category',
            categories: ['porteiro 1', 'porteiro 2', 'porteiro 3', 'porteiro 4']
          y:
            show: false
        bar:
          widht: 50
          
    $scope.votacao =
      data:
        columns: [
          ['Número de votos',  3, 4,  6, 15]
        ]
        type: 'bar'
      config:
        legend:
          show: false
        axis:
          rotated: true

    $scope.consumo =
      data:
        x: 'mes'
        columns: [
          ['mes', '2015-01-01', '2015-02-01', '2015-03-01', '2015-04-01', '2015-05-01', '2015-06-01', '2015-07-01', '2015-08-01', '2015-09-01']
          ['dados1',  1822.19, 2281.01, 1152.80, 2998.51, 2032.63, 3233.91, 3691.94, 1000.52, 2032.63]
          ['dados2',  1412.21, 3281.01, 1352.80, 2198.51, 2332.63, 3074.91, 3491.94, 1400.52, 1222.63]   
          ['dados3',  3402.21, 1281.01, 1642.80, 2298.51, 2932.63, 3374.91, 3891.94, 1200.52, 2322.63]    
          ['dados4',  2360.21, 2281.01, 1752.80, 2098.51, 2022.63, 3474.91, 2032.94, 1100.52, 2922.63]           
        ]
        names:
          dados1: 'Energia'
          dados2: 'Água'
          dados3: 'Gás'
          dados4: 'Outros'
        colors:
          dados1: '#6aade6'
          dados2: '#f38282'
          dados3: '#ffb94c'
          dados4: '#7cdf7c'
        type: 'bar'
      config:
        bar:
          width:
            ratio: 0.5
        axis:
          x:
            type: 'timeseries',
            tick:
              format: (date)-> monthNames[date.getMonth()]
        grid:
          x:
            show: true
        
    monthNames2 = ['J', 'F', 'M', 'A', 'M', 'J', 'J', 'A', 'S', 'O', 'N', 'D']
        
    $scope.previsao =
      data:
        x: 'mes'
        columns: [
          ['mes', '2015-01-01', '2015-02-01', '2015-03-01', '2015-04-01', '2015-05-01', '2015-06-01']
          ['dados',  1400, 1081, 1852, 2298, 2032, 3074, 3891]
        ]
        names:
            dados: 'Previsões: '
        colors:
          dados: '#6aade6'
        type: 'bar'

      config:
        axis:
          x:
            type: 'timeseries',
            tick:
              format: (date)-> monthNames2[date.getMonth()]
          y:
            type: 'currency',
            tick:
              format: (date)-> 'R$ ' + date
        point:
          r: 4
        legend:
            show: false
          
    $scope.cobrancas = 
      data:
        x: 'mes'
        columns: [
          ['mes', '2015-01-01', '2015-02-01', '2015-03-01', '2015-04-01', '2015-05-01', '2015-06-01', '2015-07-01', '2015-08-01', '2015-09-01', '2015-10-01', '2015-11-01', '2015-12-01']
          ['dados1',  1400, 1081, 1852, 2298, 2032, 3074, 3891, 1400, 1081, 1852, 2298, 2032]
          ['dados2',  1300, 2080, 2500, 2300, 1000, 2800, 3500, 1300, 2080, 2500, 2300, 1000]
        ]
        names:
            dados1: 'Inadimplência: '
            dados2: 'Recebimentos: '
        types:
            dados1: 'spline',
            dados2: 'bar',
        colors:
          dados1: '#f38282'
          dados2: '#7cdf7c'
      config:
        axis:
          x:
            type: 'timeseries',
            tick:
              format: (date)-> monthNames2[date.getMonth()]
          y:
            show: false
        point:
          r: 3
        legend:
            show: false

    $scope.pagamentos = 
      data:
        x: 'mes'
        columns: [
          ['mes', '2015-01-01', '2015-02-01', '2015-03-01', '2015-04-01', '2015-05-01', '2015-06-01', '2015-07-01', '2015-08-01', '2015-09-01', '2015-10-01', '2015-11-01', '2015-12-01']
          ['dados1',  1400, 1081, 1852, 2298, 2032, 3074, 3891, 1400, 1081, 1852, 2298, 2032]
          ['dados2',  1300, 2080, 2500, 2300, 1000, 2800, 3500, 1300, 2080, 2500, 2300, 1000]
        ]
        names:
            dados1: 'Constas a pagar: '
            dados2: 'Pagamentos: '
        types:
            dados1: 'spline',
            dados2: 'bar',
        colors:
          dados1: '#f38282'
          dados2: '#438ccb'
      config:
        axis:
          x:
            type: 'timeseries',
            tick:
              format: (date)-> monthNames2[date.getMonth()]
          y:
            show: false
        point:
          r: 3
        legend:
            show: false
        
    $scope.periodicos = [
      {data: new Date("2015-10-23"), nome: "Pintura do Portão"}
      {data: new Date("2015-10-25"), nome: "Pagamento de funcionarios"}
      {data: new Date("2015-11-5"), nome: "Recebimento de taixa de Condomínio"}
      {data: new Date("2015-11-18"), nome: "Aniversário do Síndico"}
    ]

    $scope.circulares = [
      {data: new Date("2015-9-5"), nome: "Manutenção nos elevadores"}
      {data: new Date("2015-9-8"), nome: "Reforma da fachada"}
      {data: new Date("2015-9-16"), nome: "Pintura das vagas da garagem"}
      {data: new Date("2015-9-21"), nome: "Falta de água"}
    ]

    $scope.reservas = [
      {nome: "Salão de festa", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'disponivel'}
      {nome: "Salão de Jogos", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'disponivel'}
      {nome: "Piscina", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'disponivel'}
      {nome: "Área de churrasco", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'indisponivel'}
    ]
          
    $scope.achadoPerdido = [
      {nome: "Celular Moto G", conteudo: "Perdi meu celular perto da área de lazer.", semana:'segunda-feira', horario:'12:00', status:'perdido'}
      {nome: "Guarda Chuva", conteudo: "Achei em cima da bancada da churrascaria.", semana:'segunda-feira', horario:'12:00', status:'achado'}
      {nome: "Bola de futebol", conteudo: "Achei na quadra de futebol.", semana:'segunda-feira', horario:'12:00', status:'achado'}
      {nome: "Kite de ping-pong", conteudo: "Deixei na área de jogos.", semana:'segunda-feira', horario:'12:00', status:'perdido'}
    ]
  
    $scope.emprestimos = [
      {nome: "Chave do Salão de Festas", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'disponivel'}
      {nome: "Chave Salão de Jogos", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'disponivel'}
      {nome: "Bola de Futebol", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'disponivel'}
      {nome: "Kite de ping-pong", conteudo: "Torre 1 - 101 (João Pedro)", semana: "Segunda-Feira", h1: "14:00", h2: "19:00", status:'indisponivel'}
    ]

    $scope.estoques = [
      {nome: "Bol ade Futebol", horario:'12:00', valor:-20}
      {nome: "Bola de Voley", horario:'12:00', valor:20}
      {nome: "Detergente", horario:'12:00', valor:20}
      {nome: "Sabão", horario:'12:00', valor:-20}
    ]

    $scope.mural = [
      {foto: "http://lorempixel.com/50/50/people/9", nome: "Angelina Dumont",  conteudo: "Cum sociis natoque penatibus et magnis dis parturient montes"}
      {foto: "http://lorempixel.com/50/50/people/7", nome: "Juliana Pães :D",  conteudo: "Ut vitae lacus sem ellentesque maximus, nunc sit amet varius dignissim, dui est consectetur neque"}
      {foto: "http://lorempixel.com/50/50/people/3", nome: "Jonathan Morris",  conteudo: "Nunc quis diam diamurabitur at dolor elementum, dictum turpis vel"}
      {foto: "http://lorempixel.com/50/50/people/6", nome: "Mary Belle",       conteudo: "Proin laoreet commodo eros id faucibus. Donec ligula quam, imperdiet vel ante placerat"}
    ]

    $scope.documentos = [
      {nome: "Ata da assembléia 12/04/2015", formato: "doc"}
      {nome: "Balancete", formato: "pdf"}
      {nome: "Novas regras de portaria", formato: "jpg"}
      {nome: "Balancete Out", formato: "pdf"}
    ]

]

.directive 'chart', ->
  restrict: 'E'
  scope:
    data: '='
    config: '='
  link: (scope, elem)->
    drawChart = (data)->
      c3Obj =
        bindto: elem[0]
        data: scope.data

      if scope.config
        c3Obj = angular.extend c3Obj, scope.config

      c3.generate c3Obj

    scope.lineChart = false
    scope.$watch 'data', ((data)->
      if scope.lineChart
        scope.lineChart.load data
      else
        scope.lineChart = drawChart(data)
    ), true
