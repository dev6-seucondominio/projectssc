angular.module 'app', [
  'sc.app.helpers'
]

.factory 'LancamentosService', [
  ()->
    lanc =
      somaTotal: (obj)->
        count = 0
        for o in obj
          count++ for a in o.lancamentos
        count

      somaQTAberto: (obj)->
        count = 0
        for o in obj
          count++ for a in o.lancamentos when a.status == 'Aberto'
        count

      somaQTReaberto: (obj)->
        count = 0
        for o in obj
          count++ for a in o.lancamentos when a.status == 'Reaberto'
        count

      somaQTGerado: (obj)->
        count = 0
        for o in obj
          count++ for a in o.lancamentos when a.status == 'Gerado'
        count

      somaQTExcluido: (obj)->
        count = 0
        for o in obj
          count++ for a in o.lancamentos when a.status == 'Excluido'
        count

      qtdSelecionados: (obj)->
        count = 0
        count++ for o in obj when o.checked == true
        count

      listaPagadores: [
        {id: 1,nome: 'Torre 01 - 101'}
        {id: 2,nome: 'Torre 01 - 102'}
        {id: 3,nome: 'Torre 01 - 201'}
        {id: 4,nome: 'Torre 01 - 202'}
        {id: 5,nome: 'Torre 02 - 101'}
        {id: 6,nome: 'Torre 02 - 102'}
        {id: 7,nome: 'Torre 02 - 201'}
        {id: 8,nome: 'Torre 02 - 202'}
        {id: 9,nome: 'Apoema'}
        {id: 10,nome: 'Belcor Seguros'}
        {id: 11,nome: 'Bonavita'}
        {id: 12,nome: 'Beltrano de Oliveira 023.456.245-84'}
        {id: 13,nome: 'Flor do Ipê'}
        {id: 14,nome: 'Taurus'}
        {id: 15,nome: 'Beltrano de Oliveira da Silva 145.630.124-03'}
      ]
      pagadores: [
        {
          id: 1
          nome: 'Torre 01 - 101'
          lancamentos: [
            {
              titulo: 'Rateio de Fevereiro'
              bomPara: new Date()
              pagador: 'Torre 01 - 101'
              status: 'Aberto'
              programacao:true
              composicao: []
            }
            {
              titulo: 'Churrasqueira'
              bomPara: new Date()
              pagador: 'Torre 01 - 101'
              status: 'Reaberto'
              composicao: []
            }
            {
              titulo: 'Salão de Festas'
              bomPara: new Date()
              pagador: 'Torre 01 - 101'
              status: 'Gerado'
              composicao: []
            }
            {
              titulo: 'Piscina'
              bomPara: new Date()
              pagador: 'Torre 01 - 101'
              status: 'Excluido'
              composicao: []
            }
            {
              titulo: 'Multa'
              bomPara: new Date()
              pagador: 'Torre 01 - 101'
              status: 'Pausado'
              composicao: []
            }
          ]
        }
        {
          id: 2
          nome: 'Torre 01 - 102'
          lancamentos: [
            {
              titulo: 'Rateio de Fevereiro'
              bomPara: new Date()
              pagador: 'Torre 01 - 102'
              status: 'Gerado'
              composicao: []
            }
            {
              titulo: 'Churrasqueira'
              bomPara: new Date()
              pagador: 'Torre 01 - 102'
              status: 'Aberto'
              composicao: []
            }
          ]
        }
        {
          id: 3
          nome: 'Torre 02 - 201'
          lancamentos: [
            {
              titulo: 'Rateio de Fevereiro'
              bomPara: new Date()
              pagador: 'Torre 02 - 201'
              status: 'Excluido'
              composicao: []
            }
            {
              titulo: 'Churrasqueira'
              bomPara: new Date()
              pagador: 'Torre 02 - 201'
              status: 'Aberto'
              composicao: []
            }
          ]
        }
        {
          id: 4
          nome: 'Belcor Seguros'
          lancamentos: [
            {
              titulo: 'Vistoria do Prédio'
              bomPara: new Date()
              pagador: 'Belcor Seguros'
              status: 'Aberto'
              composicao: []
            }
          ]
        }
        {
          id: 5
          nome: 'Beltrano de Oliveira da Silva 145.630.124-03'
          lancamentos: [
            {
              titulo: 'Rateio de Fevereiro'
              bomPara: new Date()
              pagador: 'Beltrano de Oliveira da Silva 145.630.124-03'
              status: 'Aberto'
              composicao: []
            }
            {
              titulo: 'Churrasqueira'
              bomPara: new Date()
              pagador: 'Beltrano de Oliveira da Silva 145.630.124-03'
              status: 'Aberto'
              composicao: []
            }
            {
              titulo: 'Salão de Festas'
              bomPara: new Date()
              pagador: 'Beltrano de Oliveira da Silva 145.630.124-03'
              status: 'Reaberto'
              composicao: []
            }
            {
              titulo: 'Piscina'
              bomPara: new Date()
              pagador: 'Beltrano de Oliveira da Silva 145.630.124-03'
              status: 'Aberto'
              composicao: []
            }
          ]
        }
        {
          id: 6
          nome: 'Flor do Ipê'
          lancamentos: [
            {
              titulo: 'Salão de Festas'
              bomPara: new Date()
              pagador: 'Flor do Ipê'
              status: 'Aberto'
              composicao: []
            }
          ]
        }
      ]
    lanc
]

.controller 'LancamentosCtrl', [
  '$scope', 'LancamentosService', '$window', '$scModal'
  (sc, LancamentosService, win, $scModal)->

    sc.modalTotalSelecionados = new $scModal()
    sc.marcarTodosModal2      = new $scModal()
    sc.marcarTodosModal       = new $scModal()
    sc.excluirModal           = new $scModal()
    sc.modalConfigBoleto      = new $scModal()
    sc.gerarCobranca          = new $scModal()
    sc.hoje                   = new Date
    sc.clikc_s                = {}
    sc.numComposicaoC         = []
    sc.scrollPos              = document.body.scrollTop || document.documentElement.scrollTop || 0
    sc.scrollPosBoll          = sc.scrollPos >= 23 ? true : false

    win.onscroll = ->
      sc.scrollPos = document.body.scrollTop || document.documentElement.scrollTop || 0
      if sc.scrollPos >= 23
        sc.scrollPosBoll = true
      else
        sc.scrollPosBoll = false
      sc.$apply()

    sc.filtro =
      aberto:''
      reaberto:''
      gerado:''
      exluido:''

    registroDefault =
      _new: true
      acc: true
      titulo: 'Novo Lançamento'
      bomPara: new Date()

    sc.novoLancamento = ->
      sc.novoRegistro = angular.extend {}, registroDefault unless sc.novoRegistro?._new

    sc.selecionados = ->
      LancamentosService.qtdSelecionados(sc.pagadores)

    sc.toggleAccPagadores = (pag)->
      p.acc = false for p in sc.pagadores unless pag.acc
      pag.acc = !pag.acc

    sc.somaTotal = ()->
      LancamentosService.somaTotal(sc.pagadores)

    sc.somas = (nome)->
      obj = sc.pagadores
      switch nome
        when 'Aberto' then LancamentosService.somaQTAberto(obj)
        when 'Reaberto' then LancamentosService.somaQTReaberto(obj)
        when 'Gerado' then LancamentosService.somaQTGerado(obj)
        when 'Excluido' then LancamentosService.somaQTExcluido(obj)

    sc.verificarSelesionados = (name)->
      if name ==  'Aberto'
        if sc.filtro.aberto == 'Aberto'
          sc.filtro.aberto = ''
        else
          sc.filtro.aberto = 'Aberto'
        return
      if name ==  'Reaberto'
        if sc.filtro.reaberto == 'Reaberto'
          sc.filtro.reaberto = ''
        else
          sc.filtro.reaberto = 'Reaberto'
        return
      if name == 'Gerado'
        if sc.filtro.gerado == 'Gerado'
          sc.filtro.gerado = ''
        else
          sc.filtro.gerado = 'Gerado'
        return
      if name == 'Excluido'
        if sc.filtro.exluido == 'Excluido'
          sc.filtro.exluido = ''
        else
          sc.filtro.exluido = 'Excluido'
        return
      return

    sc.config =
      menu: [
        {nome:'Configurações de Cobranças'}
        {nome:'Configurações Boleto'}
      ]
      status: [
        {nome: 'Aberto', active: true}
        {nome: 'Reaberto', active: true}
        {nome: 'Gerado'}
        {nome: 'Pausado'}
        {nome: 'Excluído'}
      ]
      modalCobranca: new $scModal
      modalConfigBoleto: new $scModal
      composicao:    ['Ambos', 'Débito', 'Crédito']
      planoDeContas: ['1 Receita', '2 Despesa']
      destinatarios: ['Morador', 'Proprietário']

    sc.actionMenu = (obj, objExt = [])->
      switch obj.nome
        when 'Configurações Boleto'
          sc.config.modalConfigBoleto.open()
        when 'Configurações de Cobranças'
          sc.config.modalCobranca.open()

    sc.pagadores = LancamentosService.pagadores
]

.controller 'LancamentoCtrl', [
  '$scope', 'LancamentosService', '$scModal'
  ($scope, LancamentosService, $scModal)->
    $scope.initForm = (obj)->
      $scope.form = obj
      $scope.form._openCadastro = true
      $scope.form._inputs = [
        {
          num: 0
          nome: 'Novo Rateio'
          valor: 0
          planoContas: 1
          subComp: true
          subComposicao: [
            num: 0
            nome: 'Novo Rateio'
            valor: 0
          ]
        }
        {
          num: 1
          nome: 'Novo Rateio'
          valor: 0
          planoContas: 1
          subComposicao: [
            num: 0
            nome: 'Novo Rateio'
            valor: 0
          ]
        }
      ]
      $scope.form.pagadores = []

    $scope.iconeStatus = (obj)->
      switch obj.status
        when 'Aberto'   then 'sc-icon-seta-7-direita'
        when 'Reaberto' then 'sc-icon-seta-8-direita'
        when 'Gerado'   then 'sc-icon-circulo-1'
        when 'Excluido' then 'sc-icon-banir'
        else ''

    $scope.corStatus = (obj)->
      switch obj.status
        when 'Aberto'   then 'blue'
        when 'Reaberto' then 'blue-dark'
        when 'Gerado'   then 'green'
        when 'Excluido' then 'red'
        when 'Pausado' then 'gray'
        else ''

    $scope.removeLancamento = (obj) ->
      $scope.form._inputs.splice($scope.form._inputs.indexOf(obj), 1)

    $scope.removeLancamentoSub = (numC, obj) ->
      numC.splice(numC.indexOf(obj), 1)

    $scope.addLancamento = (numC) ->
      $scope.form._inputs.push
        num: $scope.form._inputs.length
        nome: 'Novo Rateio'
        valor: 0
        planoContas: 1
        subComposicao: [
          {
            num: 0
            nome: 'Título'
            valor: 0
          }
        ]

    $scope.addLancamentoSub = (numSub) ->
      numSub.push
        num: 0
        nome: 'Título'
        valor: 0

    $scope.alertExclusao = ->
      confirm 'Deseja realmente excluir este lançamento ?'

    $scope.setPagador = (pagador)->
      $scope.form.pagadores ||= []
      $scope.form.pagadores.push {pessoa: pagador}

    $scope.deletePagador = (proprietario)->
      $scope.form.pagadores.remove(proprietario)

    $scope.pesquisar = ->
      skip_ids = $scope.form.pagadores.map (e)-> e.pessoa.id

      LancamentosService.listaPagadores.filter (e)->
        !skip_ids.inArray(e.id)

    $scope.listaPagadores = LancamentosService.listaPagadores
]

.controller "ConfigCobrancasCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.listaInput = [{num: 0}]

    sc.addInput = ->
      sc.listaInput.push
        num: sc.listaInput.length

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 10
        cobranca1Juridica: 0
        juros: 0
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.fecharModal = ->
      sc.config.modalCobranca.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
    ]

    sc.modelos = [
      {
        nome: "Taxa de condomínio"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.m."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [2]
        mensagemAdiconal: ""
      }
      {
        nome: "Sala Comercial"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
      }
    ]
]

.controller "BoletosModalCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 0
        cobrancaJuridica: 0
        juros: 0
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.fecharModal = ->
      sc.modalConfigBoleto.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
    ]

    sc.listaInput = [{num: 0}]

    sc.addInput = ->
      sc.listaInput.push
        num: sc.listaInput.length

    sc.modelos = [
      {
        nome: "Taxa de condomínio"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.m."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [2]
        mensagemAdiconal: ""
      }
      {
        nome: "Sala Comercial"
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
      }
    ]
]

.controller "TotalSelecionadosCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 0
        cobrancaJuridica: 0
        juros: 0
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.fecharModal = ->
      sc.modalConfigBoleto.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
    ]

    sc.listaInput = [{num: 0}]

    sc.addInput = ->
      sc.listaInput.push
        num: sc.listaInput.length

    sc.modelos = [
      {
        nome: "Torre 1 - 101"
        valor:200.00
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.m."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [2]
        mensagemAdiconal: ""
        lancamentos: [
          {
            name: 'Taxa de condomínio'
            value: 150
          }
          {
            name: 'Salão de festas'
            value: 50
          }
        ]
      }
      {
        nome: "Torre 1 - 102"
        valor:300.00
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
        lancamentos: [
          {
            name: 'Taxa de condomínio'
            value: 200
          }
          {
            name: 'Multa'
            value: 100
          }
        ]
      }

      {
        nome: "Torre 1 - 103"
        valor:300.00
        multa: 2
        cobrancaJuridica: 60
        juros: 1
        tipoJuros: "a.d."
        bloqueioSegVia: 7
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: true
        tiposRecibo: [1, 3]
        mensagemAdiconal: ""
        lancamentos: [
          {
            name: 'Taxa de condomínio'
            value: 200
          }
          {
            name: 'Multa'
            value: 100
          }
        ]
      }
    ]
]
