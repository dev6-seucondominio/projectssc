angular.module 'app', [ 
  'sc.app.helpers'
]

.controller 'AppCtrl', [
  '$scope', '$scModal', '$filter'
  (sc, $scModal, $filter)->
  
    sc.configModal = new $scModal
    
    sc.addPassagem = {}
    
    sc.anotacoes = []
    sc.anotacoesTmp = []

    sc.passagens = [
      {
        id:0
        sai:'Janaina'
        entra:'Julia'
        obs:'Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto'
        data:'2015-10-05'
        hora:'08:00'
        vistoria:'Option 1'
      }
      {
        id:1
        sai:'Judaina'
        entra:'Janaina'
        obs:'Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto'
        data:'2015-10-05'
        hora:'19:00'
        vistoria:'Option 2'
      }
      {
        id:2
        sai:'Joana'
        entra:'Julieta'
        obs:'Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto'
        data:'2015-10-06'
        hora:'08:00'
        vistoria:'Option 1'
      }
      {
        id:3
        sai:'Julia'
        entra:'Joana'
        obs:'Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto'
        data:'2015-10-06'
        hora:'19:00'
        vistoria:'Option 2'
      }
      {
        id:4
        sai:'Juliana'
        entra:'Jumara'
        obs:'Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto'
        data:'2015-10-07'
        hora:'08:00'
        vistoria:'Option 1'
      }
      {
        id:5
        sai:'Julieta'
        entra:'Juliana'
        obs:'Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto Texto'
        data:'2015-10-07'
        hora:'19:00'
        vistoria:'Option 2'
      }
    ]

    validation = (obj) ->
      if(!obj.sai || !obj.entra || !obj.vistoria || !obj.saiPass || !obj.entraPass)
        obj.error = true
        obj.errorSai = !obj.sai ? true:false
        obj.errorSaiPass = !obj.saiPass ? true:false
        obj.errorEntra = !obj.entra ? true:false
        obj.errorEntraPass = !obj.entraPass ? true:false
        obj.errorVistoria = !obj.vistoria ? true:false
        return false
      else
        obj.error = false
        obj.errorSai = false
        obj.errorSaiPass = false
        obj.errorEntra = false
        obj.errorEntraPass = false
        obj.errorVistoria = false
        return true
    
    sc.cancelarPassagem = ()->
      sc.addPassagem = {}
      sc.adicionarPassagem = false
    
    sc.fazerPassagem = ()->
      console.log 'Fazer passagem'
      add = sc.addPassagem
      if(validation(add))
        sc.passagens.push
          id: sc.passagens.length
          sai: add.sai
          entra: add.entra
          obs: add.obs
          data: $filter('date')(new Date(), 'yyyy-MM-dd')
          hora: $filter('date')(new Date(), 'HH:mm')
          vistoria: add.vistoria
        sc.addPassagem = {}
        sc.adicionarPassagem = false
      
    sc.anotacaoPassagem = ()->
      console.log 'Anotação de passagem'
      add = sc.addPassagem
      sc.anotacoes.push
        sai: add.sai || ''
        entra: add.entra || ''
        obs: add.obs || ''
        data: $filter('date')(new Date(), 'yyyy-MM-dd')
        hora: $filter('date')(new Date(), 'HH:mm')
        vistoria: add.vistoria || ''
        idTemp: sc.anotacoes.length
        
      sc.anotacoesTmp = angular.copy sc.anotacoes
      sc.addPassagem = {}
      sc.adicionarPassagem = false
    
    sc.anotacaoPassagemAnotacao = (obj)->
      console.log 'Anotação de passagem -> Anotação'
      sc.anotacoes[obj.idTemp] = obj
    
    sc.cancelarPassagemAnotacao = (obj)->
      console.log 'Cancelar passagem -> Anotação'
      sc.anotacoesTmp[obj.idTemp] = angular.extend {}, sc.anotacoes[obj.idTemp]
    
    sc.fazerPassagemAnotacao = (obj)->
      console.log 'Fazer passagem -> Anotação'
      if (validation(obj))
        sc.passagens.push
          id: sc.passagens.length
          sai: obj.sai
          entra: obj.entra
          obs: obj.obs
          data: $filter('date')(new Date(), 'yyyy-MM-dd')
          hora: $filter('date')(new Date(), 'HH:mm')
          vistoria: obj.vistoria
        sc.anotacoesTmp.splice obj, 1
        sc.anotacoes.splice obj.idTemp, 1
]

.controller 'ConfigCtrl', [
  '$scope'
  (sc)->
      
    sc.addItem = {name: ""}
    sc.items = [{name: "Cheve da Piscina"}, {name: "Chave da Quadra"},{name: "Computador"}]
    
    sc.add_to_list = (item)->
      if item.name
        item.errorAdd = false
        sc.items.push 
          name: item.name
        sc.addItem.name = ""
      else
        console.log sc.addItem.errorAdd
        sc.addItem.errorAdd = true
    
    sc.remove_to_list = (item)->
      sc.items.splice(sc.items.indexOf(item), 1)
]