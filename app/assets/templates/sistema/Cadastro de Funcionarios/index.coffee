angular.module 'app', [
  'sc.app.helpers'
]

.controller 'Controll', [
  '$scope', '$scModal', 'DadosFuncionarios'
  (sc, $scModal, DadosFuncionarios)->

    sc.dadosGerais = 
      menu: [
        {
          name: 'Editar'
          icon: 'sc-icon sc-icon-lapis'
          iconColor: 'sc-text-yellow'
        }
        {
          name: 'Enviar por e-mail'
          icon: 'sc-icon sc-icon-carta-1'
          iconColor: 'sc-text-blue'
        }
        {
          name: 'Campos visíveis'
          icon: 'sc-icon sc-icon-olho'
          iconColor: 'sc-text-blue'
        }
        {
          name: 'Imprimir ficha'
          icon: 'sc-icon sc-icon-impressora'
          iconColor: 'sc-text-yellow'
        }
        {
          name: 'Dispensar Funcionário'
          icon: 'sc-icon sc-icon-sair'
          iconColor: 'sc-text-red'
        }
        {
          name: 'Excluir'
          icon: 'sc-icon sc-icon-lixeira-1'
          iconColor: 'sc-text-red'
        }
      ]
      turnos: [
        {turno:'Matutino'}
        {turno:'Vespertino'}
        {turno:'Noturno'}
      ]
      semanas: [
        {semana:'Domingo'}
        {semana:'Segunda'}
        {semana:'Terça'}
        {semana:'Quarta'}
        {semana:'Quinta'}
        {semana:'Sexta'}
        {semana:'Sábado'}
      ]

    sc.enviarEmail = 
      modal: new $scModal

    sc.dispensarFuncionario = 
      dispensar:
        name:'' 
      modal: new $scModal

    sc.camposVisiveis =
      campos: [
        {campo:'Nome', visible:true}
        {campo:'PIS', visible:true}
        {campo:'Cargo', visible:true}
        {campo:'CPF', visible:true}
        {campo:'Data contratação', visible:true}
        {campo:'Foto/Avatar', visible:true}
        {campo:'E-mail', visible:true}
        {campo:'Horário de trabalho', visible:true}
        {campo:'Empresa', visible:true}
        {campo:'Endereço', visible:true}
        {campo:'Telefone 1', visible:true}
        {campo:'E-mail', visible:true}
        {campo:'Salário', visible:true}
        {campo:'Telefone 2', visible:false}
      ]
      modal: new $scModal

    sc.filtro = 
      _open: false
      dados: {}

    sc.objTemp = {}

    sc.novoFuncionario =
      _new: false
      dados:
        infoPess: {}
        infoCadas:
          jornadas: []
      jornadaTmp: {}

    sc.funcionarios = DadosFuncionarios.funcionarios

    sc.newFunc = ()->
      if sc.novoFuncionario._new
        sc.novoFuncionario.dados = {}
      sc.novoFuncionario._new = !sc.novoFuncionario._new

    sc.actionMenu = (obj, item)->
      switch item.name
        when 'Editar'
          sc.objTemp = angular.copy obj
          obj._edit = true
          obj.opened = true
        when 'Enviar por e-mail'
          sc.enviarEmail.modal.open()
        when 'Campos visíveis'
          sc.camposVisiveis.modal.open()
        when 'Dispensar Funcionário'
          sc.dispensarFuncionario.dispensar.name = obj.infoPess.name
          sc.dispensarFuncionario.modal.open()
]

.controller 'Funcionarios', [
  '$scope'
  (sc)->

    sc.toggleAcc = (obj)->
      f.opened = false for f in sc.funcionarios unless obj.opened
      obj.opened = !obj.opened
]

.controller 'Funcionario', [
  '$scope', '$filter'
  (sc, $filter)->

    sc.initForm = (obj)->
      sc.form = obj
      sc.form.opened = false
      sc.jornadaTmp = {}

    sc.novaJornada = ()->
      sc.form.infoCadas.jornadas.push 
        entrada: $filter('date')(sc.jornadaTmp.entrada, 'yyyy-MM-dd')
        saida: $filter('date')(sc.jornadaTmp.saida, 'yyyy-MM-dd')
        entradaAlmoco: $filter('date')(sc.jornadaTmp.entradaAlmoco, 'yyyy-MM-dd')
        saidaAlmoco: $filter('date')(sc.jornadaTmp.saidaAlmoco, 'yyyy-MM-dd')
        semanas: []
      sc.jornadaTmp = {}

      add = sc.form.infoCadas.jornadas[sc.form.infoCadas.jornadas.length-1]
      for i in sc.dadosGerais.semanas
        if i.checked
          add.semanas.push
            semana: sc.dadosGerais.semanas.indexOf(i)
        i.checked = null
    
    sc.cancelar = (obj)->
      if sc.novoFuncionario._new
        sc.novoFuncionario._new = false
      else
        sc.funcionarios[obj.id] = angular.copy sc.objTemp
        sc.objTemp = {}
        obj._edit = false

    sc.salvar = (obj)->
      if sc.novoFuncionario._new
        sc.funcionarios.push obj
        sc.novoFuncionario._new = false
      else
        obj._edit = false
        sc.objTemp = {}

]

.controller 'camposVisiveisCtrl', [
  '$scope'
  (sc)->

]

# Dados

.factory 'DadosFuncionarios', [
  ()->
    func = 
      funcionarios: [
        {
          id: 0
          infoCadas:
            dataDeEmissao: '2015-05-25'
            salario: 725
            cargaHoraria: 8
            funcao:'Jardineiro'
            empresa:'Empresa tal'
            observacao:''
            turnos:
              {0}
            semanas : [
              {semana:0}
              {semana:1}
              {semana:2}
              {semana:3}
              {semana:4}
              {semana:5}
              {}
            ]
            jornadas: [
              {
                entrada: '2015-01-01'
                entradaAlmoco: '2015-01-01'
                saidaAlmoco: '2015-01-01'
                saida: '2015-01-01'
                semanas:[
                  {}
                  {semana:1}
                  {semana:2}
                  {semana:3}
                  {semana:4}
                  {semana:5}
                  {}
                ]
              }
              {
                entrada: '2015-01-01'
                entradaAlmoco: '2015-01-01'
                saidaAlmoco: '2015-01-01'
                saida: '2015-01-01'
                semanas:[
                  {semana:0}
                  {}, {}, {}, {}, {}, {}
                ]
              }
            ]
          infoPess:
            name:'Fulado de tal'
            sexo:'Masculino'
            rg:123456789
            nascimento: '1989-11-13'
            cpf: 12345678890
            eMail: 'fulano@tal.com'
            telefone: '1234-4321'
            celular: '1234-4321'
            endereco:
              rua: 'Rua tal'
              bairro: 'Bairro tal'
              numero: 's/n'
              uf: 'GO'
              cep: '7440000'
              complemento: ''
              municipio: ''
        }
        {
          id: 1
          infoCadas:
            dataDeEmissao: '2015-05-25'
            salario: 725
            cargaHoraria: 8
            funcao:'Jardineiro'
            empresa:'Empresa tal'
            observacao:''
            turnos:
              {1}
            semanas : [
              {semana:0}
              {semana:1}
              {semana:2}
              {semana:3}
              {semana:4}
              {semana:5}
              {}
            ]
            jornadas: [
              {
                entrada: '2015-01-01'
                entradaAlmoco: '2015-01-01'
                saidaAlmoco: '2015-01-01'
                saida: '2015-01-01'
                semanas:[
                  {semana:1}
                  {semana:2}
                  {semana:3}
                  {semana:4}
                  {semana:5}
                ]
              }
              {
                entrada: '2015-01-01'
                entradaAlmoco: '2015-01-01'
                saidaAlmoco: '2015-01-01'
                saida: '2015-01-01'
                semanas:[
                  {semana:0}
                ]
              }
            ]
          infoPess:
            name:'Fulado de tal 2222'
            sexo:'Masculino'
            rg:123456789
            nascimento: '1989-11-13'
            cpf: 12345678890
            eMail: 'fulano@tal.com'
            telefone: '1234-4321'
            celular: '1234-4321'
            endereco:
              rua: 'Rua tal'
              bairro: 'Bairro tal'
              numero: 's/n'
              uf: 'GO'
              cep: '7440000'
              complemento: ''
              municipio: ''
        }
      ]
    func
]