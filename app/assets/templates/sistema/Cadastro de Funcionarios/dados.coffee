angular.module 'app', [
  'sc.app.helpers'
]

.factory 'Funcionarios', [
  ()->
    func = 
      funcionarios: [
        {
          infoCadas:
            dataDeEmissao: '2015-05-25'
            salario: 725
            cargaHoraria: 8
            funcao:'Jardineiro'
            turno:
              tur3:true
            jornadas: [
              {
                entrada: ????
                entradaAlmoco: ????
                saidaAlmoco: ????
                saida: ????
                semanas:[
                  {semana:1}
                  {semana:2}
                  {semana:3}
                  {semana:4}
                  {semana:5}
                ]
              }
              {
                entrada: ????
                entradaAlmoco: ????
                saidaAlmoco: ????
                saida: ????
                semanas:[
                  {semana:0}
                ]
              }
            ]
          infoPess:
            name:'Fulado de tal'
            sexo:'Masculino'
            rg:123456789
            nascimento: '1989-11-13'
            cpf: 12345678890
            e-mail: 'fulano@tal.com'
            telefone: '1234-4321'
            celular: '1234-4321'
            endereco:
              rua: 'Rua tal'
              bairro: 'Bairro tal'
              numero: 's/n'
              uf: 'GO'
              cep: '7440000'
              municipior: 
        }
      ]
    func
]