angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  $scope.novoMonitoramento = new $scModal()
  $scope.editarMonitoramento = new $scModal()
  $scope.editarCamera = new $scModal()
  $scope.novaCamera = new $scModal()

  #### Variaveis
  $scope.cameras = [
    {
      id:'1'
      name:'Salão de Festas'
    }
    {
      id:'2'
      name:'Hall Principal'
    }
    {
      id:'3'
      name:'Piscina'
    }
    {
      id:'4'
    }
    {
      id:'5'
    }
    {
      id:'6'
    }
    {
      id:'7'
    }
    {
      id:'8'
    }
    {
      id:'9'
    }
    {
      id:'10'
    }
    {
      id:'11'
    }
    {
      id:'12'
    }
    {
      id:'13'
    }
  ]
  $scope.lista = [
    {
      name:'Monitoramento Garagem'
    }
    {
      name:'Monitoramento Area Comum'
    }
    {
      name:'Monitoramento Hall'
    }
    {
      name:'Monitoramento Subsolo'
    }
    {
      name:'Monitoramento Garagem'
    }
  ]
  $scope.lista1 = [
    {
      name:'Hall'
    }
    {
      name:'Garagem 1'
    }
    {
      name:'Garagem 2'
    }
    {
      name:'Garagem 3'
    }
  ]

  $scope.alert1 = ->
    alert 'Abrir todas as câmeras configuradas no monitoramento em uma nova aba em modo "Fullscreen" para utilização em tela separada.'
]
