angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->
  $scope.sugestoes = [
    {
      edit: false
      titulo: 'Opção de contato entre o morador e o condomínio'
      nome: 'Diego Felipe'
      descricao: 'Sistema'
      sugestoes: 'açsldkfjaçsdlfasdlçkfaçsldkj naçlsdkjaçlskdjfçlkasdjfçlaksjd nasçldkfjasçldfkjasçldkfjasçdlkfjaçlskdjf'
      residencia: 'Casa'
      email:'açl@gamil.com'
      curtidas: 0
      comentario: [
        {
          name:'Leandro Pinheiro'
          texto:'Vem com dois controles?'
        }
        {
          name:'Amanda Nunes'
          texto:'Boa noite, gostaria de saber de quanto é a memória dele e quais jogos você possui, para que eu possa comprar alguns de você mesmo. Desde já, obrigado'
        }
        {
          name:'Maria Luiza'
          texto:'Boa noite, ele é de 4gb. Tenho the last of Us, Left 4, Dead 2, street fighter e muitos outros. Podemos Negociar.'
        }
      ]
    }
    {
      edit: false
      titulo: 'Reformar a faxada do condomínio'
      nome: 'Diego Felipe'
      descricao: 'Sistema'
      sugestoes: 'açsldkfjaçsdlfasdlçkfaçsldkj naçlsdkjaçlskdjfçlkasdjfçlaksjd nasçldkfjasçldfkjasçldkfjasçdlkfjaçlskdjf'
      residencia: 'Casa'
      email:'açl@gamil.com'
      curtidas: 0
      comentario: [
        {
          name:'Leandro Pinheiro'
          texto:'Vem com dois controles?'
        }
        {
          name:'Amanda Nunes'
          texto:'Boa noite, gostaria de saber de quanto é a memória dele e quais jogos você possui, para que eu possa comprar alguns de você mesmo. Desde já, obrigado'
        }
        {
          name:'Maria Luiza'
          texto:'Boa noite, ele é de 4gb. Tenho the last of Us, Left 4, Dead 2, street fighter e muitos outros. Podemos Negociar.'
        }
      ]
    }
  ]
  $scope.selectOP = {sugestaoPara11:true, sugestaoPara:true, sugestaoPara2:true, data: true}

  $scope.select = ->
    $scope.selectOP.data = true
    $scope.selectOP.popu = false

  $scope.select2 = ->
    $scope.selectOP.popu = true
    $scope.selectOP.data = false

  $scope.select3 = ->
    $scope.selectOP.sugestaoPara11 = true
    $scope.selectOP.sugestaoPara22 = false

  $scope.select4 = ->
    $scope.selectOP.sugestaoPara22 = true
    $scope.selectOP.sugestaoPara11 = false
]

.controller "listaClass", ["$scope", "$timeout", ($scope, $timeout) ->
  #### Variaveis
  $scope.editClassificadoTemp = {}
  $scope.temId = {}
  #### Variaveis

  #### funções
  $scope.editLista = (lista) ->
    lista.edit = true
    $scope.editClassificadoTemp = angular.copy(lista)
    $scope.temId.idEdit = $scope.classificados.indexOf(lista)
    if lista.situacao == 'Fechado'
      $scope.editClassificadoTemp.boolSituacao = false
    else
      $scope.editClassificadoTemp.boolSituacao = true

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openAcc = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined

  $scope.salveEdit = ->
    if confirm 'Deseja realmente salvar a edição do classificado ?'
      $scope.classificados[$scope.temId.idEdit] = $scope.editClassificadoTemp
      lista = $scope.classificados[$scope.temId.idEdit]
      if lista.boolSituacao
        lista.situacao = 'Aberto'
      else
        lista.situacao = 'Fechado'
      lista.edit = false
      $timeout ->
        lista.bgColorAcc = 'background-color: #337ab7;'
        lista.bColor = 'border-right: 1px solid #438ccb;'
        lista.txtColor = 'color: white;'

  $scope.cancellEdit = ->
    if confirm 'Deseja realmente cancelar a edição do classificado ?'
      $scope.editClassificadoTemp = {}
      $scope.classificados[$scope.temId.idEdit].edit = false
  #### funções
]
''
