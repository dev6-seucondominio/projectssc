angular.module "scApp", [ "sc.app.helpers", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  #### Variáveis
  $scope.modalConfigBoleto = new $scModal()
  $scope.modalExcluir = new $scModal()
  $scope.hover = {}
  $scope.configuracoes = new $scModal()
  $scope.cadastroNew = new $scModal()
  $scope.dre = new $scModal()
  $scope.listaPlanos = [
    {name:'1 Ativo'}
    {name:'1.1 Ativo Circulante'}
    {name:'1.1.1 Disponibilidades'}
    {name:'1.1.1.1 Caixa'}
    {name:'1.1.1.1.1 - Caixa Sindico'}
    {name:'1.1.1.1 bancos'}
    {name:'1.1.1.1 Fundo de Reserva'}
    {name:'1.1.2 Cotas a receber'}
    {name:'1.1.2 Outros créditos'}
    {name:'2 Passivo'}
    {name:'2.1 Passivo Circulante'}
    {name:'2.1.1 Impostos a recolher'}
    {name:'2.1.2 Contas a pagar'}
    {name:'3 Receitas'}
    {name:'3.1 Receita Líquida'}
    {name:'3.2 Outras receitas operacionais'}
    {name:'4 Despesas'}
    {name:'4.1 Despesas Operacionais'}
    {name:'4.2 Despesas com Pessoal'}
    {name:'5 Patrimonio Líquido'}
    {name:'5.1 Capital Social'}
    {name:'5.2 Reservas'}
    {name:'6 Controle'}
  ]
  $scope.variaveisTestes = {}
  $scope.planosDeContas = [
    {
      name:'1 - Ativo'
      sub1: [
        {
          name:'1.1 - Ativo Circulante'
          boolStatus:true
          sub2: [
            {
              name:'1.1.1 - Disponibilidades'
              boolStatus:true
              sub3: [
                {
                  name:'1.1.1.1 - Caixa'
                  boolStatus:false
                  sub4: [
                    {
                      name:'1.1.1.1.1 - Caixa Sindico'
                      boolStatus:false
                      sub5: [
                        {
                          titulo:'Saldo Inicial (R$ 100,00)'
                          data:'31/07/15'
                          entrada:''
                          saida:''
                        }
                        {
                          titulo:'Chaveiro'
                          data:'01/08/15'
                          entrada:''
                          saida:'200'
                        }
                        {
                          titulo:'Salão de Festas'
                          data:'03/08/15'
                          entrada:'100'
                          saida:''
                        }
                        {
                          titulo:'Taxa Condominio'
                          data:'15/08/15'
                          entrada:'200'
                          saida:''
                        }
                        {
                          titulo:'Saldo Final (R$ 200,00)'
                          data:'31/08/15'
                          entrada:''
                          saida:''
                        }
                      ]
                    }
                    {
                      name:'1.1.1.1.2 - Caixa Zelador'
                      boolStatus:false
                      sub5: [
                        {
                          titulo:'Saldo Inicial (R$ 100,00)'
                          data:'31/07/15'
                          entrada:''
                          saida:''
                        }
                        {
                          titulo:'Chaveiro'
                          data:'01/08/15'
                          entrada:''
                          saida:'200'
                        }
                        {
                          titulo:'Salão de Festas'
                          data:'03/08/15'
                          entrada:'100'
                          saida:''
                        }
                        {
                          titulo:'Taxa Condominio'
                          data:'15/08/15'
                          entrada:'200'
                          saida:''
                        }
                        {
                          titulo:'Saldo Final (R$ 200,00)'
                          data:'31/08/15'
                          entrada:''
                          saida:''
                        }
                      ]
                    }
                  ]
                }
                {
                  name:'1.1.1.2 - Banco'
                  boolStatus:false
                  sub4: [
                    {
                      name:'1.1.1.2.1 - Itau'
                      boolStatus:false
                      sub5: [
                        {
                          titulo:'Saldo Inicial (R$ 100,00)'
                          data:'31/07/15'
                          entrada:''
                          saida:''
                        }
                        {
                          titulo:'Chaveiro'
                          data:'01/08/15'
                          entrada:''
                          saida:'200'
                        }
                        {
                          titulo:'Salão de Festas'
                          data:'03/08/15'
                          entrada:'100'
                          saida:''
                        }
                        {
                          titulo:'Taxa Condominio'
                          data:'15/08/15'
                          entrada:'200'
                          saida:''
                        }
                        {
                          titulo:'Saldo Final (R$ 200,00)'
                          data:'31/08/15'
                          entrada:''
                          saida:''
                        }
                      ]
                    }
                    {
                      name:'1.1.1.2.2 - Banco do Brasil'
                      boolStatus:false
                      sub5: [
                        {
                          titulo:'Saldo Inicial (R$ 100,00)'
                          data:'31/07/15'
                          entrada:''
                          saida:''
                        }
                        {
                          titulo:'Chaveiro'
                          data:'01/08/15'
                          entrada:''
                          saida:'200'
                        }
                        {
                          titulo:'Salão de Festas'
                          data:'03/08/15'
                          entrada:'100'
                          saida:''
                        }
                        {
                          titulo:'Taxa Condominio'
                          data:'15/08/15'
                          entrada:'200'
                          saida:''
                        }
                        {
                          titulo:'Saldo Final (R$ 200,00)'
                          data:'31/08/15'
                          entrada:''
                          saida:''
                        }
                      ]
                    }
                  ]
                }
              ]
            }
            {
              name:'1.1.2 - Cotas a receber'
              boolStatus:true
              tag:'Juros'
            }
            {
              name:'1.1.3 - Outros créditos'
              boolStatus:true
            }
          ]
        }
      ]
    }
    {
      name:'2 - Passivo'
      sub1: [
        {
          name:'2.1 - Passivo Circulante'
          boolStatus:true
          sub2: [
            {
              name:'2.1.1 - Impostos a recolher'
              boolStatus:true
              tag:'PIS/COFINS/CSLL'
            }
            {
              name:'2.1.2 - Contas a pagar'
              boolStatus:false
            }
          ]
        }
      ]
    }
    {
      name:'3 - Receitas'
      sub1: [
        {
          name:'3.1 - Receita Líquida'
          boolStatus:true
        }
        {
          name:'3.2 - Outras receitas operacionais'
          boolStatus:true
        }
      ]
    }
     {
      name:'4 - Despesas'
      sub1: [
        {
          name:'4.1 - Despesas Operacionais'
          boolStatus:true
        }
        {
          name:'4.2 - Despesas com Pessoal'
          boolStatus:true
        }
      ]
    }
     {
      name:'5 - Patrimonio Líquido'
      sub1: [
        {
          name:'5.1 - Capital social'
          boolStatus:true
        }
        {
          name:'5.2 - Reservas'
          boolStatus:true
        }
      ]
    }
    {
      name:'6 - Controle'
      sub1: [
        {
        }
      ]
    }
  ]

  #### funções
  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'
  
  $scope.openModal = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined
]

.controller "BoletosModalCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.editando = false
    sc.defaultValues = ->
      sc.criandoEditandoModelo = false
      sc.editando = false
      sc.modeloDefault =
        nome: ""
        multa: 10
        cobrancaJuridica: 0
        juros: 10
        tipoJuros: "a.d."
        bloqueioSegVia: 0
        instrucoesCobranca: ""
        observacaoBoleto: ""
        reciboSacado: false
        tiposRecibo: []
        mensagemAdiconal: ""
    sc.defaultValues()

    sc.criarModelo = ->
      sc.defaultValues()
      sc.criandoEditandoModelo = true
      checarItensAtivos(sc.modeloDefault)

    sc.editarModelo = (modelo)->
      sc.modeloDefault = sc.modelos[pegarIndexModelo(modelo)]
      sc.editando = true
      sc.criandoEditandoModelo = true
      checarItensAtivos(modelo)

    sc.salvarModelo = ->
      unless sc.editando
        sc.modelos.push sc.modeloDefault
      sc.defaultValues()

    sc.excluirModelo = (modelo)->
      sc.modelos.splice(pegarIndexModelo(modelo), 1)

    sc.cancelarCriarEditar = ->
      sc.defaultValues()

    sc.fecharModal = ->
      sc.modalConfigBoleto.close()
      sc.defaultValues()

    pegarIndexModelo = (modelo)->
      sc.modelos.indexOf modelo

    # TAGBOX RECIBO SACADO
    sc.limiteItensVisiveis = 1
    sc.verTodos = false

    sc.alteraVerTodos = ->
      sc.verTodos = !sc.verTodos

    sc.marcarItem = (elem, modelo) ->
      elem.ativo = !elem.ativo
      if elem.ativo
        modelo.tiposRecibo.addOrExtend elem.id
      else
        modelo.tiposRecibo.remove elem.id
      checarItensAtivos(modelo)

    checarItensAtivos = (modelo)->
      item.ativo = modelo.tiposRecibo.inArray(item.id) for item in sc.tiposRecibo

    sc.itensSelecionados = (lista) ->
      $filter("filter")(lista, {"ativo": true})

    sc.itensSelecionadosVisiveis = (lista) ->
      itens = sc.itensSelecionados(lista)
      return itens.slice(0, sc.limiteItensVisiveis) unless sc.verTodos
      itens

    sc.qtdItensEscondidos = (lista) ->
      sc.itensSelecionados(lista).length - sc.limiteItensVisiveis
    # END - TAGBOX RECIBO SACADO

    sc.tiposRecibo = [
      {id: 1, nome: "Composição Receitas"}
      {id: 2, nome: "Composição Despesas"}
      {id: 3, nome: "Inadimplência da Unidade"}
      {id: 4, nome: "Inadimplência Geral"}
      {id: 5, nome: "Demostrativo de rateio"}
    ]

    sc.marcar = ()->
    
    

    sc.modelos = [
      {
        nome: "Plano de contas 2015"
        topo: true
      }
      {
        nome: "Plano de contas 2010"
      }
      
    ]
] 