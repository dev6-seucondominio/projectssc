angular.module 'app', [ 
  'sc.app.helpers'
]

.controller 'AppCtrl', [
  '$scope', '$scModal', '$filter'
  (sc, $scModal,$filter)->
    
    sc.configConsumo = new $scModal()
    sc.novaLeitura = new $scModal()
    sc.OpCadatro = new $scModal()
    
    sc.monthPickerDate = $filter('date')(new Date(), 'yyyy-MM-dd')
    sc.selected = []
    sc.actionAlt = {}
    sc.cadastro = {}
    sc.actions     = {}
    sc.editMedidorObj = {}
    sc.addMedidor  = {}
    
    sc.medidores = [
      {
        id: 0
        name:'Água'
        unidade:'m³'
        valor: 1.5
        unidadesDesativadas:'101'
      }
      {
        id: 1
        name:'Gás'
        unidade:'cm³'
        valor: 0.25
        unidadesDesativadas:'102'
      }
      {
        id: 2
        name:'Energia'
        unidade:'kWh'
        valor: 3.2
        unidadesDesativadas:'103'
      }
    ]

    sc.leituras = [
      {
        title:'Gás'
        date:'2015-11-01'
        idBase:3
        id:1
        null:true
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-11-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-11-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Energia'
        date:'2015-11-01'
        null:true
        idBase:4
        id:2
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-11-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Água'
        date:'2015-11-01'
        idBase:5
        id:0
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-11-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Gás'
        date:'2015-10-01'
        idBase:3
        id:1
        null:true
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-10-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-10-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Energia'
        date:'2015-10-01'
        null:true
        idBase:4
        id:2
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-10-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Água'
        date:'2015-10-01'
        idBase:5
        id:0
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-10-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Gás'
        date:'2015-09-01'
        idBase:6
        id:1
        null:true
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-09-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-09-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Energia'
        date:'2015-09-01'
        null:true
        idBase:7
        id:2
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-09-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Água'
        date:'2015-09-01'
        idBase:8
        id:0
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
            alterado:true
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-09-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Gás'
        date:'2015-08-01'
        idBase:9
        id:1
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Energia'
        date:'2015-08-01'
        idBase:10
        id:2
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Água'
        date:'2015-08-01'
        null:true
        idBase:11
        id:0
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-08-01'
            atual:0
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-08-01'
            atual:1200.85
            anterior:1100.90
          }
        ]
      }
      {
        title:'Gás'
        date:'2015-07-01'
        id:1
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-07-01'
            atual:1200.85
          }
        ]
      }
      {
        title:'Energia'
        date:'2015-07-01'
        id:1
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-07-01'
            atual:1200.85
          }
        ]
      }
      {
        title:'Água'
        date:'2015-07-01'
        id:1
        unidades: [
          {
            title:'Torre1 - apto. 101'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 102'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 103'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 104'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 105'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 106'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 107'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 108'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 109'
            data:'2015-07-01'
            atual:1200.85
          }
          {
            title:'Torre1 - apto. 110'
            data:'2015-07-01'
            atual:1200.85
          }
        ]
      }
    ]

    sc.unidades = [
      {
        id: 0
        title:'Torre1 - apto. 101'
      }
      {
        id: 1
        title:'Torre1 - apto. 102'
      }
      {
        id: 2
        title:'Torre1 - apto. 103'
      }
      {
        id: 3
        title:'Torre1 - apto. 104'
      }
      {
        id: 4
        title:'Torre1 - apto. 105'
      }
      {
        id: 5
        title:'Torre1 - apto. 106'
      }
      {
        id: 6
        title:'Torre1 - apto. 107'
      }
      {
        id: 7
        title:'Torre1 - apto. 108'
      }
      {
        id: 8
        title:'Torre1 - apto. 109'
      }
      {
        id: 9
        title:'Torre1 - apto. 110'
      }
    ]

    sc.mesReferencia = ()->
      $filter('date')(sc.monthPickerDate, 'yyyy-MM')

    sc.toggleAcc = (obj)->
      i.opened = false for i in sc.leituras if !obj.opened
      obj.opened = !obj.opened
    
    sc.selectLeitura = (leitura)->
      if !leitura.select
        sc.selected = angular.extend {}, leitura
        i.select = false for i in sc.leituras
        leitura.select = true
      else
        sc.selected = []
        leitura.select = false
        
    sc.totalConsumo = ()->
      soma = 0
      if sc.selected.unidades
        for select in sc.selected.unidades
          soma+=(select.atual - select.anterior)
      soma
      
    sc.totalAPagar = ()->
      soma = 0
      if sc.selected.unidades
        for select in sc.selected.unidades
          soma+=((select.atual - select.anterior) * sc.medidores[sc.selected.id].valor)
      soma

    sc.iniciarCadastro = ()->
      add = sc.cadastro
      date = $filter('date')(new Date(), 'yyyy-MM-dd')
      
      add.date = date
      add.null = true

      addBase = add.idBase
      add.unidades = []
      add = add.unidades
      for unidade in sc.unidades
        addAnterior = sc.leituras[addBase].unidades[sc.unidades.indexOf(unidade)].anterior
        add.push
          title:unidade.title
          date: date
          anterior: addAnterior
      sc.actionAlt.addNovaLeitura = true
      sc.actionAlt.novaLeituraOpen = !sc.actionAlt.novaLeituraOpen

]

.controller 'configConsumoCtrl', [
  '$scope'
  (sc)->
  
    sc.editMedidor = (obj)->
      sc.editMedidorObj = angular.extend {}, obj
      sc.actions.edit = true

    sc.saveEditMedidor = (copy)->
      sc.medidores.addOrExtend copy
      sc.actions.edit = false

    sc.addMedidor = (data)->
      sc.actions.add = false
      sc.medidores.push
        id: sc.medidores.lenght
        name: data.name
        unidade: data.unidade
        valor: data.valor

    sc.removeMedidor = (medidor)->
      sc.medidores.splice(sc.medidores.indexOf medidor, 1)
        
]

.controller 'LeituraCtrl', [
  '$scope', '$filter'
  (sc, $filter)->

    sc.selectionOption = (medidor)->
      a.select = false for a in sc.medidores
      medidor.select = true
      sc.cadastro.title = medidor.name
      sc.cadastro.id = sc.medidores.indexOf(medidor)

    sc.selectionLeitura = (leitura)->
      a.select = false for a in sc.leituras
      leitura.select = true
      sc.cadastro.idBase = sc.leituras.indexOf(leitura)
      sc.cadastro.base = leitura.title
      
    sc.salveCadastro = ()->
      add = sc.cadastro
      sc.leituras.push
        title: add.title
        id: add.id
        date: add.date
        idBase: add.idBase
        null: add.null
      addBase = add.idBase
      sc.leituras[sc.leituras.length-1].unidades = []
      addNew = sc.leituras[sc.leituras.length-1].unidades
      for unidade in add.unidades
        addNew.push
          title:unidade.title
          data: unidade.date
          atual: parseInt(unidade.atual) || 0
          anterior: unidade.anterior
      sc.actionAlt.addNovaLeitura = false
    
    sc.cancelarCadastro = ()->
      sc.actionAlt.addNovaLeitura = false
      
]