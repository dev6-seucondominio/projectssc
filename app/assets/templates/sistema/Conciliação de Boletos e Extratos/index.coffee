angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->
  #### Variaveis
  $scope.hover = {}
  $scope.aviso = new $scModal()
  $scope.justifica = new $scModal()
  $scope.retornoModal = new $scModal()
  $scope.retornoModalAviso = new $scModal()
  $scope.revificarRegistro = new $scModal()
  $scope.clikc_s = {}
  $scope.consiliacoesErro = [
    {
      tipo:'Pagamento a menor'
      local1:'Torre 4'
      local2:'Apto. 101'
      name:'Fulado de tal'
      data:'06/06/2015'
      valorPago:'R$ 200,00'
      valorPago:'R$ 200,00'
      status:'erro'
      v1:'150,00'
      v2:'100,00'
      teste:'débito'
      diferenca:'50,00'
    }
    {
      tipo:'Pagamento a maior'
      local1:'Torre 4'
      local2:'Apto. 102'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'erro'
      v1:'150,00'
      v2:'200,00'
      teste:'crédito'
      diferenca:'50,00'
    }
    {
      tipo:'Cobrança não encontrada'
      local1:'Torre 4'
      local2:'Apto. 103'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'erro'
      v1:'250,00'
    }
    {
      tipo:'Duplicidade'
      local1:'Torre 4'
      local2:'Apto. 104'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'erro'
      v1:'250,00'
      total:'300,00'
      teste:'crédito'
      diferenca:'150,00'
      historico: [
        {
          data:'06/04/1990'
          valor:'150'
        }
        {
          data:'10/04/1990'
          valor:'150'
        }
      ]
    }
  ]

  $scope.consiliacoesSucesso = [
    {
      local1:'Torre 1'
      local2:'Apto 101'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'sucesso'
    }
    {
      local1:'Torre 1'
      local2:'Apto 103'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'sucesso'
    }
    {
      local1:'Torre 2'
      local2:'Apto 101'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'sucesso'
    }
    {
      local1:'Torre 2'
      local2:'Apto 102'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'sucesso'
    }
    {
      local1:'Torre 2'
      local2:'Apto 103'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'sucesso'
    }
    {
      local1:'Torre 2'
      local2:'Apto 104'
      name:'Fulado de tal'
      data:'06/06/2015'
      valor:'R$ 200,00'
      status:'sucesso'
    }
  ]

  #### Variaveis

  #### Funções
  $scope.exemploClick = ->
    if $scope.clikc_s.exemplo
      $scope.clikc_s.exemplo = false
      $scope.clikc_s.exemploSize = 'margin-top: 11em;'
    else
      $scope.clikc_s.exemplo = true
      $scope.clikc_s.exemploSize = 'margin-top: 16em;'

  $scope.initErro = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.initSucesso = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openAcc = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined

  $scope.alert1 = ->
    alert 'Aleterar divergencia para Pagametno a maior'

  $scope.alert2 = ->
    alert 'Justificativa da rejeição'
  #### Funções
]
