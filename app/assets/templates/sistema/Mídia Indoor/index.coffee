angular.module 'app', [
  'sc.app.helpers'
]

.factory 'MidiaIndoorService', [
  ()->
    midiaIn =
      menu: [
        {nome: 'Monitorar'}
        {nome: 'Editar'}
        {nome: 'Excluir'}
      ]
      midiaIndoor: [
        {
          id: 1
          nome: 'Ponto 01'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
              programacao:true
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 2
          nome: 'Ponto 02'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 3
          nome: 'Ponto 03'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 4
          nome: 'Ponto 04'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 5
          nome: 'Ponto 05'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 6
          nome: 'Ponto 06'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 7
          nome: 'Ponto 07'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
        {
          id: 8
          nome: 'Ponto 08'
          local: 'local tal'
          grupo: 'grupo tal'
          UltExibicao: 'um dia tal'
          slides: [
            {
              id: 1
              nome: 'Slide 01'
            }
            {
              id: 2
              nome: 'Slide 02'
            }
          ]
        }
      ]
    midiaIn
]

.controller 'controll', [
  '$scope', 'MidiaIndoorService', '$scModal', '$window', '$filter', "$timeout", '$element'
  (sc, MidiaIndoorService, $scModal, win, $filter, $timeout, element)->

    sc.modalConfigFaixas       = new $scModal()
    sc.novoSlideModal          = new $scModal()
    sc.novoPontoModal          = new $scModal()
    sc.excluirModal            = new $scModal()

    sc.actionClick             = {}
    sc.midiaIndoor             = MidiaIndoorService.midiaIndoor
    sc.menuAcc                 = MidiaIndoorService.menu

    sc.scrollPos               = document.body.scrollTop || document.documentElement.scrollTop || 0
    sc.scrollPosBoll           = sc.scrollPos >= 23 ? true : false
    sc.scrollPosBollMobile     = false
    scrollPosBack              = sc.scrollPos

    sc.scrollTopButton = ->
      $("body,html").animate({scrollTop: 0}, "slow")

    win.onscroll = ->
      sc.scrollPos = document.body.scrollTop || document.documentElement.scrollTop || 0
      if element.width() >= 519
        sc.mobile = false
        if sc.scrollPos >= 23
          sc.scrollPosBoll = true
        else
          sc.scrollPosBoll = false
      else
        sc.mobile = true
        if sc.scrollPos > (scrollPosBack)
          sc.scrollPosBollMobile = true
        if sc.scrollPos <= (scrollPosBack)
          sc.scrollPosBollMobile = false
        scrollPosBack = sc.scrollPos
      sc.$apply()

    sc.menuClickAction = (obj, objExt)->
      console.log objExt
      if obj.nome == "Editar"
        if objExt
          sc.novoSlideModal.open()
        else
          sc.novoPontoModal.open()
      if obj.nome == 'Excluir'
        sc.excluirModal.open()
]

.controller 'SlidesCtrl', [
  '$scope', 'MidiaIndoorService', '$filter', "$timeout"
  (sc, MidiaIndoorService, $filter, $timeout)->

    sc.midiaIndoor = MidiaIndoorService.midiaIndoor

    sc.toggleAccMidia = (pag)->
      p.acc = false for p in sc.midiaIndoor unless pag.acc
      pag.acc = !pag.acc
]

.controller 'sliderCtrl', [
  '$scope', 'MidiaIndoorService'
  ($scope, MidiaIndoorService)->

    $scope.initForm = (obj)->
      $scope.form = obj
      $scope.form._openCadastro = true

    $scope.corStatus = (obj)->
      if obj.acc
        return 'sc-acc-gray-light'

    $scope.corText = (obj)->
      switch obj.status
        when 'Jurídica' then 'sc-text-red-darker'
        when 'Vencido' then 'sc-text-red'
        when 'Pago' then 'sc-text-green'
        when 'À vencer' then 'sc-text-yellow'
        else ''

    $scope.pesquisar = ->
      skip_ids = $scope.form.cobrancas.map (e)-> e.pessoa.id
]

.controller "NovoSlideModalCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.fecharModal = ->
      sc.novoSlideModal.close()
]

.controller "NovoPontoModalCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.fecharModal = ->
      sc.novoPontoModal.close()
]

.controller "FAixasModalCtrl", [
  "$scope", "$filter"
  (sc, $filter)->

    sc.fecharModal = ->
      sc.modalConfigFaixas.close()
]
