angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  $scope.addNew = {}

  $scope.listaTipo = [
    {name:'Limpeza'}
    {name:'Obra'}
    {name:'Manutenção'}
    {name:'Outros'}
  ]

  $scope.tarefas = [
    {
      id:'02'
      situacao:true
      name: 'Pintar muro garagem'
      responsavel:'Síndico'
      tipo:'Limpeza'
      inicio:'2015-05-05'
      previsao:'2015-05-14'
      conclusao:'16/05/2015'
      atraso:'2 dias'
      vista: false
      description:'Síndico em 05/05/2015 às 16:19'
      prazo:'14/05/2015'
      pessoasEncarregadas: [
        {
          name:'Fulano de tal'
          cargo:'Cargo tal'
        }
        {
          name:'Fulano de tal'
          rgCpf:'1234567'
        }
      ]
      materiasServicos: [
        {
          titulo:'Argamassa'
          valor:'900,00'
        }
      ]
      comentarios: [
        {
          bg:'sc-bg-green'
          img:'http://cdn4.iconfinder.com/data/icons/general13/png/64/administrator.png'
          name:'Diego Felipe'
          comentario:'Fazendo muito barulho'
        }
        {
          bg:'sc-bg-green'
          icon:'sc-icon-comentario'
          name:'Diego Felipe'
          comentario:'açlsdjflkas alçsdjf alkçsd façlsd flkas dklf çaksd'
        }
        {
          bg:'sc-bg-green'
          icon:'sc-icon-comentario'
          name:'Diego Felipe'
          comentario:'Fazendo muito barulho'
        }
      ]
    }
    {
      id:'03'
      situacao:false
      name: 'Aparar a grama'
      responsavel:'Síndico'
      tipo:'Limpeza'
      inicio:'2015-05-05'
      previsao:'2015-14-05'
      conclusao:'16/05/2015'
      atraso:'2 dias'
      vista:true
      description:'Síndico em 05/05/2015 às 16:19'
      prazo:'14/05/2015'
      pessoasEncarregadas: [
        {
          name:'Fulano de tal'
          cargo:'Cargo tal'
        }
        {
          name:'Fulano de tal'
          rgCpf:'1234567'
        }
      ]
      materiasServicos: [
        {
          titulo:'Argamassa'
          valor:'900,00'
        }
      ]
      comentarios: [
        {
          bg:'sc-bg-green'
          img:'http://cdn4.iconfinder.com/data/icons/general13/png/64/administrator.png'
          name:'Diego Felipe'
          comentario:'Fazendo muito barulho'
        }
        {
          bg:'sc-bg-green'
          icon:'sc-icon-comentario'
          name:'Diego Felipe'
          comentario:'açlsdjflkas alçsdjf alkçsd façlsd flkas dklf çaksd'
        }
        {
          bg:'sc-bg-green'
          icon:'sc-icon-comentario'
          name:'Diego Felipe'
          comentario:'Fazendo muito barulho'
        }
      ]
    }
  ]

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openModal = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined
]
