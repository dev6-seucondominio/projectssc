angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", "$element", "$window", ($filter, $scope, $scModal, $timeout, $scTarget, element, win) ->
  #### Variaveis
  $scope.include =
      mes:'Acumulado 2015'
      previsto:'50.000,00'
      realizado:'45.500,00'
  $scope.previsoesMensais = [
    {
      mes:'janeiro'
      previsto:'9.000,00'
      realizado:'8.5000,00'
      detalhes: [
        {
          name:'1. Receitas'
          previsto:'50.000,00'
          realizado:'30.000,00'
        }
        {
          name:'1.2 Fundo de Reseva'
          previsto:'5.000,00'
          realizado:'3.000,00'
        }
        {
          name:'1.3 Acordo'
          previsto:'20.000,00'
          realizado:'10.000,00'
        }
        {
          name:'1.4 Salão de Festa'
          previsto:'1.000,00'
          realizado:'800,00'
        }
        {
          name:'2. Despesas'
          previsto:'40.000,00'
          realizado:'30.000,00'
        }
        {
          name:'2.1 Despesas com Água'
          previsto:'3.000,0'
          realizado:'12.500,00'
        }
        {
          name:'2.2 Salário com pessoal'
          previsto:'10.000,0'
          realizado:'8.000,00'
        }
        {
          name:'2.3 Energia'
          previsto:'3.500,0'
          realizado:'500,00'
        }
        {
          name:'2.4 Pinutar e Reforma'
          previsto:'3.500,0'
          realizado:'2.000,00'
        }
        {
          name:'2.5 Manutenção Cameras'
          previsto:'3.500,0'
          realizado:'1.000,00'
        }
      ]
    }
    {
      mes:'Fevereiro'
      previsto:'9.000,00'
      realizado:'8.5000,00'
      detalhes: [
        {
          name:'1. Receitas'
          previsto:'50.000,00'
          realizado:'30.000,00'
        }
        {
          name:'1.2 Fundo de Reseva'
          previsto:'5.000,00'
          realizado:'3.000,00'
        }
        {
          name:'1.3 Acordo'
          previsto:'20.000,00'
          realizado:'10.000,00'
        }
        {
          name:'1.4 Salão de Festa'
          previsto:'1.000,00'
          realizado:'800,00'
        }
        {
          name:'2. Despesas'
          previsto:'40.000,00'
          realizado:'30.000,00'
        }
        {
          name:'2.1 Despesas com Água'
          previsto:'3.000,0'
          realizado:'12.500,00'
        }
        {
          name:'2.2 Salário com pessoal'
          previsto:'10.000,0'
          realizado:'8.000,00'
        }
        {
          name:'2.3 Energia'
          previsto:'3.500,0'
          realizado:'500,00'
        }
        {
          name:'2.4 Pinutar e Reforma'
          previsto:'3.500,0'
          realizado:'2.000,00'
        }
        {
          name:'2.5 Manutenção Cameras'
          previsto:'3.500,0'
          realizado:'1.000,00'
        }
      ]
    }
    {
      mes:'Março'
      previsto:'9.000,00'
      realizado:'8.5000,00'
      detalhes: [
        {
          name:'1. Receitas'
          previsto:'50.000,00'
          realizado:'30.000,00'
        }
        {
          name:'1.2 Fundo de Reseva'
          previsto:'5.000,00'
          realizado:'3.000,00'
        }
        {
          name:'1.3 Acordo'
          previsto:'20.000,00'
          realizado:'10.000,00'
        }
        {
          name:'1.4 Salão de Festa'
          previsto:'1.000,00'
          realizado:'800,00'
        }
        {
          name:'2. Despesas'
          previsto:'40.000,00'
          realizado:'30.000,00'
        }
        {
          name:'2.1 Despesas com Água'
          previsto:'3.000,0'
          realizado:'12.500,00'
        }
        {
          name:'2.2 Salário com pessoal'
          previsto:'10.000,0'
          realizado:'8.000,00'
        }
        {
          name:'2.3 Energia'
          previsto:'3.500,0'
          realizado:'500,00'
        }
        {
          name:'2.4 Pinutar e Reforma'
          previsto:'3.500,0'
          realizado:'2.000,00'
        }
        {
          name:'2.5 Manutenção Cameras'
          previsto:'3.500,0'
          realizado:'1.000,00'
        }
      ]
    }
    {
      mes:'Abril'
      previsto:'9.000,00'
      realizado:'8.5000,00'
      detalhes: [
        {
          name:'1. Receitas'
          previsto:'50.000,00'
          realizado:'30.000,00'
        }
        {
          name:'1.2 Fundo de Reseva'
          previsto:'5.000,00'
          realizado:'3.000,00'
        }
        {
          name:'1.3 Acordo'
          previsto:'20.000,00'
          realizado:'10.000,00'
        }
        {
          name:'1.4 Salão de Festa'
          previsto:'1.000,00'
          realizado:'800,00'
        }
        {
          name:'2. Despesas'
          previsto:'40.000,00'
          realizado:'30.000,00'
        }
        {
          name:'2.1 Despesas com Água'
          previsto:'3.000,0'
          realizado:'12.500,00'
        }
        {
          name:'2.2 Salário com pessoal'
          previsto:'10.000,0'
          realizado:'8.000,00'
        }
        {
          name:'2.3 Energia'
          previsto:'3.500,0'
          realizado:'500,00'
        }
        {
          name:'2.4 Pinutar e Reforma'
          previsto:'3.500,0'
          realizado:'2.000,00'
        }
        {
          name:'2.5 Manutenção Cameras'
          previsto:'3.500,0'
          realizado:'1.000,00'
        }
      ]
    }
    {
      mes:'Maio'
      previsto:'9.000,00'
      realizado:'8.5000,00'
      detalhes: [
        {
          name:'1. Receitas'
          previsto:'50.000,00'
          realizado:'30.000,00'
        }
        {
          name:'1.2 Fundo de Reseva'
          previsto:'5.000,00'
          realizado:'3.000,00'
        }
        {
          name:'1.3 Acordo'
          previsto:'20.000,00'
          realizado:'10.000,00'
        }
        {
          name:'1.4 Salão de Festa'
          previsto:'1.000,00'
          realizado:'800,00'
        }
        {
          name:'2. Despesas'
          previsto:'40.000,00'
          realizado:'30.000,00'
        }
        {
          name:'2.1 Despesas com Água'
          previsto:'3.000,0'
          realizado:'12.500,00'
        }
        {
          name:'2.2 Salário com pessoal'
          previsto:'10.000,0'
          realizado:'8.000,00'
        }
        {
          name:'2.3 Energia'
          previsto:'3.500,0'
          realizado:'500,00'
        }
        {
          name:'2.4 Pinutar e Reforma'
          previsto:'3.500,0'
          realizado:'2.000,00'
        }
        {
          name:'2.5 Manutenção Cameras'
          previsto:'3.500,0'
          realizado:'1.000,00'
        }
      ]
    }
  ]
  $scope.listaInputs      = [
    {
      id: 0
      description:'Descrição'
      ano: 0
      valor: 10
      planoContas: 0
    }
  ]

  $scope.novaPrevisao            = new $scModal()
  $scope.editarPrevisto          = new $scModal()
  $scope.scrollPos               = document.body.scrollTop || document.documentElement.scrollTop || 0
  $scope.scrollPosBoll           = $scope.scrollPos >= 23 ? true : false
  $scope.scrollPosBollMobile     = false
  scrollPosBack                  = $scope.scrollPos
  #### Variaveis

  #### funções

  $scope.addCamp = ()->
    $scope.listaInputs.push
      id: $scope.listaInputs.lenght-1
      description:''
      ano:''
      valor: 0
      planoContas:''

  $scope.rmvCamp = (obj)->
    $scope.listaInputs.splice(obj.id, 1)

  $scope.scrollTopButton = ->
    $("body,html").animate({scrollTop: 0}, "slow")

  win.onscroll = ->
    $scope.scrollPos = document.body.scrollTop || document.documentElement.scrollTop || 0
    console.log element.width()
    if element.width() >= 694
      $scope.mobile = false
      if $scope.scrollPos >= 23
        $scope.scrollPosBoll = true
      else
        $scope.scrollPosBoll = false
    else
      $scope.mobile = true
      if $scope.scrollPos > (scrollPosBack)
        $scope.scrollPosBollMobile = true
      if $scope.scrollPos <= (scrollPosBack)
        $scope.scrollPosBollMobile = false
      scrollPosBack = $scope.scrollPos
    if $scope.scrollPos == 0
      $timeout ->
        $scope.testeclick = ''
    $scope.$apply()

  mobileBool = ->
    if element.width() >= 694
      return false
    else
      return true

  $scope.toggleAcc = (pag)->
    if mobileBool()
      p.acc = false for p in $scope.previsoesMensais unless pag.acc
    pag.acc = !pag.acc

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openAcc = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined
]
