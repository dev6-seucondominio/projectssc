angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  $scope.addNew = {}
  $scope.novaInfracaoModal = new $scModal()

  $scope.listaTipo = [
    {name:'Limpeza'}
    {name:'Obra'}
    {name:'Manutenção'}
    {name:'Outros'}
  ]

  $scope.ocorrenciaINflacao = [
    {
      id:'1'
      name: 'Reclamação de barulho'
      criado:'Tiago Hermano'
      situacao:false
      data:'2015-05-26'
      autor:'Tiago Hermano (Bloco 1 - Apartamento 1)'
    }
    {
      id:'2'
      name: 'Reclamação de barulho'
      criado:'Tiago Hermano'
      situacao:true
      data:'2015-05-26'
      autor:'Tiago Hermano (Bloco 1 - Apartamento 1)'
    }
    {
      id:'3'
      name: 'Reclamação de barulho'
      criado:'Tiago Hermano'
      situacao:false
      data:'2015-05-26'
      autor:'Tiago Hermano (Bloco 1 - Apartamento 1)'
    }
    {
      id:'4'
      name: 'Reclamação de barulho'
      criado:'Tiago Hermano'
      situacao:true
      data:'2015-05-26'
      autor:'Tiago Hermano (Bloco 1 - Apartamento 1)'
    }
  ]

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openModal = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined
]
