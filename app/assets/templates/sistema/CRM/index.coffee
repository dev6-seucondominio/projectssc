angular.module "myapp", [
  "sc.app.helpers"
  "ui.sortable"
]

.run [
  '$rootScope', 'scAlert', 'scTopMessages'
  ($rootScope, scAlert, scTopMessages)->
    $rootScope.scAlert = scAlert
    $rootScope.scTopMessages = scTopMessages
]

.service "CRMService", [
  "$filter"
  ($filter) ->
    helpers =
      vendedor:
        cadastrar: (vend, reg, id) ->
          if vend.active
            reg.vendedores.push
              nome: vend.nome
              id: vend.id
          else
            item = $filter('filter')(reg.vendedores, {'id': id})[0]
            index = reg.vendedores.indexOf item
            reg.vendedores.splice(index, 1)
            vend.active = false

      ramosAtividade:
        cadastrar: (ramo, reg, id) ->
          if ramo.active
            reg.ramosAtividade.push
              nome: ramo.nome
              id: ramo.id
          else
            item = $filter('filter')(reg.ramosAtividade, {'id': id})[0]
            index = reg.ramosAtividade.indexOf item
            reg.ramosAtividade.splice(index, 1)
            ramo.active = false

    return {
      vendedor:
        cadastrar: (vendedor, registro, id) ->
          return helpers.vendedor.cadastrar(vendedor, registro, id)
      ramosAtividade:
        cadastrar: (ramo, registro, id) ->
          return helpers.ramosAtividade.cadastrar(ramo, registro, id)
    }
]

.controller "CRM::DemonstracaoClientes", [
  "$scope", "$filter", "$scModal", "scTopMessages", "scAlert"
  ($scope, $filter, $scModal, scTopMessages, scAlert) ->

    $scope.ModalDemonstracao = new $scModal()

    $scope.demonstracaoClientes = [
      {nome: "Editar Compras",    link: "#"}
      {nome: "Pagamentos",        link: "#"}
      {nome: "Relatório Semanal", link: "#"}
      {nome: "Extrato",           link: "#"}
      {nome: "Moradores",         link: "#"}
      {nome: "Agenda",            link: "#"}
    ]

    demoDefaultValues = ->
      $scope.novaDemo =
        nome: ""
        link: ""
        cadastrando: false
        editando: false
    demoDefaultValues()

    $scope.criarSalvarDemo = ->
      if $scope.novaDemo.editando
        $scope.demonstracaoClientes[$scope.index].nome = $scope.novaDemo.nome
        $scope.demonstracaoClientes[$scope.index].link = $scope.novaDemo.link
        $scope.index = null
      else
        $scope.demonstracaoClientes.push
          nome: $scope.novaDemo.nome
          link: $scope.novaDemo.link
      demoDefaultValues()

    $scope.estaEditando = ->
      $scope.novaDemo.cadastrando || $scope.novaDemo.editando

    $scope.editarDemo = (item) ->
      $scope.index = pegarIndex(item)
      $scope.novaDemo.nome = $scope.demonstracaoClientes[$scope.index].nome
      $scope.novaDemo.link = $scope.demonstracaoClientes[$scope.index].link
      $scope.novaDemo.editando = true

    $scope.cancelarEdicao = ->
      demoDefaultValues()

    $scope.excluirDemo = (item) ->
      $scope.demonstracaoClientes.splice(pegarIndex(item), 1)

    pegarIndex = (item) ->
      $scope.demonstracaoClientes.indexOf item
]

.controller "CRM::Vendedores", [
  "$scope", "$filter"
  ($scope, $filter) ->

    $scope.init = (vendedores) ->
      $scope.vendedores = vendedores

    $scope.vendedores = [
      {nome: "Germano", id: 1}
      {nome: "Guilherme", id: 2}
      {nome: "Gumercino", id: 3}
      {nome: "Lúcio", id: 4}
      {nome: "Rodrigo", id: 5}
    ]

    $scope.limiteItensVisiveis = 1
    $scope.verTodos = false

    $scope.changeVerTodos = ->
      $scope.verTodos = !$scope.verTodos

    $scope.itensSelecionados = (lista) ->
      $filter('filter')(lista, {'active': true})

    $scope.itensSelecionadosVisiveis = (lista, registro) ->
      for vend in registro.vendedores
        item.active = true for item in lista when item.nome == vend.nome
      itens = $scope.itensSelecionados(lista)
      return itens.slice(0, $scope.limiteItensVisiveis) if !$scope.verTodos
      return itens

    $scope.qtdItensEscondidos = (lista) ->
      $scope.itensSelecionados(lista).length - $scope.limiteItensVisiveis
]

.controller "CRM::RamosAtividade", [
  "$scope", "$filter", "$scModal"
  ($scope, $filter, $scModal) ->

    $scope.ModalRamosAtividade = new $scModal()

    $scope.init = (ramosAtividade) ->
      $scope.ramosAtividade = ramosAtividade

    $scope.ramosAtividade = [
      {nome: "Condomínio", id: 1}
      {nome: "Hotel", id: 2}
      {nome: "Advocacia", id: 3}
      {nome: "Empresas", id: 4}
      {nome: "Imobiliária", id: 5}
      {nome: "Cotabilidade", id: 6}
    ]

    ramoDefaultValues = ->
      $scope.novoRamo = {
        nome: ""
        sugerirContato: ""
        observacoes: ""
        cadastrando: false
        editando: false
      }
    ramoDefaultValues()

    ultimoId = 6

    $scope.criarSalvarRamo = ->
      if $scope.novoRamo.editando
        $scope.ramosAtividade[$scope.index].nome = $scope.novoRamo.nome
        $scope.ramosAtividade[$scope.index].sugerirContato = $scope.novoRamo.sugerirContato
        $scope.ramosAtividade[$scope.index].observacoes = $scope.novoRamo.observacoes
        $scope.index = null
      else
        ultimoId++
        $scope.ramosAtividade.push
          nome: $scope.novoRamo.nome
          sugerirContato: $scope.novoRamo.sugerirContato
          observacoes: $scope.novoRamo.observacoes
          id: ultimoId
      ramoDefaultValues()

    $scope.estaEditando = ->
      $scope.novoRamo.cadastrando || $scope.novoRamo.editando

    $scope.editarRamo = (item) ->
      $scope.index = pegarIndex(item)
      $scope.novoRamo.nome = $scope.ramosAtividade[$scope.index].nome
      $scope.novoRamo.sugerirContato = $scope.ramosAtividade[$scope.index].sugerirContato
      $scope.novoRamo.observacoes = $scope.ramosAtividade[$scope.index].observacoes
      $scope.novoRamo.editando = true

    $scope.cancelarEdicao = ->
      ramoDefaultValues()

    $scope.excluirRamo = (item) ->
      $scope.ramosAtividade.splice(pegarIndex(item), 1)

    pegarIndex = (item) ->
      $scope.ramosAtividade.indexOf item

    $scope.limiteItensVisiveis = 1
    $scope.verTodos = false

    $scope.changeVerTodos = ->
      $scope.verTodos = !$scope.verTodos

    $scope.itensSelecionados = (lista) ->
      $filter('filter')(lista, {'active': true})

    $scope.itensSelecionadosVisiveis = (lista, registro) ->
      for ramo in registro.ramosAtividade
        item.active = true for item in lista when item.nome == ramo.nome
      itens = $scope.itensSelecionados(lista)
      return itens.slice(0, $scope.limiteItensVisiveis) if !$scope.verTodos
      return itens

    $scope.qtdItensEscondidos = (lista) ->
      $scope.itensSelecionados(lista).length - $scope.limiteItensVisiveis
]

.controller "CRM::StatusNegociacao", [
  "$scope", "$filter", "$scModal"
  ($scope, $filter, $scModal) ->

    $scope.ModalStatus = new $scModal()

    $scope.init = (status) ->
      $scope.status = status

    $scope.status = [
      {nome: "Aberta"}
      {nome: "Pendente"}
      {nome: "Fechada"}
      {nome: "Perdida"}
      {nome: "E-mail/Tel. Errado"}
    ]

    statusDefaultValues = ->
      $scope.novoStatus = {
        nome: ""
        cadastrando: false
        editando: false
      }
    statusDefaultValues()

    $scope.criarSalvarStatus = ->
      if $scope.novoStatus.editando
        $scope.status[$scope.index].nome = $scope.novoStatus.nome
        $scope.index = null
      else
        $scope.status.push(
          nome: $scope.novoStatus.nome
        )
      statusDefaultValues()

    $scope.estaEditando = ->
      $scope.novoStatus.cadastrando || $scope.novoStatus.editando

    $scope.editarStatus = (item) ->
      $scope.index = pegarIndex(item)
      $scope.novoStatus.nome = $scope.status[$scope.index].nome
      $scope.novoStatus.editando = true

    $scope.cancelarEdicao = ->
      statusDefaultValues()

    $scope.excluirStatus = (item) ->
      $scope.status.splice(pegarIndex(item), 1)

    pegarIndex = (item) ->
      $scope.status.indexOf item

    $scope.limiteItensVisiveis = 1
    $scope.verTodos = false

    $scope.changeVerTodos = ->
      $scope.verTodos = !$scope.verTodos

    $scope.itensSelecionados = (lista) ->
      $filter('filter')(lista, {'active': true})

    $scope.itensSelecionadosVisiveis = (lista) ->
      itens = $scope.itensSelecionados(lista)
      return itens.slice(0, $scope.limiteItensVisiveis) if !$scope.verTodos
      return itens

    $scope.qtdItensEscondidos = (lista) ->
      $scope.itensSelecionados(lista).length - $scope.limiteItensVisiveis
]

.controller "CRM::Qualificacao", [
  "$scope", "$scModal"
  ($scope, $scModal) ->

    $scope.ModalQualificacao = new $scModal()

    $scope.init = (qualificacao) ->
      $scope.qualificacao = qualificacao

    $scope.qualificacao = ["Frio", "Em Nutrição", "Inativo", "Reciclado", "Qualificado"]

    qualificacaoDefaultValues = ->
      $scope.novaQualificacao =
        tipo: ""
        cadastrando: false
        editando: false
    qualificacaoDefaultValues()

    $scope.criarSalvarQualificacao = ->
      if $scope.novaQualificacao.editando
        $scope.qualificacao[$scope.index] = $scope.novaQualificacao.tipo
        $scope.index = null
      else
        $scope.qualificacao.push(
          $scope.novaQualificacao.tipo
        )
      qualificacaoDefaultValues()

    $scope.estaEditando = ->
      $scope.novaQualificacao.cadastrando || $scope.novaQualificacao.editando

    $scope.editarQualificacao = (item) ->
      $scope.index = pegarIndex(item)
      $scope.novaQualificacao.tipo = $scope.qualificacao[$scope.index]
      $scope.novaQualificacao.editando = true

    $scope.cancelarEdicao = ->
      qualificacaoDefaultValues()

    $scope.excluirQualificacao = (item) ->
      $scope.qualificacao.splice(pegarIndex(item), 1)

    pegarIndex = (item) ->
      $scope.qualificacao.indexOf item
]

.controller "CRM::Ctrl", [
  "$scope", "CRMService", "$filter", "$scModal", "scAlert", "scTopMessages", "$timeout"
  ($scope, CRMService, $filter, $scModal, scAlert, scTopMessages, $timeout) ->

    $scope.envioTermo = new $scModal()
    $scope.anyModal = new $scModal()
    $scope.imgImplant = new $scModal()

    $scope.pesquisaInsa = new $scModal()

    $scope.termoUsoView = new $scModal()
    $scope.termoUso = new $scModal()

    $scope.errors =
      mensagem: false

    $scope.contatoAtual = {}

    $scope.contato =
      _edit: false
      _add: false
      params: {}

    $scope.abrirTermo = ()->
      $scope.contatoAtual.modalImplantacao.close()
      $scope.contatoAtual.modalEnvioTermo.close()
      $scope.termoUso.open()

    $scope.contratarOpen = ()->
      $scope.imgImplant.close()
      $scope.contatoAtual.modalImplantacao.open()
      $scope.contatoAtual._edit = true

    $scope.estados = ["BA","CE","GO","MG","SP","RJ"]
    $scope.cidades = ["Aparecida de Goiânia","Goiânia","São Paulo","Rio de Janeiro"]
    $scope.ramosAtividadeFiltro = [
      "Condomínio"
      "Hotel"
      "Advocacia"
      "Empresas"
      "Imobiliária"
      "Cotabilidade"
    ]

    $scope.to_presquisa =
      _show: false
      representante: ''
      listagem: []

    $scope.listaPesquisaPer = [
      {
        pergunta: 'O representante chegou no horário'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
      {
        pergunta: 'Como você foi atendido por nossos funcionários'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
      {
        pergunta: 'Ele foi capaz de responder a todas suas perguntas com pasciência e prontidão'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
      {
        pergunta: 'O Senhor(a) ficou satisfeito com o nosso produto'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
      {
        pergunta: 'Qua a probalidade de você recomendar os nossos serviços a um amigo ou colega'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
      {
        pergunta: 'O que você recomenda para melhorarmos nosso atendimento ao cliente'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
      {
        pergunta: 'Há algum síndico ou administradora que o Senhor(a) possa nos indicar'
        btnActive:[
          {active:false}, {active:false}, {active:false}, {active:false}, {active:false}
        ]
      }
    ]

    $scope.activeItem = (item, obj)->
      j.active = false for j in obj
      item.active = true

    $scope.preencherPesquisa = ()->
      if $scope.to_presquisa.representante == ""
        $scope.to_presquisa =
          _show: false
          representante: ""
          listagem: angular.copy $scope.listaPesquisaPer
      $scope.pesquisaInsa.open()

    $scope.abrirPesquisa = (obj)->
      $scope.to_presquisa =
        _show: true
        representante: obj.representante
        listagem: angular.copy obj.satisfacao
      $scope.pesquisaInsa.open()

    $scope.cadastrarVend = (vend, reg, id) ->
      CRMService.vendedor.cadastrar(vend, reg, id)

    $scope.cadastrarRamo = (ramo, reg, id) ->
      CRMService.ramosAtividade.cadastrar(ramo, reg, id)

    $scope.iconesFormaDeContato = [
      {tipo: "telefone",    icone: "sc-icon-telefone-1", active: true}
      {tipo: "email",       icone: "sc-icon-carta-4"}
      {tipo: "visita",      icone: "sc-icon-grupo-2"}
      {tipo: "demostracao", icone: "sc-icon-video-2"}
      {tipo: "chat",        icone: "sc-icon-comentario"}
      {tipo: "skype",       icone: "sc-icon-skype"}
      {tipo: "cobranca",    icone: "sc-icon-dinheiro-2"}
    ]

    $scope.iconeAtivo = (icone) ->
      item.active = item.tipo == icone for item in $scope.iconesFormaDeContato

    $scope.tipoDeContato = [
      {tipo: "Ativo",     icone: "sc-icon-seta-4-cima", active: true}
      {tipo: "Receptivo", icone: "sc-icon-seta-4-baixo"}
    ]

    $scope.tipoDeContatoAtivo = (contato) ->
      item.active = item.tipo == contato for item in $scope.tipoDeContato

    $scope.exibirNomes = (nomes) ->
      i = 1
      out = ""
      for item in nomes
        if i <= 3
          out += item.nome.split(" ")[0]
          if i == nomes.length - 1
            out += " e "
          else if i != nomes.length
            out += ", "
        else
          out += "..."
          break
        i++
      out

    $scope.novoCliente = ->
      $scope.registros.unshift(
        cliente: "Novo Cliente"
        ramosAtividade: []
        contatos: []
        vendedores: []
        negociacoes: []
      )

    $scope.apagarCliente = (index) ->
      $scope.registros.splice(index, 1)

    $scope.enviarEmailConfirm = ()->
      scAlert.open
        title: 'Confirmação'
        messages: ['Seus dados estão sendo processados.', 'Aguarde que você receberá um boleto de adesão no seu e-mail.']
        buttons: [
          label: 'Ok'
          color: 'green'
          action: ()->
            $timeout ->
              scTopMessages.openSuccess "E-mail enviado com sucesso!"
            , 5000
            $timeout ->
              scTopMessages.close()
            , 10500
            $scope.contatoAtual.modalImplantacao.close()
            $scope.contatoAtual.modalEnvioTermo.close()
      ]

    $scope.okTermo = ()->
      $scope.termoUsoView.close()

    $scope.enviarEmailCont = ()->
      $scope.termoUsoView.open()

    $scope.contratarConfirm = ()->
      scAlert.open
        title: 'Confirmação'
        messages: ['Será enviado para o e-mail do cliente um formulário solicitando os dados para implantação do sistema.', 'Deseja enviar ?']
        buttons: [
          {
            label: 'Voltar'
            color: 'blue'
            action: ()->
              $scope.contatoAtual.modalImplantacao.open()
              $scope.contatoAtual._edit = true
          }
          {
            label: 'Confirmar Dados'
            color: 'green'
            action: ()->
              $scope.contatoAtual.modalImplantacao.open()
              $scope.contatoAtual._edit = false
          }
      ]

    $scope.enviarEmail2 = (obj)->
      obj.modalImplantacao2.open()

    $scope.enviarEmail = (obj)->
      scAlert.open
        title: 'Confirmação'
        messages: ['Será enviado para o e-mail do cliente um formulário solicitando os dados para implantação do sistema.', 'Deseja enviar ?']
        buttons: [
          {
            label: 'Cancelar'
            color: 'red'
          }
          {
            label: 'Enviar'
            color: 'green'
            action: ()->
              $scope.contatoAtual = obj
              $scope.imgImplant.open()
          }
      ]

    $scope.novoContato = (registro) ->
      registro.contatos.push
        nome: "Novo Contato"

    $scope.apagarContato = (contatos, index) ->
      contatos.splice(index, 1)

    $scope.novaNegociacao = (registro) ->
      console.log registro.msgNegociacao
      if !registro.msgNegociacao
        $scope.errors.mensagem = true
        return
      registro.negociacoes.push(
        vendedor: "Germano"
        avatar: "http://lorempixel.com/48/48/people/2"
        mensagem: registro.msgNegociacao
        formaDeContato: $filter('filter')($scope.iconesFormaDeContato, {'active': true})[0].icone
        tipoDeContato: $filter('filter')($scope.tipoDeContato, {'active': true})[0].tipo
        iconeTipoDeContato: $filter('filter')($scope.tipoDeContato, {'active': true})[0].icone
        data: "09/01/2015 às 13:38"
        representante: $scope.to_presquisa.representante
        satisfacao: $scope.to_presquisa.listagem
      )
      registro.msgNegociacao = ""
      $scope.errors.mensagem = false
      $scope.to_presquisa =
        _show: false
        representante: ''
        listagem: []

    $scope.registros = [
      {
        cliente: "Cond. Ambar Marfin"
        contatoPartiu: false
        proximoContato: "30/02/2015"
        ultimoContato: "05/02/2015"
        ramosAtividade: [
          {nome: "Condomínio",  id: 1}
          {nome: "Hotel",       id: 2}
        ]
        qualificacao: "Qualificado"
        concorrente: false
        referencia: false
        maisDe80Und: false
        relevancia: "5"
        cobranca: "00.000.000/0001-00"
        observacoes: "It has survived not only five centuries, but also the leap into electronic typesetting."
        contatos: [
          {
            nome: "Pedro"
            email: "pedro@gmail.com"
            funcao: "Síndico"
            telefone: "62 8579-0731"
            telefone2: ""
            aniversario: ""
            cobranca: {email: false, telefone: true}
            modalEnvioTermo:    new $scModal()
            modalImplantacao:   new $scModal()
            modalImplantacao2:  new $scModal()
          }
          {
            nome: "Gumercino"
            email: "gumercino@gmail.com"
            funcao: "Porteiro"
            telefone: "62 8579-0731"
            telefone2: ""
            aniversario: ""
            cobranca: {email: true, telefone: false}
            modalEnvioTermo:    new $scModal()
            modalImplantacao:   new $scModal()
            modalImplantacao2:  new $scModal()
          }
          {
            nome: "José"
            email: "jose@gmail.com"
            funcao: "Porteiro"
            telefone: "62 8579-0731"
            telefone2: ""
            aniversario: ""
            cobranca: {email: true, telefone: false}
            modalEnvioTermo:    new $scModal()
            modalImplantacao:   new $scModal()
            modalImplantacao2:  new $scModal()
          }
        ]
        cep: "74914-540"
        estado: "GO"
        cidade: "Goiânia"
        logradouro: "R. São Francisco de Assis"
        bairro: "Jardim Maria Inez"
        complemento: ""
        exterior: false
        estadoExterior: ""
        cidadeExterior: ""
        vendedores: [
          {nome: "Germano",   id: 1}
          {nome: "Gumercino", id: 3}
        ]
        negociacoes: [
          {
            vendedor: "Lúcio"
            avatar: "http://lorempixel.com/48/48/people/1"
            mensagem: "Blablabla..."
            formaDeContato: "sc-icon-telefone-1"
            tipoDeContato: "Ativo"
            iconeTipoDeContato: "sc-icon-seta-4-cima"
            data: "09/01/2015 às 13:38"
            representante: 'Plasma'
            satisfacao: [
              {
                pergunta: 'O representante chegou no horário'
                btnActive:[
                  {active:true}, {active:false}, {active:false}, {active:false}, {active:false}
                ]
              }
              {
                pergunta: 'Como você foi atendido por nossos funcionários'
                btnActive:[
                  {active:false}, {active:true}, {active:false}, {active:false}, {active:false}
                ]
              }
              {
                pergunta: 'Ele foi capaz de responder a todas suas perguntas com pasciência e prontidão'
                btnActive:[
                  {active:false}, {active:false}, {active:true}, {active:false}, {active:false}
                ]
              }
              {
                pergunta: 'O Senhor(a) ficou satisfeito com o nosso produto'
                btnActive:[
                  {active:false}, {active:false}, {active:false}, {active:true}, {active:false}
                ]
              }
              {
                pergunta: 'Qua a probalidade de você recomendar os nossos serviços a um amigo ou colega'
                btnActive:[
                  {active:false}, {active:false}, {active:false}, {active:false}, {active:true}
                ]
              }
              {
                pergunta: 'O que você recomenda para melhorarmos nosso atendimento ao cliente'
                btnActive:[
                  {active:false}, {active:false}, {active:true}, {active:false}, {active:false}
                ]
              }
              {
                pergunta: 'Há algum síndico ou administradora que o Senhor(a) possa nos indicar'
                btnActive:[
                  {active:false}, {active:true}, {active:false}, {active:false}, {active:false}
                ]
              }
            ]
          }
          {
            vendedor: "Germano"
            avatar: "http://lorempixel.com/48/48/people/2"
            mensagem: "Blablabla..."
            formaDeContato: "sc-icon-grupo-2"
            tipoDeContato: "Receptivo"
            iconeTipoDeContato: "sc-icon-seta-4-baixo"
            data: "09/01/2015 às 13:38"
          }
          {
            vendedor: "Guilherme"
            avatar: "http://lorempixel.com/48/48/people/3"
            mensagem: "Blablabla..."
            formaDeContato: "sc-icon-video-2"
            tipoDeContato: "Ativo"
            iconeTipoDeContato: "sc-icon-seta-4-cima"
            data: "09/01/2015 às 13:38"
          }
          {
            vendedor: "Rogério"
            avatar: "http://lorempixel.com/48/48/people/4"
            mensagem: "Blablabla..."
            formaDeContato: "sc-icon-dinheiro-2"
            tipoDeContato: "Receptivo"
            iconeTipoDeContato: "sc-icon-seta-4-baixo"
            data: "09/01/2015 às 13:38"
          }
        ]
      }
      {
        cliente: "Cond. Parque dos Ipês"
        contatoPartiu: true
        proximoContato: "30/02/2015"
        ultimoContato: "05/02/2015"
        ramosAtividade:  [
          {nome: "Imobiliária", id: 5}
          {nome: "Hotel",       id: 2}
        ]
        qualificacao: "Inativo"
        concorrente: false
        referencia: false
        maisDe80Und: false
        relevancia: "5"
        cobranca: "00.000.000/0001-00"
        observacoes: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
        contatos: [
          {
            nome: "Gleidson"
            email: "gleidson@gmail.com"
            funcao: "Síndico"
            telefone: "62 8579-0731"
            telefone2: ""
            aniversario: ""
            cobranca: {email: false, telefone: false}
            modalEnvioTermo:    new $scModal()
            modalImplantacao:   new $scModal()
            modalImplantacao2:  new $scModal()
          }
        ]
        cep: "74914-540"
        estado: "GO"
        cidade: "Goiânia"
        logradouro: "R. São Francisco de Assis"
        bairro: "Jardim Maria Inez"
        complemento: ""
        exterior: false
        estadoExterior: ""
        cidadeExterior: ""
        vendedores: [
          {nome: "Lúcio", id: 4}
        ]
        negociacoes: [
          {
            vendedor: "Guilherme"
            avatar: "http://lorempixel.com/48/48/people/3"
            mensagem: "Blablabla..."
            formaDeContato: "sc-icon-carta-4"
            tipoDeContato: "Ativo"
            iconeTipoDeContato: "sc-icon-seta-4-cima"
            data: "09/01/2015 às 13:38"
          }
        ]
      }
    ]
]

.filter "scFilter", () ->
  (collection, search) ->
    if search
      regexp = createAccentRegexp(search)

      doesMatch = (txt) ->
        (''+txt).match(regexp)

      collection.filter (el) ->
        if typeof el == 'object'
          return true for att, value of el when (typeof value == 'string') && doesMatch(value)
        doesMatch(el)
        false
    else
      collection

menu =
  w:             $(window)
  topBar:        $('#top-bar')
  topBarContent: $('#top-bar-content')
  init: ->
    @setTopBarHeight()
  setTopBarHeight: ->
    @topBar.css 'height', @topBarContent[0].offsetHeight
menu.init()
menu.w.resize -> menu.setTopBarHeight()
