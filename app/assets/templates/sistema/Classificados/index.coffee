angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "controll", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->
  $scope.classificados = [
    {
      edit: false
      titulo: 'Vendo video-game XBOX 360'
      descricao: 'Video-game e tal e baljaçljldkfjadfadsçflasdjfl'
      photo: 'http://www.meliuz.com.br/blog/wp-content/uploads/2014/08/xbox_360.jpg'
      descricaoFull: 'açsldkfjaçsdlfasdlçkfaçsldkj naçlsdkjaçlskdjfçlkasdjfçlaksjd nasçldkfjasçldfkjasçldkfjasçdlkfjaçlskdjf'
      responsavel: 'Diego Felipe'
      contato: '(62) 8934-1983'
      origem: 'Apt. 101 - Bloco A'
      situacao: 'aberto'
      boolSituacao: false
      comentario: [
        {
          name:'Leandro Pinheiro'
          texto:'Vem com dois controles?'
        }
        {
          name:'Amanda Nunes'
          texto:'Boa noite, gostaria de saber de quanto é a memória dele e quais jogos você possui, para que eu possa comprar alguns de você mesmo. Desde já, obrigado'
        }
        {
          name:'Maria Luiza'
          texto:'Boa noite, ele é de 4gb. Tenho the last of Us, Left 4, Dead 2, street fighter e muitos outros. Podemos Negociar.'
        }
      ]
    }
    {
      edit: false
      titulo: 'Vendo video-game XBOX 360'
      descricao: 'Video-game e tal e baljaçljldkfjadfadsçflasdjfl'
      photo: 'http://www.meliuz.com.br/blog/wp-content/uploads/2014/08/xbox_360.jpg'
      descricaoFull: 'açsldkfjaçsdlfasdlçkfaçsldkj naçlsdkjaçlskdjfçlkasdjfçlaksjd nasçldkfjasçldfkjasçldkfjasçdlkfjaçlskdjf'
      responsavel: 'Diego Felipe'
      contato: '(62) 8934-1983'
      origem: 'Apt. 101 - Bloco A'
      situacao: 'fechado'
      boolSituacao: true
      comentario: [
        {
          name:'Leandro Pinheiro'
          texto:'Vem com dois controles?'
        }
        {
          name:'Amanda Nunes'
          texto:'Boa noite, gostaria de saber de quanto é a memória dele e quais jogos você possui, para que eu possa comprar alguns de você mesmo. Desde já, obrigado'
        }
        {
          name:'Maria Luiza'
          texto:'Boa noite, ele é de 4gb. Tenho the last of Us, Left 4, Dead 2, street fighter e muitos outros. Podemos Negociar.'
        }
      ]
    }
  ]
]

.controller "listaClass", ["$scope", "$timeout", ($scope, $timeout) ->
  #### Variaveis
  $scope.editClassificadoTemp = {}
  $scope.temId = {}
  #### Variaveis

  #### funções
  $scope.editLista = (lista) ->
    lista.edit = true
    $scope.editClassificadoTemp = angular.copy(lista)
    $scope.temId.idEdit = $scope.classificados.indexOf(lista)
    if lista.situacao == 'Fechado'
      $scope.editClassificadoTemp.boolSituacao = false
    else
      $scope.editClassificadoTemp.boolSituacao = true

  $scope.init = (lista) ->
    lista.bgColorAcc = 'background-color: #f3f3f4;'
    lista.bColor = 'border-right: 1px solid #e0e2e3;'

  $scope.openAcc = (lista) ->
    lista.opened = !lista.opened
    if lista.opened
      lista.bgColorAcc = 'background-color: #337ab7;'
      lista.bColor = 'border-right: 1px solid #438ccb;'
      lista.txtColor = 'color: white;'
    else
      lista.bgColorAcc = 'background-color: #f3f3f4;'
      lista.bColor = 'border-right: 1px solid #e0e2e3;'
      lista.txtColor = undefined

  $scope.salveEdit = ->
    if confirm 'Deseja realmente salvar a edição do classificado ?'
      $scope.classificados[$scope.temId.idEdit] = $scope.editClassificadoTemp
      lista = $scope.classificados[$scope.temId.idEdit]
      if lista.boolSituacao
        lista.situacao = 'Aberto'
      else
        lista.situacao = 'Fechado'
      lista.edit = false
      $timeout ->
        lista.bgColorAcc = 'background-color: #337ab7;'
        lista.bColor = 'border-right: 1px solid #438ccb;'
        lista.txtColor = 'color: white;'

  $scope.cancellEdit = ->
    if confirm 'Deseja realmente cancelar a edição do classificado ?'
      $scope.editClassificadoTemp = {}
      $scope.classificados[$scope.temId.idEdit].edit = false
  #### funções
]
