angular.module "scApp", [ "sc.app.helpers", "ui.bootstrap", "angular.filter"]

.controller "EntradaCtrl", ["$filter", "$scope", "$scModal", "$timeout", "$scTarget", ($filter, $scope, $scModal, $timeout, $scTarget) ->

  ## Modais Início
  $scope.cadastro = new $scModal()
  $scope.configuracoes = new $scModal()
  $scope.editarCadastro = new $scModal()
  $scope.editSaidasModal = new $scModal()
  $scope.editCadastroModal = new $scModal()
  $scope.editEntradasModal = new $scModal()
  $scope.editPendentesModal = new $scModal()
  $scope.editarCadastroSaida = new $scModal()
  $scope.cadastroVeiculoModal = new $scModal()
  $scope.editarCadastroAddVisit = new $scModal()
  ## Modais Final

  ## Variáveis Início
  size = null
  bool1 = bool2 = bool3 = bool4 = bool5 = true
  $scope.color1 = $scope.color2 = $scope.color3 = 'sc-btn-blue'
  $scope.caixaCadastro = {}
  $scope.indexSelected = $scope.indexSelectedDestinoAdvance = $scope.indexSelectedVeicleAdvance = $scope.indexSelectedNomeAdvance = $scope.indexSelectedpendentesEdit = $scope.indexSelectedDestinoEditSaidas = $scope.indexSelectedMotivoEditSaidas = $scope.indexSelectedVeicleEditSaidas = $scope.indexSelectedExitEdit = $scope.indexSelectedEnterEdit = $scope.indexSelectedCadastroDes = $scope.indexSelectedCadastro = $scope.indexSelectedDestino  = $scope.indexSelectedMotivo = -1
  $scope.accordionShow = {}
  $scope.cadastroPessoa = {}
  $scope.cadastroVeiculo = {}
  $scope.editarCadastroV = false
  $scope.peopleSaidasEdit = {}
  $scope.motivoVisitaTemp = {}
  $scope.editCadastroTemp = {}
  $scope.caixaNovoVeiculo = {}
  $scope.borderStyleSaidas = 'border-right: 1px solid #e0e2e3;'
  $scope.caixaFiltroAdvance = {}
  $scope.peopleSaidasEditar = {}
  $scope.peopleEntradasEdit = {}
  $scope.peopleEditCadastro = {}
  $scope.cadastroVeiculoNew = {}
  $scope.caixaNovoVisitante = {}
  $scope.editarCadastroTemp = {}
  $scope.editarVisitanteTemp = {}
  $scope.peoplePendentesEdit = {}
  $scope.borderStyleEntradas = 'border-right: 1px solid #438ccb;'
  $scope.borderStylePendentes = 'border-right: 1px solid #e0e2e3;'
  $scope.peopleEntradasEditar = {}
  $scope.peoplePendentesEditar = {}
  $scope.caixaNovoVisitanteEdit = {}
  $scope.motivoVisitaTempAdvance = {}
  $scope.editarCadastroSaidasTemp = {}
  $scope.cadastroMotivoVisitaTemp = {}
  $scope.backgroundColorAccSaidas = 'background-color: #f3f3f4;'
  $scope.backgroundColorAccEntradas = 'background-color: #337ab7;'
  $scope.editarCadastroEntradasTemp = {}
  $scope.editarCadastroPendentesTemp = {}
  $scope.backgroundColorAccPendentes = 'background-color: #f3f3f4;'
  $scope.aplicarConfiguracao = {
    nomeCompleto: true
    rg: true
    Oexpedidor: true
    cpf: false
    foto: false
    modelo: true
    placa: true
    cor: true
    tag: true
    editarVisitante: true
    excluirVisitante: true
    editarDH: true
  }
  $scope.aplicarConfiguracaoTemp = {}

  $scope.pessoas = [ #Dados do visitante
    {
      id: 0
      name: 'João Paulo'
      cpf: '111.111.111-11'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: true
    }
    {
      id: 1
      name: 'Diego Felipe'
      cpf: '111.111.111-11'
      rg: '1111111'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: true
    }
    {
      id: 2
      name: 'Felipe Silva'
      cpf: '111.111.111-11'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: false
    }
    {
      id: 3
      name: 'Diego'
      cpf: '333.333.333-33'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: false
    }
    {
      id: 4
      name: 'Angular Crazy'
      cpf: '111.111.111-11'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://eucompraria.com.br/media/img/produtos/caneca-angular/caneca-angular2.jpg'
      saiu: false
    }
    {
      id: 5
      name: 'Alasca'
      cpf: '333.333.333-33'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: false
    }
    {
      id: 6
      name: 'John Green'
      cpf: '111.111.111-11'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: true
    }
    {
      id: 7
      name: 'Bujão'
      cpf: '111.111.111-11'
      rg: '0000000'
      Oexpedidor: 'AAA'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: true
    }
    {
      id: 8
      name: 'Joana Dark'
      cpf: '222.222.222-22'
      rg: '1234567'
      Oexpedidor: 'SSP'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: false
    }
    {
      id: 9
      name: 'Carlos Drummond'
      cpf: '222.222.222-22'
      rg: '1234567'
      Oexpedidor: 'SSP'
      photo: 'http://i.imgur.com/UfMLctB.jpg'
      saiu: false
    }
  ]
  $scope.veiculos = [ #Dados de Veículos
    {
      id: 0
      name: 'Audi A8 0'
      placa: 'KWH-0000'
      cor: 'preto'
      saiu: true
    }
    {
      id: 1
      name: 'Audi A8 1'
      placa: 'KWH-0000'
      cor: 'preto'
      saiu: false
    }
  ]

  $scope.detalhesVisita = [ #Detalhes da visita
    { name: 'Visitante' }
    { name: 'Prestador de Serviço' }
    { name: 'Entregador' }
    { name: 'Funcionário' }
    { name: 'Outros' }
  ]
  $scope.detalhesDestino = [ #Destinos da visita
    { name: 'Condomínio' }
    { name: 'Bloco 1 - 101' }
    { name: 'Bloco 1 - 102' }
    { name: 'Bloco 1 - 103' }
    { name: 'Bloco 1 - 104' }
    { name: 'Bloco 2 - 101' }
    { name: 'Bloco 2 - 102' }
    { name: 'Bloco 2 - 103' }
    { name: 'Bloco 3 - 101' }
    { name: 'Bloco 3 - 102' }
    { name: 'Outro' }
  ]

  $scope.buffer = []

  $scope.entradas = [
    {
      id: 2

      destino: 'Bloco 1 - 101'
      data: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      motivoVisita: 'Visitante'
      observacaoMotivo: 'Observações motivo...'
      autorizacao: 'ADM'
      tag: '002'
      car: false
      pendente: false
    }
    {
      id: 3

      destino: 'Bloco 1 - 101'
      data: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      motivoVisita: 'Visitante'
      observacaoMotivo: 'Observações motivo...'
      autorizacao: 'ADM'
      tag: '003'
      car: false
      pendente: false
    }
    {
      id: 4

      destino: 'Bloco 2 - 101'
      data: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      motivoVisita: 'Visitante'
      observacaoMotivo: 'Observações motivo...'
      autorizacao: 'ADM'
      tag: '004'
      car: false
      pendente: false
    }
    {
      id: 5

      destino: 'Bloco 1 - 101'
      data: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      prestador: 'Google'
      motivoVisita: 'Prestador de Serviço'
      observacaoMotivo: 'Observações motivo...'
      autorizacao: 'ADM'
      tag: '001'
      car: true
      carId: 1
      pendente: false
    }
    {
      id: 8

      destino: 'Bloco 1 - 101'
      data: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      motivoVisita: 'Prestador de Serviço'
      prestador: 'Google'
      observacaoMotivo: 'Prestador de Serviço'
      autorizacao: 'ADM'
      tag: '001'
      car: false
      pendente: true
    }
    {
      id: 9

      destino: 'Bloco 1 - 101'
      data: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      motivoVisita: 'Visitante'
      observacaoMotivo: 'Observações motivo...'
      autorizacao: 'ADM'
      tag: '009'
      car: false
      pendente: true
    }
  ]
  $scope.saidas = [
    {
      id: 1

      destino: 'Bloco 1 - 101'
      data: '2015-05-12'
      dataExit: '2015-05-12'
      hora: "2015-06-12T15:20:00.000Z"
      horaExit: '2015-06-12T15:20:00.000Z'
      motivoVisita: 'Visitante'
      observacaoMotivo: 'Observações motivo...'
      autorizacao: 'ADM'
      tag: '001'
      car: true
      carId: 1
    }
  ]
  ## Variáveis Fim

  ## Filtros Início
  $scope.motivoFilterEditEntradas = ->
    $scope.motivoEditEntradasFilter = $filter('filter')($scope.detalhesVisita, {} )

  $scope.destinoSizeAdvanceFilter = ->
    $scope.destinoSizeAdvance = $filter('filter')($scope.detalhesDestino, { name: $scope.motivoVisitaTempAdvance.destino })

  $scope.entradasSemPendentes = ->
    $scope.sizeEntradasSPendente = $filter('filter')($scope.entradas, { pendente: 'false' })

  $scope.entradasVeiclecoFilterEdit = ->
    $scope.veicleControllEntradas = $filter('filter')($scope.veiculos, { name: $scope.peopleEntradasEditar.veicle, saiu: 'true' })

  $scope.saidasVeicleFilterEdit = ->
    $scope.veicleControllSaidas = $filter('filter')($scope.veiculos, { name: $scope.peopleSaidasEditar.veicle })

  $scope.destinoSizeI = ->
    $scope.destinoCadastroControll = $filter('filter')($scope.detalhesDestino, { name: $scope.cadastroMotivoVisitaTemp.destino })

  $scope.sizeVeicleFilterCadastro = ->
    $scope.veicleControllList = $filter('filter')($scope.veiculos, { name: $scope.cadastroVeiculo.car })

  $scope.sizeVeicleFilter = ->
    $scope.veicleControllPrin = $filter('filter')($scope.veiculos, { name: $scope.searchVeicle, saiu: 'true' })

  $scope.sizeVeicleFilterAdvance = ->
    $scope.veicleControllAdvance = $filter('filter')($scope.veiculos, { name: $scope.searchVeicleAdvance })

  $scope.visitanteFilterEdit = ->
    $scope.pessoasEditFilter = $filter('filter')($scope.pessoas, { name: $scope.peopleEditCadastro.name, saiu:'true' })

  $scope.visitanteEntradasFilterEdit = ->
    $scope.pessoasEntradasEditFilter = $filter('filter')($scope.pessoas, { name: $scope.peopleEntradasEditar.name, saiu:'true' })

  $scope.visitantePendentesFilterEdit = ->
    $scope.pessoasPendentesEditFilter = $filter('filter')($scope.pessoas, { name: $scope.peoplePendentesEditar.name, saiu:'true' })

  $scope.visitanteSaidasFilterEdit = ->
    $scope.pessoasSaidasEditFilter = $filter('filter')($scope.pessoas, { name: $scope.peopleSaidasEditar.name, saiu:'true' })

  $scope.sizeVeicleFilterEdit = ->
    $scope.veicleFilterEdit = $filter('filter')($scope.veiculos, { name: $scope.editarCadastroTemp.veicle, saiu: 'true' })

  $scope.motivoFilterEditSaidas = ->
    $scope.controllMotivoEditSaidas = $filter('filter')($scope.detalhesVisita, { name: $scope.editarCadastroSaidasTemp.motivoVisita })

  $scope.motivoFilterEdit = ->
    $scope.controllMotivoEdit = $filter('filter')($scope.detalhesVisita, { name: $scope.editarCadastroTemp.motivoVisita })

  $scope.DestinoFilterEditEntradas = ->
    $scope.controllDestinoEditEntradas = $filter('filter')($scope.detalhesDestino, { name: $scope.editarCadastroEntradasTemp.destino })

  $scope.DestinoFilterEditSaidas = ->
    $scope.controllDestinoEditSaidas = $filter('filter')($scope.detalhesDestino, { name: $scope.editarCadastroSaidasTemp.destino })

  $scope.DestinoFilterEdit = ->
    $scope.controllDestinoEdit = $filter('filter')($scope.detalhesDestino, { name: $scope.editarCadastroTemp.destino })
  ## Filtros Fim

  ## Funções Início
  $scope.editarCadastroVisitanteNewVisit = ->
    $scope.editSaidasModal.close()
    $scope.editarVisitanteTemp = angular.copy($scope.pessoas[$scope.editarCadastroTemp.id])
    $timeout ->
      $scope.editarCadastroAddVisit.open()

  $scope.savaEditarCadastroNewVisit = ->
    $scope.pessoas[$scope.editarCadastroTemp.id] = $scope.editarVisitanteTemp
    $scope.editarCadastroAddVisit.close()
    $timeout ->
      $scope.editCadastroSaidas($scope.saveIdlist)

  $scope.cancellEditarCadastroNewVisit = ->
    $scope.editarCadastroAddVisit.close()
    $timeout ->
      $scope.editCadastroSaidas($scope.saveIdlist)

  $scope.editarCadastroVisitanteSaidas = ->
    $scope.editSaidasModal.close()
    $scope.editarVisitanteTemp = angular.copy($scope.pessoas[$scope.editarCadastroSaidasTemp.id])
    $timeout ->
      $scope.editarCadastroSaida.open()

  $scope.savaEditarCadastroSaidas = ->
    $scope.pessoas[$scope.editarCadastroSaidasTemp.id] = $scope.editarVisitanteTemp
    $scope.editarCadastroSaida.close()
    $timeout ->
      $scope.editCadastroSaidas($scope.saveIdSaidaslist)

  $scope.cancellEditarCadastroVisitanteSaidas = ->
    $scope.editarCadastroSaida.close()
    $timeout ->
      $scope.editCadastroSaidas($scope.saveIdSaidaslist)

  $scope.editarCadastroVisitante = ->
    $scope.editEntradasModal.close()
    $scope.editarVisitanteTemp = angular.copy($scope.pessoas[$scope.editarCadastroEntradasTemp.id])
    $timeout ->
      $scope.editarCadastro.open()

  $scope.savaEditarCadastroVisitante = ->
    $scope.pessoas[$scope.editarCadastroEntradasTemp.id] = $scope.editarVisitanteTemp
    $scope.editarCadastro.close()
    $timeout ->
      $scope.editCadastroEntradas($scope.saveIdEntradaslist)

  $scope.cancellEditarCadastroVisitante = ->
    $scope.editarCadastro.close()
    $timeout ->
      $scope.editCadastroEntradas($scope.saveIdEntradaslist)

  $scope.salvarConfiguracao = ->
    $scope.aplicarConfiguracao = angular.copy($scope.aplicarConfiguracaoTemp)
    $scope.configuracaoIcon = false
    $scope.configuracoes.close()

  $scope.cancelarConfiguracao = ->
    $scope.configuracoes.close()
    $scope.configuracaoIcon = false

  $scope.configuracoesGerais = ->
    $scope.configuracaoIcon = true
    $scope.aplicarConfiguracaoTemp = angular.copy($scope.aplicarConfiguracao)
    $scope.configuracoes.open()

  $scope.navegationDestinoAdvance = (keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedDestinoAdvance > 0
          $scope.indexSelectedDestinoAdvance -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedDestinoAdvance < $scope.destinoSizeAdvance.length-1
          $scope.indexSelectedDestinoAdvance += 1
      when keyMap.ENTER
        add = $scope.destinoSizeAdvance[$scope.indexSelectedDestinoAdvance]
        $scope.selectMotivoDestinoAdvance(add)
      else
        $scope.indexSelectedDestinoAdvance = -1

  $scope.selectMotivoDestinoAdvance = (add) ->
    $scope.motivoVisitaTempAdvance.destino = add.name
    $scope.focarElemento($scope.caixaFiltroAdvance, "caixaFiltroAdvance.dataInit")
    $timeout ->
      $scope.searchDestinoClickAdvance = false

  $scope.hoverAddDestinoAdvance = (destino) ->
    destino.hover = true
    $scope.indexSelectedDestinoAdvance = -1

  $scope.navegationVeiculoAdvance = (key, funcao) -> #O.K.
    switch key
      when keyMap.ARROW.UP
        if $scope.indexSelectedVeicleAdvance > 0
          $scope.indexSelectedVeicleAdvance -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedVeicleAdvance < $scope.veicleControllAdvance.length-1
          $scope.indexSelectedVeicleAdvance += 1
      when keyMap.ENTER
        add = $scope.veicleControllAdvance[$scope.indexSelectedVeicleAdvance]
        $scope.selectVeicleAdvance(add)
      else
        $scope.indexSelectedVeicleAdvance = -1

  $scope.selectVeicleAdvance = (add) -> #o.K.
    $scope.searchVeicleAdvance = add.name
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.carTag")
    $timeout ->
      $scope.searchVeicleClickAdvance = false

  $scope.hoverAddVeicleAdvance = (veicle) ->
    veicle.hover = true
    $scope.indexSelectedVeicleAdvance = -1

  $scope.navegationSearch = (people, keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedNomeAdvance > 0
          $scope.indexSelectedNomeAdvance -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedNomeAdvance < $scope.filterSizeAdvance.length-1
          $scope.indexSelectedNomeAdvance += 1
      when keyMap.ENTER
        people = $scope.filterSizeAdvance[$scope.indexSelectedNomeAdvance]
        $scope.selectPeopleAdvenc(people)
      else
        $scope.indexSelectedNomeAdvance = -1

  $scope.selectPeopleAdvenc = (add) -> #O.K.
    $scope.searchPeopleAdvance = add.name
    $scope.focarElemento $scope.caixaFiltroAdvance, 'caixaFiltroAdvance.carInput'
    $timeout ->
      $scope.motivoVisitaTempAdvance.showVisita = false

  $scope.hoverVisitAdvance = (people) ->
    people.hover = true
    $scope.indexSelectedNomeAdvance = -1

  $scope.placaEditarNovo = (key) ->
    temp = $scope.editarCadastroTemp.PlacaEdit
    $timeout ->
      size =  $scope.editarCadastroTemp.PlacaEdit.length-1
    if (((48 <= key <= 57) || (96 <= key <= 105)) && bool1) || ((65 <= key <= 90) && !bool1)
      bool1 = !bool1
      $timeout ->
        temp += '-' + $scope.editarCadastroTemp.PlacaEdit[size] if $scope.editarCadastroTemp.PlacaEdit[size] != '-'
        $scope.editarCadastroTemp.PlacaEdit = temp
    else
      if key == 8 && $scope.editarCadastroTemp.PlacaEdit[size] == '-'
        bool1 = true
      else
        if size == 0
          bool1 = true

  $scope.placaEditarSaida = (key) ->
    temp = $scope.peopleSaidasEditar.PlacaEdit
    $timeout ->
      size =  $scope.peopleSaidasEditar.PlacaEdit.length-1
    if (((48 <= key <= 57) || (96 <= key <= 105)) && bool4) || ((65 <= key <= 90) && !bool4)
      bool4 = !bool4
      $timeout ->
        temp += '-' + $scope.peopleSaidasEditar.PlacaEdit[size] if $scope.peopleSaidasEditar.PlacaEdit[size] != '-'
        $scope.peopleSaidasEditar.PlacaEdit = temp
    else
      if key == 8 && $scope.peopleSaidasEditar.PlacaEdit[size] == '-'
        bool4 = true
      else
        if size == 0
          bool4 = true

  $scope.placaEditarEntrada = (key) ->
    temp = $scope.peopleEntradasEditar.PlacaEdit
    $timeout ->
      size =  $scope.peopleEntradasEditar.PlacaEdit.length-1
    if (((48 <= key <= 57) || (96 <= key <= 105)) && bool3) || ((65 <= key <= 90) && !bool3)
      bool3 = !bool3
      $timeout ->
        temp += '-' + $scope.peopleEntradasEditar.PlacaEdit[size] if $scope.peopleEntradasEditar.PlacaEdit[size] != '-'
        $scope.peopleEntradasEditar.PlacaEdit = temp
    else
      if key == 8 && $scope.peopleEntradasEditar.PlacaEdit[size] == '-'
        bool3 = true
      else
        if size == 0
          bool3 = true

  $scope.placaCadastroCar = (key) ->
    temp = $scope.cadastroVeiculoNew.veiculoPlaca
    $timeout ->
      size =  $scope.cadastroVeiculoNew.veiculoPlaca.length-1
    if (((48 <= key <= 57) || (96 <= key <= 105)) && bool2) || ((65 <= key <= 90) && !bool2)
      bool2 = !bool2
      $timeout ->
        temp += '-' + $scope.cadastroVeiculoNew.veiculoPlaca[size] if $scope.cadastroVeiculoNew.veiculoPlaca[size] != '-'
        $scope.cadastroVeiculoNew.veiculoPlaca = temp
    else
      if key == 8 && $scope.cadastroVeiculoNew.veiculoPlaca[size] == '-'
        bool2 = true
      else
        if size == 0
          bool2 = true

  $scope.placaCadastroCarCadastro = (key) ->
    temp = $scope.cadastroVeiculo.carPlaca
    $timeout ->
      size =  $scope.cadastroVeiculo.carPlaca.length-1
    if (((48 <= key <= 57) || (96 <= key <= 105)) && bool5) || ((65 <= key <= 90) && !bool5)
      bool5 = !bool5
      $timeout ->
        temp += '-' + $scope.cadastroVeiculo.carPlaca[size] if $scope.cadastroVeiculo.carPlaca[size] != '-'
        $scope.cadastroVeiculo.carPlaca = temp
    else
      if key == 8 && $scope.cadastroVeiculo.carPlaca[size] == '-'
        bool5 = true
      else
        if size == 0
          bool5 = true

  $scope.hoverAddVisit = (people) ->
    people.hover = true
    $scope.indexSelected = -1

  $scope.hoverAddVeicle = (veicle) ->
    veicle.hover = true
    $scope.indexSelectedVeicleNew = -1

  $scope.hoverAddDestino = (destino) ->
    destino.hover = true
    $scope.indexSelectedDestino = -1

  $scope.dateSystem = ->
    moment(new Date()).format('YYYY-DD-MM')

  $scope.hourSystem = ->
    new Date()

  $scope.actionAcc = (alt) ->
    alt.openEntrada = !alt.openEntrada
    if alt.openEntrada
      $scope.backgroundColorAccEntradas = 'background-color: #337ab7;'
      $scope.borderStyleEntradas = 'border-right: 1px solid #438ccb;'
    else
      $scope.backgroundColorAccEntradas = 'background-color: #f3f3f4;'
      $scope.borderStyleEntradas = 'border-right: 1px solid #e0e2e3;'

  $scope.actionAccPendentes = (alt) ->
    alt.pendentes = !alt.pendentes
    if alt.pendentes
      $scope.borderStylePendentes = 'border-right: 1px solid #438ccb;'
      $scope.backgroundColorAccPendentes = 'background-color: #337ab7;'
    else
      $scope.borderStylePendentes = 'border-right: 1px solid #e0e2e3;'
      $scope.backgroundColorAccPendentes = 'background-color: #f3f3f4;'

  $scope.actionAccSaidas = (alt) ->
    alt.openedExit = !alt.openedExit
    if alt.openedExit
      $scope.borderStyleSaidas = 'border-right: 1px solid #438ccb;'
      $scope.backgroundColorAccSaidas = 'background-color: #337ab7;'
    else
      $scope.borderStyleSaidas = 'border-right: 1px solid #e0e2e3;'
      $scope.backgroundColorAccSaidas = 'background-color: #f3f3f4;'

  $scope.clickRelatorio = (i) ->
    if i == 1
      if $scope.color1 == 'sc-btn-blue'
        $scope.color1 = 'sc-btn-gray-light'
      else
        $scope.color1 = 'sc-btn-blue'
    else if i == 2
      if $scope.color2 == 'sc-btn-blue'
        $scope.color2 = 'sc-btn-gray-light'
      else
        $scope.color2 = 'sc-btn-blue'
    else
      if $scope.color3 == 'sc-btn-blue'
        $scope.color3 = 'sc-btn-gray-light'
      else
        $scope.color3 = 'sc-btn-blue'

  $scope.registerExitGrup = ->
    if confirm "Registrar Saida?"
      sizeDestino = 0
      if $scope.tempDestino
        for i in $scope.entradas
          if i.destino == $scope.tempDestino && i.sairOp
            sizeDestino++

      for lista in $scope.entradas
        if lista &&  lista.sairOp
          if lista.destino == $scope.tempDestino
            sizeDestino--
          if lista.car
            $scope.tempDestino = ''
            $scope.tempVeicle = ''
            $scope.tempHour = ''
            $scope.tempDate = ''
            $scope.tempTag = ''
          if sizeDestino == 0 && $scope.tempVeicle != '' && (lista.destino == $scope.tempDestino)
            select2 = undefined
            lista.id = undefined
            lista.car = true
            lista.tag = $scope.tempTag
            lista.data = $scope.tempDate
            lista.hora = $scope.tempHour
            lista.carId = $scope.tempVeicle
            lista.sairOp = undefined
            lista.destino = $scope.tempDestino
            lista.pendente = true
            lista.prestador = undefined
            lista.autorizacao = undefined
            lista.motivoVisita = 'Veículos'
            lista.observacaoMotivo = undefined
            $scope.tempVeicle = ''
          else
            $scope.saidas.push
              id: lista.id
              destino: lista.destino
              data: lista.data
              dataExit: $scope.dateSystem()
              hora: lista.hora
              horaExit: $scope.hourSystem()
              motivoVisita: lista.motivoVisita
              observacaoMotivo: lista.observacaoMotivo
              autorizacao: lista.autorizacao

            if lista.car
              addCar = $scope.saidas[$scope.saidas.length-1]
              addCar.tag = lista.tag
              addCar.car = lista.car
              addCar.carId = lista.carId
            $scope.pessoas[lista.id].saiu = true
        i = $scope.entradas.length-1
      while i >= 0
        lista = $scope.entradas[i]
        if lista.sairOp
          $scope.entradas.splice($scope.entradas.indexOf(lista), 1)
        i--

  $scope.saidaCond = (grup) ->
    grup[0].select = !grup[0].select
    for i in grup
      i.select2 = grup[0].select
      i.sairOp = grup[0].select

  $scope.saidaCondPendentes = (grup) ->
    grup[0].select = !grup[0].select
    for i in grup
      if i.motivoVisita != 'Veículos'
        i.select2 = grup[0].select
        i.sairOp = grup[0].select

  $scope.saidaGrupo = (grup) ->
    grup[0].select2 = !grup[0].select2
    for i in grup
      i.sairOp = grup[0].select2

  $scope.saidaGrupoPendentes = (grup) ->
    grup[0].select2 = !grup[0].select2
    for i in grup
      i.sairOp = grup[0].select2 if i.motivoVisita != 'Veículos'

  $scope.registerExit = (lista, index, size) -> #O.K.
    if confirm "Registrar Saida?"
      for i in $scope.entradas
        if i.id == lista.id
          remove = $scope.entradas.indexOf(i)
      $scope.saidas.push
        id: lista.id
        destino: lista.destino
        data: lista.data
        dataExit: $scope.dateSystem()
        hora: lista.hora
        prestador: lista.prestador
        horaExit: $scope.hourSystem()
        motivoVisita: lista.motivoVisita
        observacaoMotivo: lista.observacaoMotivo
        autorizacao: lista.autorizacao

      if lista.car
        addCar = $scope.saidas[$scope.saidas.length-1]
        addCar.tag = lista.tag
        addCar.car = lista.car
        addCar.carId = lista.carId

      $scope.pessoas[lista.id].saiu = true
      $scope.entradas.splice(remove, 1)

  $scope.navegation = (people, keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelected > 0
          $scope.indexSelected -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelected < $scope.filterSize.length-1
          $scope.indexSelected += 1
      when keyMap.ENTER#, keyMap.TAB
        if $scope.filterSize.length == 0 && funcao != 4
          funcao = 0
        if funcao == 0 #Cadastro
          $scope.cadastrarVisitante()
        else if funcao == 1 #Adicionar Visita (buffer)
          $scope.addVisit()
        else if funcao == 2 #Selecionar opção (Nomes)
          people = $scope.filterSize[$scope.indexSelected]
          $scope.selectPeople(people)
        else if funcao == 3 #concluir cadastro visita
          $scope.addCadastroVisitante()
        else if funcao == 4
          $scope.cadastroEditVisit = !$scope.cadastroEditVisit
      else
        $scope.indexSelected = -1

  $scope.navegationDestinoCadastro = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedCadastroDes > 0
          $scope.indexSelectedCadastroDes -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedCadastroDes < $scope.destinoCadastroControll.length-1
          $scope.indexSelectedCadastroDes += 1
      when keyMap.ENTER
        if funcao == 0 #select
          add = $scope.destinoCadastroControll[$scope.indexSelectedCadastroDes]
          $scope.selectDestinoCadastro()
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitante, directFoco)
      else
        $scope.indexSelectedCadastroDes = -1

  $scope.selectDestinoCadastro = (add) ->
    $scope.cadastroMotivoVisitaTemp.destino = add.name
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.autorizacao")
    $timeout ->
      $scope.detalhesVisita.showDestino = false

  $scope.selectMotivoCadastro = (add)->
    $scope.cadastroMotivoVisitaTemp.motivoDaVisita = add.name
    if add.name == $scope.detalhesVisita[1].name
      $scope.addMotivoVisita = true
      $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.servico")
    else
      $scope.addMotivoVisita = false
      $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.Observacao")
    $timeout ->
      $scope.detalhesVisita.showMotivo = false
      $scope.indexSelectedCadastro = -1

  $scope.navegationMotivo = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedMotivo > 0
          $scope.indexSelectedMotivo -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedMotivo < $scope.detalhesSize.length-1
          $scope.indexSelectedMotivo += 1
      when keyMap.ENTER
        if funcao == 0 #select
          add = $scope.detalhesSize[$scope.indexSelectedMotivo]
          $scope.selectMotivoVisit(add)
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitante, directFoco)
      else
        $scope.indexSelectedMotivo = -1

  $scope.navegationDestino = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedDestino > 0
          $scope.indexSelectedDestino -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedDestino < $scope.destinoSize.length-1
          $scope.indexSelectedDestino += 1
      when keyMap.ENTER
        if funcao == 0 #select
          add = $scope.destinoSize[$scope.indexSelectedDestino]
          $scope.selectMotivoDestino(add)
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitante, directFoco)
      else
        $scope.indexSelectedDestino = -1

  $scope.selectMotivoDestino = (add) ->
    $scope.addBloco = add.id
    $scope.motivoVisitaTemp.destino = add.name
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.autorizacao")
    $timeout ->
      $scope.searchDestinoClick = false

  $scope.selectMotivoVisit = (add) ->
    $scope.motivoVisitaTemp.motivoDaVisita = add.name
    if add.name == $scope.detalhesVisita[1].name
      $scope.addMotivoVisita = true
      $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.servico")
    else
      $scope.addMotivoVisita = false
      $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.Observacao")
    $timeout ->
      $scope.searchMotivoClick = false

  $scope.focarElemento = (obj, directFoco) ->
    $scTarget.focus(obj, directFoco, false)

  $scope.cadastrarVeiculo = ->
    $scope.veiculoModelo = $scope.searchVeicle
    $scope.cadastroVeiculoModal.open()
    $scope.focarElemento($scope.caixaNovoVeiculo, "caixaNovoVeiculo.modelo")

  testeNovaVisita = ->
    if $scope.searchPeople == ''
      alert 'Erro: \n    Adicionar Visitante'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.addVisita')
      return false
    if $scope.carSelect
      if $scope.searchVeicle == ''
        alert 'Erro: \n    Adicionar Vículo'
        $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.carInput')
        return false
      else
        if $scope.tagVeicle == '' && $scope.aplicarConfiguracao.tag
          alert 'Erro: \n    Tag'
          $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.carTag')
          return false
    if $scope.addMotivoVisita
      if !$scope.motivoVisitaTemp.prestador
        alert 'Erro: \n    Prestador de Serviço'
        $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.servico')
        return false
    if !$scope.motivoVisitaTemp.motivoDaVisita
      alert 'Erro: \n    Motivo da visita'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.obs')
      return false
    if !$scope.motivoVisitaTemp.observacaoMotivo
      alert 'Erro: \n    Observações (Motivo da visita)'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.Observacao')
      return false
    if !$scope.motivoVisitaTemp.destino
      alert 'Erro: \n    Destino/Residência'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.destino')
      return false
    if !$scope.motivoVisitaTemp.autorizacao
      alert 'Erro: \n    Autorização'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.autorizacao')
      return false
    return true

  $scope.addVisit = ->
    if testeNovaVisita()
      $scope.pessoas[$scope.addId].saiu = false

      $scope.buffer.push
        id: $scope.addId
        destino: $scope.motivoVisitaTemp.destino
        data: $scope.dateSystem()
        hora: $scope.hourSystem()
        motivoVisita: $scope.motivoVisitaTemp.motivoDaVisita
        observacaoMotivo: $scope.motivoVisitaTemp.observacaoMotivo
        autorizacao: $scope.motivoVisitaTemp.autorizacao

      for i in $scope.buffer
        if $scope.addId == i.id
          bufferId = $scope.buffer.indexOf(i)
      add = $scope.buffer[bufferId]

      add.tag = $scope.tagVeicle if $scope.tagVeicle

      if $scope.addMotivoVisita
        add.prestador = $scope.motivoVisitaTemp.prestador

      if $scope.carSelect == true
        add.car = true
        add.carId = $scope.veicleId
        $scope.veiculos[add.carId].saiu = false
      $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.salvarRegistro")
      reset()

  $scope.addVisitanteButton = ->
    reset()
    $scope.AddVisitanteShow = !$scope.AddVisitanteShow
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.addVisita")

  $scope.navegationVeiculo = (key, funcao, direct) -> #O.K.
    switch key
      when keyMap.ARROW.UP
        if $scope.indexSelectedVeicleNew > 0
          $scope.indexSelectedVeicleNew -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedVeicleNew < $scope.veicleControllPrin.length-1
          $scope.indexSelectedVeicleNew += 1
      when keyMap.ENTER
        if $scope.veicleControllPrin.length == 0 && funcao != 0
          funcao = 1
        if funcao == 0
          $scope.focarElemento($scope.caixaNovoVeiculo, direct)
        else if funcao == 1
          $scope.cadastrarVeiculoVisitaNew()
        else if funcao == 2 # Select Veicle
          add = $scope.veicleControllPrin[$scope.indexSelectedVeicleNew]
          $scope.selectVeiclenew(add)
      else
        $scope.indexSelectedVeicleNew = -1

  $scope.selectMotivoVisitEdit = (list) ->
    $scope.editarCadastroTemp.motivoVisita = list.name
    if list.name == $scope.detalhesVisita[1].name
      $scope.addMotivoVisitaEdit = true
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.servicoEdit")
    else
      $scope.addMotivoVisitaEdit = false
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.ObservacaoEdit")
    $timeout ->
      $scope.editCadastroTemp.showMotivo = false

  $scope.navegationDestinoEdit = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedDestinoEdit > 0
          $scope.indexSelectedDestinoEdit -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedDestinoEdit < $scope.controllDestinoEdit.length-1
          $scope.indexSelectedDestinoEdit += 1
      when keyMap.ENTER
        if funcao == 0 #select
          list = $scope.controllDestinoEdit[$scope.indexSelectedDestinoEdit]
          $scope.selectDestinoVisitEdit(list)
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, directFoco)
      else
        $scope.indexSelectedDestinoEdit = -1

  $scope.selectDestinoVisitEdit = (list) ->
    $scope.editarCadastroTemp.destino = list.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.autorizacaoEdit")
    $timeout ->
      $scope.editCadastroTemp.showDestino = false

  $scope.cadastrarVeiculoVisitaEdit = ->
    $scope.carCC = true
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.modeloVisitVeicleEdit")

  $scope.navegationVeiculoCadastro = (key, funcao, direct) -> #O.K.
    switch key
      when keyMap.ARROW.UP
        if $scope.cadastroVeiculo.index > 0
          $scope.cadastroVeiculo.index -= 1
      when keyMap.ARROW.DOWN
        if $scope.cadastroVeiculo.index < $scope.veicleControllList.length-1
          $scope.cadastroVeiculo.index += 1
      when keyMap.ENTER
        if $scope.veicleControllList.length == 0
          funcao = 1
        if funcao == 0
          $scope.focarElemento($scope.caixaNovoVeiculo, direct)
        else if funcao == 1
          $scope.cadastrarVeiculoVisita()
        else if funcao == 2 # Select Veicle
          add = $scope.veicleControllList[$scope.cadastroVeiculo.index]
          $scope.selectVeicle(add)
      else
        $scope.cadastroVeiculo.index = -1

  $scope.selectVeiclenew = (add) -> #o.K.
    $scope.searchVeicle = add.name
    $scope.veicleId = add.id
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.carTag")
    $timeout ->
      $scope.searchVeicleClick = false

  $scope.selectVeicle = (add) ->
    $scope.cadastroVeiculo.car = add.name
    $scope.veicleId = add.id
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.carInputTag")
    $timeout ->
      $scope.cadastroVeiculo.carShow = false

  $scope.cadastroVeiculoSalvar = ->
    $scope.veiculos.push
      id: $scope.veiculos.length
      name: $scope.veiculoModelo + ' (' + $scope.veiculoPlaca1 + '-' + $scope.veiculoPlaca2 + ')'
      cor: $scope.veiculoCor
      photo: 'http://upload.wikimedia.org/wikipedia/commons/9/99/Audi_A8_2013_(11209850785).jpg'
      saiu: true

    $scope.veiculoModelo = $scope.veiculoCor = ''
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.obs")

  $scope.cadastroVeiculoSalvarNew = ->
    $scope.veiculos.push
      id: $scope.veiculos.length
      name: $scope.cadastroVeiculoNew.veiculoModelo
      placa: $scope.cadastroVeiculoNew.veiculoPlaca
      cor: $scope.cadastroVeiculoNew.veiculoCor
      saiu: true

    $scope.searchVeicle = $scope.cadastroVeiculoNew.veiculoModelo
    $scope.tagVeicle = $scope.cadastroVeiculoNew.tag
    $scope.veicleId = $scope.veiculos.length-1
    $scope.cadastroVeiculoNew.veiculoModelo = $scope.cadastroVeiculoNew.tag = $scope.cadastroVeiculoNew.veiculoCor = ''
    $scope.cadastroVeiculoModal.close()
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.obs")

  reset = ->
    $scope.tagVeicle = ''
    $scope.addId = ''
    $scope.addBloco = ''
    $scope.carSelect = false
    $scope.searchPeople = ''
    $scope.searchVeicle = ''
    $scope.addMotivoVisita = null
    $scope.motivoVisitaTemp = {}

  buscaId = (alvo) -> #O.K.
    for i in $scope.saidas
      if i.id == alvo
        return $scope.saidas.indexOf(i)
    return null

  $scope.selectPeople = (add) -> #O.K.
    buscaRegistro = buscaId(add.id)
    if buscaRegistro != null
      addRegistro = $scope.saidas[buscaRegistro]
      $scope.searchPeople = add.name
      $scope.addId = add.id
      $scope.motivoVisitaTemp.destino = addRegistro.destino
      $scope.motivoVisitaTemp.autorizacao = addRegistro.autorizacao
      $scope.motivoVisitaTemp.motivoDaVisita = addRegistro.motivoVisita
      $scope.motivoVisitaTemp.observacaoMotivo = addRegistro.observacaoMotivo
      if $scope.aplicarConfiguracao.tag
        $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.carTag")
      else
        $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.carTag")
      if addRegistro.car && $scope.veiculos[addRegistro.carId].saiu
        $scope.veicleId = addRegistro.carId
        $scope.carSelect = true
        $scope.searchVeicle = $scope.veiculos[addRegistro.carId].name
        $scope.veiculos[addRegistro.carId].add = false
    else
      $scope.addId = add.id
      $scope.searchPeople = add.name
      $scope.searchMotivoClick = true
      $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.carTag")
    $timeout ->
      $scope.motivoVisitaTemp.showVisita = false

  $scope.selectCar = ->
    $scope.carSelect = !$scope.carSelect

  $scope.navegationVeiculoEdit = (key, funcao, direct) -> #O.K.
    switch key
      when keyMap.ARROW.UP
        if $scope.indexSelectedVeicleEdit > 0
          $scope.indexSelectedVeicleEdit -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedVeicleEdit < $scope.veicleFilterEdit.length-1
          $scope.indexSelectedVeicleEdit += 1
      when keyMap.ENTER
        if $scope.veicleFilterEdit.length == 0
          funcao = 1
        if funcao == 0
          $scope.focarElemento($scope.caixaNovoVeiculo, direct)
        else if funcao == 1
          $scope.cadastrarVeiculoVisitaEdit()
        else if funcao == 2 # Select Veicle
          add = $scope.veicleFilterEdit[$scope.indexSelectedVeicleEdit]
          $scope.selectVeiclenEdit(add)
      else
        $scope.indexSelectedVeicleEdit = -1

  $scope.selectVeiclenEdit = (add) -> #o.K.
    for i in $scope.veiculos
      if i.id == $scope.veicleIdEdit
        i.saiu = true

    $scope.editarCadastroTemp.veicle = add.name
    $scope.veicleIdEdit = add.id
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.tagEdit")
    $timeout ->
      $scope.showVeicle = false

  $scope.newVisit = ->
    if confirm 'Deseja realmente registrar a entrada ?'
      for add in $scope.buffer
        $scope.entradas.push
          id: add.id
          destino: add.destino
          data: add.data
          hora: add.hora
          prestador: add.prestador
          motivoVisita: add.motivoVisita
          observacaoMotivo: add.observacaoMotivo
          autorizacao: add.autorizacao
          tag: add.tag
          car: add.car
          carId: add.carId
          pendente: false
      $scope.buffer = []
      $scope.AddVisitanteShow = false

  $scope.cadastrarVeiculoVisitaNew = ->
    $scope.cadastroVeiculoModal.open()
    $scope.searchVeicleClick = false
    $scope.cadastroVeiculoNew.veiculoModelo = $scope.searchVeicle
    $scope.focarElemento($scope.caixaNovoVeiculo, "caixaNovoVeiculo.modelo")

  $scope.cadastrarVeiculoVisita = ->
    $scope.carCC = true
    $scope.focarElemento($scope.caixaNovoVisitante, "caixaNovoVisitante.modeloVisitVeicle")

  $scope.cancellVisit = ->
    $scope.buffer.length = 0
    $scope.AddVisitanteShow = null

  $scope.removeVeiculo = -> #O.K.
    $scope.editarCadastroTemp.tag = ''
    $scope.cadastroVeiculo.select = false
    $scope.editarCadastroTemp.veicle = ''

  $scope.navegationMotivoEditSaidas = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedMotivoEditSaidas > 0
          $scope.indexSelectedMotivoEditSaidas -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedMotivoEditSaidas < $scope.controllMotivoEditSaidas.length-1
          $scope.indexSelectedMotivoEditSaidas += 1
      when keyMap.ENTER
        if funcao == 0 #select
          list = $scope.controllMotivoEditSaidas[$scope.indexSelectedMotivoEditSaidas]
          $scope.selectMotivoVisitEditSaidas(list)
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, directFoco)
      else
        $scope.indexSelectedMotivoEditSaidas = -1

  $scope.selectMotivoVisitEditSaidas = (list) ->
    $scope.editarCadastroSaidasTemp.motivoVisita = list.name
    if list.name == $scope.detalhesVisita[1].name
      $scope.addMotivoVisita = true
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.servicoEdit")
    else
      $scope.addMotivoVisita = false
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.ObservacaoEdit")
    $timeout ->
      $scope.peopleSaidasEdit.showMotivo = false

  $scope.selectMotivoVisitEditEntradas = (list) ->
    $scope.editarCadastroEntradasTemp.motivoVisita = list.name
    if list.name == $scope.detalhesVisita[1].name
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.servicoEdit")
      $scope.addMotivoVisita = true
    else
      $scope.addMotivoVisita = false
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.ObservacaoEdit")
    $timeout ->
      $scope.peopleEntradasEdit.showMotivo = false

  $scope.selectMotivoVisitEditPendentes = (list) ->
    $scope.editarCadastroPendentesTemp.motivoVisita = list.name
    if list.name == $scope.detalhesVisita[1].name
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.servicoEdit")
      $scope.addMotivoVisita = true
    else
      $scope.addMotivoVisita = false
      $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.ObservacaoEdit")
    $timeout ->
      $scope.peopleEntradasEdit.showMotivo = false

  $scope.navegationVeiculoEditEntradas = (key, funcao, direct) -> #O.K.
    switch key
      when keyMap.ARROW.UP
        if $scope.indexSelectedVeicleEditEntradas > 0
          $scope.indexSelectedVeicleEditEntradas -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedVeicleEditEntradas < $scope.veicleControllEntradas.length-1
          $scope.indexSelectedVeicleEditEntradas += 1
      when keyMap.ENTER
        if $scope.veicleControllEntradas.length == 0
          funcao = 1
        if funcao == 0
          $scope.focarElemento($scope.caixaNovoVeiculo, direct)
        else if funcao == 1
          $scope.cadastrarVeiculoVisitaEditEntradas()
        else if funcao == 2 # Select Veicle
          add = $scope.veicleControllEntradas[$scope.indexSelectedVeicleEditEntradas]
          $scope.selectVeiclenEditEntradas(add)
      else
        $scope.indexSelectedVeicleEditEntradas = -1

  $scope.navegationVeiculoEditSaidas = (key, funcao, direct) -> #O.K.
    switch key
      when keyMap.ARROW.UP
        if $scope.indexSelectedVeicleEditSaidas > 0
          $scope.indexSelectedVeicleEditSaidas -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedVeicleEditSaidas < $scope.veicleControllSaidas.length-1
          $scope.indexSelectedVeicleEditSaidas += 1
      when keyMap.ENTER
        if $scope.veicleControllSaidas.length == 0
          funcao = 1
        if funcao == 0
          $scope.focarElemento($scope.caixaNovoVeiculo, direct)
        else if funcao == 1
          $scope.cadastrarVeiculoVisitaEditEntradas()
        else if funcao == 2 # Select Veicle
          add = $scope.veicleControllEntradas[$scope.indexSelectedVeicleEditSaidas]
          $scope.selectVeiclenEditSaidas(add)
      else
        $scope.indexSelectedVeicleEditSaidas = -1

  $scope.selectVeiclenEditSaidas = (add) -> #o.K.
    for i in $scope.veiculos
      if i.id == $scope.editarCadastroSaidasTemp.carId
        i.saiu = true

    $scope.indexSaiuCar = add.id
    $scope.peopleSaidasEditar.veicle = add.name
    $scope.editarCadastroSaidasTemp.carId = add.id
    $scope.editarCadastroSaidasTemp.tag = ''
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.tagEdit")
    $timeout ->
      $scope.peopleSaidasEdit.showVeicle = false

  $scope.cadastrarVeiculoVisitaEditEntradas = ->
    $scope.carCC = true
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.modeloVisitVeicleEdit")

  $scope.voltardePendentes = (lista) ->
    if confirm 'Deseja realmente voltar a visita para entradas ?'
      lista.pendente = false

  $scope.removeRegisterEntradas = (lista, size) ->
    if confirm 'Deseja remover a entrada?'
      if lista.car
        $scope.tempDestino = ''
        $scope.tempVeicle = ''
        $scope.tempHour = ''
        $scope.tempDate = ''
        $scope.tempTag = ''
      if size == 1 && $scope.tempVeicle != '' && (lista.destino == $scope.tempDestino)
        lista.id = ''
        lista.car = true
        lista.tag = $scope.tempTag
        lista.data = $scope.tempDate
        lista.hora = $scope.tempHour
        lista.carId = $scope.tempVeicle
        lista.destino = $scope.tempDestino
        lista.pendente = true
        lista.autorizacao = ''
        lista.motivoVisita = 'Veículos'
        lista.observacaoMotivo = ''
        $scope.tempVeicle = ''
      else
        $scope.pessoas[lista.id].saiu = true
        $scope.entradas.splice($scope.entradas.indexOf(lista), 1)

  $scope.removeRegisterSaidas = (lista) ->
    a = confirm 'Deseja remover a entrada?'
    if a
      $scope.pessoas[lista.id].saiu = true
      $scope.saidas.splice($scope.saidas.indexOf(lista), 1)

  $scope.removeRegisterAddVisit = (lista) ->
    a = confirm 'Deseja remover a entrada?'
    if a
      $scope.pessoas[lista.id].saiu = true
      $scope.buffer.splice($scope.buffer.indexOf(lista), 1)

  $scope.voltardeSaidas = (lista) ->
    if confirm 'Deseja realmente voltar o visitante para entradas? '
      $scope.entradas.push
        id: lista.id
        destino: lista.destino
        data: lista.data
        hora: lista.hora
        motivoVisita: lista.motivoVisita
        observacaoMotivo: lista.observacaoMotivo
        autorizacao: lista.autorizacao
        car: lista.car
        pendente: false
      if lista.car
        $scope.entradas[$scope.entradas.length-1].carId = lista.carId
      $scope.saidas.splice($scope.saidas.indexOf(lista), 1)

  $scope.selectVeiclenEditEntradas = (add) -> #o.K.
    for i in $scope.entradas
      if i.motivoVisita == 'Veículos' && i.carId == add.id
        $scope.savaRemoveEntradaVeiculo = $scope.entradas.indexOf(i)
    for i in $scope.veiculos
      if i.id == $scope.editarCadastroEntradasTemp.carId
        i.saiu = true
    $scope.indexSaiuCar = add.id
    $scope.peopleEntradasEditar.veicle = add.name
    $scope.editarCadastroEntradasTemp.carId = add.id
    $scope.editarCadastroEntradasTemp.tag = ''
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.tagEdit")
    $timeout ->
      $scope.peopleEntradasEdit.showVeicle = false

  $scope.selectPeopleEdit = (add) -> #O.K.
    $scope.addIdEdit = add.id
    $scope.peopleEditCadastro.name = add.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.obs')
    $timeout ->
      $scope.peopleEditCadastro.showNomeEdit = false

  $scope.navegationEdit = (keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedEdit > 0
          $scope.indexSelectedEdit -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedEdit < $scope.pessoasEditFilter.length-1
          $scope.indexSelectedEdit += 1
      when keyMap.ENTER#, keyMap.TAB
        if $scope.pessoasEditFilter.length == 0
          funcao = 0
        if funcao == 0 #Cadastro
          $scope.cadastroEditVisitEdit = !$scope.cadastroEditVisitEdit
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nomeAddEdit')
        if funcao == 1 #Selecionar opção (Nomes)
          people = $scope.pessoasEditFilter[$scope.indexSelectedEdit]
          $scope.selectPeopleEdit(people)
      else
        $scope.indexSelectedEdit = -1

  $scope.navegationEditEntradas = (keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedEnterEdit > 0
          $scope.indexSelectedEnterEdit -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedEnterEdit < $scope.pessoasEntradasEditFilter.length-1
          $scope.indexSelectedEnterEdit += 1
      when keyMap.ENTER#, keyMap.TAB
        if $scope.pessoasEntradasEditFilter.length == 0
          funcao = 0
        if funcao == 0 #Cadastro
          $scope.cadastroEditVisitEdit = !$scope.cadastroEditVisitEdit
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nomeAddEdit')
        if funcao == 1 #Selecionar opção (Nomes)
          people = $scope.pessoasEntradasEditFilter[$scope.indexSelectedEnterEdit]
          $scope.selectPeopleEntradasEdit(people)
      else
        $scope.indexSelectedEnterEdit = -1

  $scope.navegationEditPendentes = (keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedpendentesEdit > 0
          $scope.indexSelectedpendentesEdit -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedpendentesEdit < $scope.pessoasPendentesEditFilter.length-1
          $scope.indexSelectedpendentesEdit += 1
      when keyMap.ENTER#, keyMap.TAB
        if $scope.pessoasPendentesEditFilter.length == 0
          funcao = 0
        if funcao == 0 #Cadastro
          $scope.cadastroEditVisitEdit = !$scope.cadastroEditVisitEdit
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nomeAddEdit')
        if funcao == 1 #Selecionar opção (Nomes)
          people = $scope.pessoasPendentesEditFilter[$scope.indexSelectedpendentesEdit]
          $scope.selectPeoplePendentes(people)
      else
        $scope.indexSelectedpendentesEdit = -1

  $scope.selectPeoplePendentes = (add) -> #O.K.
    $scope.indexSaiuPeople = $scope.editarCadastroPendentesTemp.id
    $scope.editarCadastroPendentesTemp.id = add.id
    $scope.peoplePendentesEditar.name = add.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.carSelectTarget')
    $timeout ->
      $scope.peoplePendentesEdit.showNomeEdit = false

  $scope.hoverPendenteNome = (people) ->
    people.hover = true
    $scope.indexSelectedpendentesEdit

  $scope.navegationEditSaidas = (keyCode, funcao) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedExitEdit > 0
          $scope.indexSelectedExitEdit -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedExitEdit < $scope.pessoasSaidasEditFilter.length-1
          $scope.indexSelectedExitEdit += 1
      when keyMap.ENTER#, keyMap.TAB
        if $scope.pessoasSaidasEditFilter.length == 0
          funcao = 0
        if funcao == 0 #Cadastro
          $scope.cadastroEditVisitEdit = !$scope.cadastroEditVisitEdit
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nomeAddEdit')
        if funcao == 1 #Selecionar opção (Nomes)
          people = $scope.pessoasSaidasEditFilter[$scope.indexSelectedExitEdit]
          $scope.selectPeopleSaidasEdit(people)
      else
        $scope.indexSelectedExitEdit = -1

  $scope.selectPeopleSaidasEdit = (add) -> #O.K.
    $scope.indexSaiuPeople = $scope.editarCadastroSaidasTemp.id
    $scope.editarCadastroSaidasTemp.id = add.id
    $scope.peopleSaidasEditar.name = add.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.carSelectTarget')
    $timeout ->
      $scope.peopleSaidasEdit.showNomeEdit = false

  $scope.navegationDestinoEditSaidas = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedDestinoEditSaidas > 0
          $scope.indexSelectedDestinoEditSaidas -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedDestinoEditSaidas < $scope.controllDestinoEditSaidas.length-1
          $scope.indexSelectedDestinoEditSaidas += 1
      when keyMap.ENTER
        if funcao == 0 #select
          list = $scope.controllDestinoEditSaidas[$scope.indexSelectedDestinoEditSaidas]
          $scope.selectDestinoVisitEditSaidas(list)
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, directFoco)
      else
        $scope.indexSelectedDestinoEditSaidas = -1

  $scope.selectDestinoVisitEditSaidas = (list) ->
    $scope.editarCadastroSaidasTemp.destino = list.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.autorizacaoEdit")
    $timeout ->
      $scope.peopleSaidasEdit.showDestino = false

  $scope.navegationDestinoEditEntradas = (keyCode, funcao, directFoco) ->
    switch keyCode
      when keyMap.ARROW.UP
        if $scope.indexSelectedDestinoEditEntradas > 0
          $scope.indexSelectedDestinoEditEntradas -= 1
      when keyMap.ARROW.DOWN
        if $scope.indexSelectedDestinoEditEntradas < $scope.controllDestinoEditEntradas.length-1
          $scope.indexSelectedDestinoEditEntradas += 1
      when keyMap.ENTER
        if funcao == 0 #select
          list = $scope.controllDestinoEditEntradas[$scope.indexSelectedDestinoEditEntradas]
          $scope.selectDestinoVisitEditEntradas(list)
        if funcao == 1
          $scope.focarElemento($scope.caixaNovoVisitanteEdit, directFoco)
      else
        $scope.indexSelectedDestinoEditEntradas = -1

  $scope.selectDestinoVisitEditEntradas = (list) ->
    $scope.editarCadastroEntradasTemp.destino = list.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, "caixaNovoVisitanteEdit.autorizacaoEdit")
    $timeout ->
      $scope.peopleEntradasEdit.showDestino = false

  $scope.removeVeiculoEntradas = -> #O.K.
    $scope.tempVeicle = $scope.editarCadastroEntradasTemp.carId
    $scope.tempDestino = $scope.editarCadastroEntradasTemp.destino
    $scope.tempHour = $scope.editarCadastroEntradasTemp.hora
    $scope.tempDate = $scope.editarCadastroEntradasTemp.data
    $scope.veiculos[$scope.editarCadastroEntradasTemp.carId].saiu = true
    $scope.editarCadastroEntradasTemp.tag = ''
    $scope.editarCadastroEntradasTemp.car = false
    $scope.editarCadastroEntradasTemp.carId = false
    $scope.peopleEntradasEditar.veicle = ''

  $scope.removeVeiculoSaidas = -> #O.K.
    $scope.editarCadastroSaidasTemp.tag = ''
    $scope.editarCadastroSaidasTemp.car = false
    $scope.peopleSaidasEditar.name = ''

  $scope.editCadastroPendente = (index) ->
    $scope.addMotivoVisita = false
    $scope.saveIdPendentelist = index
    $scope.editarCadastroPendentesTemp = angular.copy($scope.entradas[index])
    $scope.editarCadastroPendentesTemp.carId = $scope.entradas[index].carId
    $scope.editarCadastroPendentesTemp.car = true
    $scope.editarCadastroPendentesTemp.motivoVisita = ''
    $scope.peoplePendentesEditar.veicle = $scope.veiculos[$scope.entradas[index].carId].name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nome')
    $scope.editPendentesModal.open()

  $scope.editCadastroEntradas = (index) -> #O.K.
    $scope.saveIdEntradaslist = index
    $scope.editarCadastroEntradasTemp = angular.copy($scope.entradas[index])
    pessoaEdit = $scope.pessoas[$scope.entradas[index].id]
    $scope.peopleEntradasEditar.name = pessoaEdit.name
    if $scope.editarCadastroEntradasTemp.motivoVisita == $scope.detalhesVisita[1].name
      $scope.addMotivoVisita = true

    if $scope.entradas[index].car
      $scope.editarCadastroEntradasTemp.carId = $scope.entradas[index].carId
      $scope.editarCadastroEntradasTemp.car = true
      $scope.peopleEntradasEditar.veicle = $scope.veiculos[$scope.entradas[index].carId].name
    else
      $scope.peopleEntradasEditar.veicle = null
      $scope.editarCadastroEntradasTemp.car = false
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nome')
    $scope.editEntradasModal.open()

  $scope.editCadastroSaidas = (index) -> #O.K.
    $scope.saveIdSaidaslist = index
    $scope.editarCadastroSaidasTemp = angular.copy($scope.saidas[index])
    pessoaEdit = $scope.pessoas[$scope.saidas[index].id]
    $scope.peopleSaidasEditar.name = pessoaEdit.name
    if $scope.editarCadastroSaidasTemp.carId
      $scope.editarCadastroSaidasTemp.carId = $scope.saidas[index].id
      $scope.editarCadastroSaidasTemp.car = true
      $scope.peopleSaidasEditar.veicle = $scope.veiculos[$scope.saidas[index].carId].name
    else
      $scope.peopleSaidasEditar.veicle = null
      $scope.editarCadastroSaidasTemp.car = false
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nome')
    $scope.editSaidasModal.open()

  $scope.salvarEditCadastroSaidas = ->
    lista = angular.copy($scope.editarCadastroSaidasTemp)
    $scope.pessoas[$scope.indexSaiuPeople].saiu = true if $scope.indexSaiuPeople
    if $scope.cadastroEditVisitEdit
      $scope.pessoas.push
        id: $scope.pessoas.length
        name: $scope.peopleSaidasEditar.name
        cpf: $scope.peopleSaidasEditar.cpf
        rg: $scope.peopleSaidasEditar.rg
        Oexpedidor: $scope.peopleSaidasEditar.OExpedidor
        photo: 'http://i.imgur.com/UfMLctB.jpg'
        saiu: false
      $scope.pessoas[$scope.pessoas.length-1].saiu = false
      lista.id = $scope.pessoas.length-1

    if lista.car
      $scope.veiculos[lista.carId].saiu = true
      if $scope.carCC
        $scope.veiculos.push
          id: $scope.veiculos.length
          name: $scope.peopleSaidasEditar.veicle + ' (' + $scope.peopleSaidasEditar.PlacaEdit + ')'
          cor: $scope.peopleSaidasEditar.cor
          saiu: false
        lista.carId = $scope.veiculos.length-1
      $scope.veiculos[lista.carId].saiu = false

    $scope.saidas[$scope.saveIdSaidaslist] = lista
    $scope.pessoas[lista.id].saiu = false
    $scope.closeEditarCadastroSaidas()

  $scope.closeEditarCadastroSaidas = ->
    $scope.editarCadastroSaidasTemp = {};
    $scope.peopleSaidasEditar = {};
    $scope.carCC = false
    $scope.cadastroEditVisitEdit = false
    $scope.editSaidasModal.close()

  $scope.salvarEditCadastroEntradas = ->
    lista = angular.copy($scope.editarCadastroEntradasTemp)
    $scope.pessoas[$scope.indexSaiuPeople].saiu = true if $scope.indexSaiuPeople
    if $scope.savaRemoveEntradaVeiculo
      $scope.entradas.splice($scope.entradas.indexOf($scope.savaRemoveEntradaVeiculo), 1)

    if $scope.tempDestino
      $scope.entradas.push
        car: true
        data: $scope.tempDate
        hora: $scope.tempHour
        carId: $scope.tempVeicle
        destino: $scope.tempDestino
        pendente: true
        motivoVisita: 'Veículos'
      $scope.tempDestino = $scope.tempDate = $scope.tempHour = $scope.tempVeicle = undefined

    if $scope.cadastroEditVisitEdit
      $scope.pessoas.push
        id: $scope.pessoas.length
        name: $scope.peopleEntradasEditar.name
        cpf: $scope.peopleEntradasEditar.cpf
        rg: $scope.peopleEntradasEditar.rg
        Oexpedidor: $scope.peopleEntradasEditar.OExpedidor
        photo: 'http://i.imgur.com/UfMLctB.jpg'
        saiu: false
      $scope.pessoas[$scope.pessoas.length-1].saiu = false
      lista.id = $scope.pessoas.length-1

    if lista.car
      $scope.veiculos[lista.carId].saiu = true
      if $scope.carCC
        $scope.veiculos.push
          id: $scope.veiculos.length
          name: $scope.peopleEntradasEditar.veicle + ' (' + $scope.peopleEntradasEditar.PlacaEdit + ')'
          cor: $scope.peopleEntradasEditar.cor
        lista.carId = $scope.veiculos.length-1
      $scope.veiculos[lista.carId].saiu = false

    $scope.entradas[$scope.saveIdEntradaslist] = lista
    $scope.pessoas[lista.id].saiu = false
    $scope.closeEditarCadastroEntradas()

  $scope.salvarEditCadastroPendentes = ->
    lista = angular.copy($scope.editarCadastroPendentesTemp)
    $scope.pessoas[$scope.indexSaiuPeople].saiu = true if $scope.indexSaiuPeople
    if $scope.cadastroEditVisitEdit
      $scope.pessoas.push
        id: $scope.pessoas.length
        name: $scope.peopleEntradasEditar.name
        cpf: $scope.peopleEntradasEditar.cpf
        rg: $scope.peopleEntradasEditar.rg
        Oexpedidor: $scope.peopleEntradasEditar.OExpedidor
        photo: 'http://i.imgur.com/UfMLctB.jpg'
        saiu: false
      lista.id = $scope.pessoas.length-1

    if lista.car
      $scope.veiculos[lista.carId].saiu = true
      if $scope.carCC
        $scope.veiculos.push
          id: $scope.veiculos.length
          name: $scope.peopleEntradasEditar.veicle + ' (' + $scope.peopleEntradasEditar.PlacaEdit + ')'
          cor: $scope.peopleEntradasEditar.cor
        lista.carId = $scope.veiculos.length-1
      $scope.veiculos[lista.carId].saiu = false

    $scope.entradas[$scope.saveIdPendentelist] = lista
    $scope.pessoas[lista.id].saiu = false
    $scope.closeEditarCadastroEntradas()

  $scope.closeEditarCadastroEntradas = ->
    $scope.editarCadastroEntradasTemp = {};
    $scope.peopleEntradasEditar = {};
    $scope.carCC = false
    $scope.cadastroEditVisitEdit = false
    $scope.editCadastroModal.close()

  $scope.selectPeopleEntradasEdit = (add) -> #O.K.
    $scope.indexSaiuPeople = $scope.editarCadastroEntradasTemp.id
    $scope.editarCadastroEntradasTemp.id = add.id
    $scope.peopleEntradasEditar.name = add.name
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.carSelectTarget')
    $timeout ->
      $scope.peopleEntradasEdit.showNomeEdit = false

  $scope.editCadastro = (index) -> #O.K.
    $scope.saveIdlist = index
    $scope.editarCadastroTemp = angular.copy($scope.buffer[index])
    pessoaEdit = $scope.pessoas[$scope.buffer[index].id]
    $scope.peopleEditCadastro.name = pessoaEdit.name

    if $scope.editarCadastroTemp.motivoVisita == $scope.detalhesVisita[1].name
      $scope.addMotivoVisitaEdit = true

    if $scope.buffer[index].car == true
      $scope.veicleIdEdit = $scope.buffer[index].id
      $scope.cadastroVeiculo.select = true
      $scope.editarCadastroTemp.veicle = $scope.veiculos[$scope.buffer[index].carId].name
    else
      $scope.editarCadastroTemp.veicle = undefined
      $scope.cadastroVeiculo.select = false
    $scope.focarElemento($scope.caixaNovoVisitanteEdit, 'caixaNovoVisitanteEdit.nome')
    $scope.editCadastroModal.open()

  $scope.cadastrarVisitante = ->
    $scope.cadastro.open()
    $scope.cadastroPessoa.nome = $scope.searchPeople
    $scope.focarElemento($scope.caixaCadastro, "caixaCadastro.nome")
    $timeout ->
      $scope.motivoVisitaTemp.showVisita = false

  testeCadastro = ->
    if $scope.cadastroPessoa.nome == '' && $scope.aplicarConfiguracao.nomeCompleto
      alert 'Erro: \n    Nome Completo'
      $scope.focarElemento($scope.caixaCadastro, 'caixaCadastro.nome')
      return false
    if $scope.cadastroPessoa.rg == '' && $scope.aplicarConfiguracao.rg
      alert 'Erro: \n    RG'
      $scope.focarElemento($scope.caixaCadastro, 'caixaCadastro.rg')
      return false
    if $scope.cadastroPessoa.OExpedidor == '' && $scope.aplicarConfiguracao.Oexpedidor
      alert 'Erro: \n    Orgão Expedidor'
      $scope.focarElemento($scope.caixaCadastro, 'caixaCadastro.orgaoTarget')
      return false
    if $scope.cadastroPessoa.cpf == '' && $scope.aplicarConfiguracao.cpf
      alert 'Erro: \n    CPF'
      $scope.focarElemento($scope.caixaCadastro, 'caixaCadastro.cpfTarget')
      return false
    if $scope.cadastroVeiculo.select
      if $scope.carCC
        if $scope.cadastroVeiculo.car == '' && $scope.aplicarConfiguracao.modelo
          alert 'Erro: \n    Adicionar veículo'
          $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.modeloVisitVeicle')
          return false
        else
          if $scope.cadastroVeiculo.carPlaca == '' && $scope.aplicarConfiguracao.placa
            alert 'Erro: \n    Placa do veículo'
            $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.placaVeiculo')
            return false
          else
            if $scope.cadastroVeiculo.cor == '' && $scope.aplicarConfiguracao.cor
              alert 'Erro: \n    Cor do veículo'
              $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.corVeiculo')
              return false
            else
               if $scope.cadastroVeiculo.tag == '' && $scope.aplicarConfiguracao.tag
                  alert 'Erro: \n    Tag'
                  $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.carInputTag')
                  return false
      if $scope.cadastroVeiculo.car == '' && $scope.aplicarConfiguracao.modelo
        alert 'Erro: \n    Adicionar veículo'
        $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.modeloVisitVeicle')
        return false

    if $scope.addMotivoVisita
      if !$scope.cadastroMotivoVisitaTemp.prestador
        alert 'Erro: \n    Prestador de Serviço'
        $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.servico')
        return false
    if !$scope.cadastroMotivoVisitaTemp.motivoDaVisita
      alert 'Erro: \n    Motivo da visita'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.obs')
      return false
    if !$scope.cadastroMotivoVisitaTemp.observacaoMotivo
      alert 'Erro: \n    Observações (Motivo da visita)'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.Observacao')
      return false
    if !$scope.cadastroMotivoVisitaTemp.destino
      alert 'Erro: \n    Destino/Residência'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.destino')
      return false
    if !$scope.cadastroMotivoVisitaTemp.autorizacao
      alert 'Erro: \n    Autorização'
      $scope.focarElemento($scope.caixaNovoVisitante, 'caixaNovoVisitante.autorizacao')
      return false
    return true

  $scope.addCadastroVisitante = -> #aparentemente O.K.
    if testeCadastro()
      if $scope.carCC == true
        $scope.veiculos.push
          id: $scope.veiculos.length
          cor: $scope.cadastroVeiculo.cor
          tag: $scope.cadastroVeiculo.tag
          name: $scope.cadastroVeiculo.car
          placa: $scope.cadastroVeiculo.caPlaca
          saiu: true

        $scope.veicleId = $scope.veiculos.length-1
        $scope.tagVeicle = $scope.cadastroVeiculo.tag
        $scope.carSelect = $scope.cadastroVeiculo.select
        $scope.searchVeicle = $scope.cadastroVeiculo.car

      $scope.addId = $scope.pessoas.length

      if $scope.cadastroVeiculo.select
        $scope.tagVeicle = $scope.cadastroVeiculo.tag
        $scope.carSelect = $scope.cadastroVeiculo.select
        $scope.searchVeicle = $scope.cadastroVeiculo.car

      $scope.pessoas.push
        id: $scope.pessoas.length
        rg: $scope.cadastroPessoa.rg
        cpf: $scope.cadastroPessoa.cpf
        saiu: true
        name: $scope.cadastroPessoa.nome
        photo: 'http://i.imgur.com/UfMLctB.jpg'
        Oexpedidor: $scope.cadastroPessoa.OExpedidor

      $scope.addId = $scope.pessoas.length-1
      $scope.motivoVisitaTemp = $scope.cadastroMotivoVisitaTemp
      $scope.peopleEditCadastro.name = $scope.cadastroPessoa.nome
      $scope.addVisit()
      $scope.cancelarCadastro()

  $scope.cancelarCadastro = ->
    $scope.carCC = false
    $scope.cadastroVeiculo.select = false
    $scope.cadastroMotivoVisitaTemp = {}
    $scope.cadastroPessoa = {}
    $scope.cadastro.close()

  $scope.salvarEditCadastro = ->
    lista = angular.copy($scope.buffer[$scope.saveIdlist])
    $scope.pessoas[lista.id].saiu = true
    if $scope.cadastroEditVisitEdit
      $scope.pessoas.push
        id: $scope.pessoas.length
        name: $scope.peopleEditCadastro.name
        cpf: $scope.editarCadastroTemp.cpf
        rg: $scope.editarCadastroTemp.rg
        Oexpedidor: $scope.editarCadastroTemp.OExpedidor
        photo: 'http://i.imgur.com/UfMLctB.jpg'
        saiu: false
      lista.id = $scope.pessoas.length-1
    else
      lista.id = $scope.addIdEdit if $scope.addIdEdit

    if $scope.addMotivoVisitaEdit
      lista.prestador = $scope.editarCadastroTemp.prestador

    lista.destino = $scope.editarCadastroTemp.destino
    lista.autorizacao = $scope.editarCadastroTemp.autorizacao
    lista.motivoVisita = $scope.editarCadastroTemp.motivoVisita
    lista.observacaoMotivo = $scope.editarCadastroTemp.observacaoMotivo
    lista.data = $scope.editarCadastroTemp.data
    lista.hora = $scope.editarCadastroTemp.hora

    if $scope.cadastroVeiculo.select
      if $scope.carCC
        $scope.veiculos.push
          id: $scope.veiculos.length
          name: $scope.editarCadastroTemp.veicle + ' (' + $scope.editarCadastroTemp.PlacaEdit + ')'
          cor: $scope.editarCadastroTemp.cor
        lista.carId = $scope.veiculos.length-1
      else
      if $scope.editarCadastroTemp.tag
        lista.car = true
        lista.tag = $scope.editarCadastroTemp.tag
        lista.carId = $scope.veicleIdEdit
      $scope.veiculos[lista.carId].saiu = false
    else
      lista.carId = ''
      lista.car = false
      lista.tag = ''
      $scope.veicleIdEdit = ''

    $scope.buffer[$scope.saveIdlist] = lista
    $scope.closeEditarCadastro()

  $scope.closeEditarCadastro = ->
    $scope.carCC = false
    $scope.addMotivoVisitaEdit = false
    $scope.editarCadastroTemp.rg = ''
    $scope.cadastroEditVisitEdit = false
    $scope.carSelectCadastroEdit = false
    $scope.editarCadastroTemp.cpf = ''
    $scope.searchVeicleEdit = ''
    $scope.searchPeopleEdit = ''
    $scope.editarCadastroTemp.OExpedidor = ''
    $scope.editCadastroModal.close()

  $scope.selecionarVeiculo = (lista, ind) -> #O.K.
    for i in lista
      if i.car == true
        editVeiculoOrig = lista[lista.indexOf(i)]

    if editVeiculoOrig
      editVeiculoDest = lista[ind]
      temp = editVeiculoOrig.carId
      editVeiculoDest.carId = temp
      editVeiculoDest.tag = editVeiculoOrig.tag
      editVeiculoOrig.tag = ''
      editVeiculoOrig.car = false
      editVeiculoDest.car = true

  $scope.selectMotivo = (select2) ->
    select2 = !select2
  ## Funções Final
]
