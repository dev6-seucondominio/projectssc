class Cobranca < ActiveRecord::Base
  has_many :recebimentos
  has_many :composicao_cobrancas

  def to_frontEnd_obj
    totais = {
      composicao: composicao_cobrancas.sum(:valor),
      recebimentos: recebimentos.sum(:valor)
    }
    return {
      id: id,
      valor: valor,
      juros: juros,
      multa: multa,
      atualizado: atualizado,
      vencimento: vencimento.to_date,
      totais: totais,
      recebimentos: recebimentos.map(&:to_frontEnd_obj),
      composicaoCobranca: composicao_cobrancas.map(&:to_frontEnd_obj)
    }
  end

end
