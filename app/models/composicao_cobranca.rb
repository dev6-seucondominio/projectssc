# encoding: UTF-8
class ComposicaoCobranca < ActiveRecord::Base
  belongs_to :cobranca

  def to_frontEnd_obj
    {
      valor: valor,
      titulo: titulo
    }
  end

end
